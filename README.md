# CTime

## Genèse

CTime est une application Angular 2.
Elle consomme une API rest faite en Symfony [CollaborativeTimeServeur](https://gitlab.com/al37350/collaborativeTimeServer)

## Installation

Tapez `ng serve` pour lancer le serveur de développement. Rendez-vous sur `http://localhost:4200/`. 
L'application sera automatiquement rechargée si vous changez les fichiers sources.

Pour une mise en production il suffit d'utiliser docker.
`docker-compose up -d --build`
Ctime est desormais accessible à l'adresse `http://localhost:8080`
## Screenshots
![alt text](screenshots/pie.png "Visualisation de son temps sur une journée")

![alt text](screenshots/duree.png "Enregistrement à la durée")

![alt text](screenshots/admin.png "Panel d'administration")

![alt text](screenshots/options.png "Configuration de ses informations personnelles")

## License

CollaborativeTime est sous MIT license.

## A propos de bastide-bondoux
![Bastide bondoux](http://www.bastide-bondoux.fr/img/logo-01.jpg)
CollaborativeTime a été initié par l'équipe de [Bastide-Bondoux](http://www.bastide-bondoux.fr)