import { Component, OnInit } from '@angular/core';
import { Link } from '../_models/link';
import { version } from '../globals';
import {SharedService} from "../_services/shared.service";
import {Horloge} from "../_models/horloge";
import {CurrentUserService} from "../_services/currentUser.service";
import {NotificationService} from "../_services/notification.service";
import {SharedNotificationService} from "../_services/shared.notification.service";

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
    private links: Link[];
    private logoUrl: string;
    private version : string;
    private horloge : Horloge;


    constructor(
        private  sharedService : SharedService,
        private currentUserService : CurrentUserService,
        private sharedNotificationService : SharedNotificationService
    ) {
        this.logoUrl = require("../_ressources/logo/logo-300px.png");
        this.version = version;
        this.horloge = new Horloge();
    }

    ngOnInit() {
        this.sharedService.setGroupes(this.currentUserService.getUser().groupes);
        this.links = this.sharedService.links;
    }

    notificationsLength(){
        return this.sharedNotificationService.notifications.filter(notif => notif.isActive == true).length;
    }

}
