import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarEmitterComponent } from './navbar-emitter.component';

describe('NavbarEmitterComponent', () => {
  let component: NavbarEmitterComponent;
  let fixture: ComponentFixture<NavbarEmitterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarEmitterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarEmitterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
