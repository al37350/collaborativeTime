import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'navbar-emitter',
  templateUrl: './navbar-emitter.component.html',
  styleUrls: ['./navbar-emitter.component.scss']
})
export class NavbarEmitterComponent implements OnInit {
  @Input() links;
  @Output() onChangeElem = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }

  private select(link){
    console.log(link);
    this.links.map(unLink => delete unLink.selected);
    this.onChangeElem.emit(link);
    link.selected = true;
  }

}
