import {Component, OnInit, Input} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss']
})
export class TimepickerComponent implements OnInit {
  private _value: string;
  private momentDate;

  @Input()
  set value(value: string) {
    this._value = value;
  }
  get date() { return this.momentDate };

  get timeFormatted() : string { return this.date.format('LT')};

  constructor() { };

  ngOnInit() {
      this.momentDate = moment(this._value, 'X');
    }
  }