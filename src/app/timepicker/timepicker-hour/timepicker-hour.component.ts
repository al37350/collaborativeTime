import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'timepicker-hour',
  templateUrl: './timepicker-hour.component.html',
  styleUrls: ['./timepicker-hour.component.scss']
})
export class TimepickerHourComponent implements OnInit {
  @Input() private date;

  constructor() { }

  ngOnInit() {
  }

}
