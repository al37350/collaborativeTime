import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimepickerHourComponent } from './timepicker-hour.component';

describe('TimepickerHourComponent', () => {
  let component: TimepickerHourComponent;
  let fixture: ComponentFixture<TimepickerHourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimepickerHourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimepickerHourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
