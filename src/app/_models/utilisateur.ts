export class Utilisateur{
    constructor(
        public id: number,
        public nom: string,
        public prenom: string,
        public identifiant: string,
        public email: string
    ) {  }
}