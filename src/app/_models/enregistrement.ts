import {PeriodeTemps} from "./PeriodeTemps";

export class Enregistrement extends PeriodeTemps{

    typeTravail: string;
    sujet: string;

    constructor(id: number, jour: string, heureDebut: string, heureFin: string, duree: string, typeTravail: string, sujet: string, couleur: string) {
        super(id, jour, heureDebut, heureFin, duree, couleur);
        this.typeTravail = typeTravail;
        this.sujet = sujet;
    }
}