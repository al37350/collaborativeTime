export class Timer {
    private _heures: number = 0;
    private _minutes: number = 0;
    private _secondes: number = 0;
    private _totalSecondes: number = 0;
    private _timer;
    get heures(): number { return this._heures; }
    get minutes(): number { return this._minutes; }
    get secondes(): number { return this._secondes; }
    setTotalSecondes(secondes : number){  this._totalSecondes = secondes };

    start() {
        this._timer = setInterval(() => {
            this._heures = Math.floor(++this._totalSecondes / 3600);
            this._minutes = Math.floor(this._totalSecondes / 60);
            this._secondes = this._totalSecondes - this._minutes * 60;
        }, 1000);
    }
    stop() {
        clearInterval(this._timer);
    }
    reset() {
        this._totalSecondes = this._heures = this._minutes = this._secondes = 0;
    }

    toString(){
        return this._heures + " heures " + this._minutes + " minutes et "+ this._secondes + " secondes"
    }
}