import * as moment from 'moment';
export class TimeConverter {

    /**
     *
     * @param moment
     * @param heure 08:27
     * @param delimiter
     * @returns moment
     */
    public static fromStringToTimestamp(moment, heure, delimiter : string = ":") {
        let splitHeureDeb = TimeConverter.heureStringToArray(heure, delimiter);

        let Hdeb = parseInt(splitHeureDeb[0]);
        let Mdeb = parseInt(splitHeureDeb[1]);

       return moment.clone().startOf('day').set('hour', Hdeb).set('minute', Mdeb).unix();
    }


    public static fromStringToSeconds(heure, delimiter : string = ":") {
        let splitHeure = TimeConverter.heureStringToArray(heure, delimiter);

        let hours = parseInt(splitHeure[0]);
        let minutes = parseInt(splitHeure[1]);
        return ( hours * 60 + minutes ) * 60;
    }


    public static fromSecondsToHms(d : number){
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);

        var hDisplay = h > 0 ? h + (h == 1 ? " heure, " : " heures, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? " seconde" : " secondes") : "";
        return hDisplay + mDisplay + sDisplay;
    }

    private static heureStringToArray(heureString : string, delimiter : string ){
        return heureString.split(delimiter);
    }
}