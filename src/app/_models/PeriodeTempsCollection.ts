import {Enregistrement} from "./enregistrement";
import {PeriodeOff} from "./PeriodeOff";

export class PeriodeTempsCollection {
    private _enregistrements: Array<Enregistrement>;
    private _periodesOff: Array<PeriodeOff>;


    public constructor() {
        this._enregistrements = [];
        this._periodesOff = [];
    }

    get enregistrements(): Array<Enregistrement> {
        return this._enregistrements;
    }

    get periodesOff(): Array<PeriodeOff> {
        return this._periodesOff;
    }

    set enregistrements(value: Array<Enregistrement>) {
        this._enregistrements = value;
    }

    set periodesOff(value: Array<PeriodeOff>) {
        this._periodesOff = value;
    }

    addEnregistrement(enregistrement : Enregistrement){
        this._enregistrements.push(enregistrement);
        return this;
    }

    addPeriodeOff(periodeOff : PeriodeOff){
        this._periodesOff.push(periodeOff);
        return this;
    }

    fromJSON(enregistrementsJson){
        enregistrementsJson.map( enregistrementJson => {
            if(enregistrementJson.sujet === undefined){
                this.addPeriodeOff(
                    new PeriodeOff(
                        enregistrementJson.id,
                        enregistrementJson.jour,
                        enregistrementJson.heureDebut,
                        enregistrementJson.heureFin,
                        enregistrementJson.duree,
                        enregistrementJson.motif.nom
                    )
                );
            }
            else{
                if(enregistrementJson.typeTravail != null){
                    this.addEnregistrement(

                        new Enregistrement(
                            enregistrementJson.id,
                            enregistrementJson.jour,
                            enregistrementJson.heureDebut,
                            enregistrementJson.heureFin,
                            enregistrementJson.duree,
                            enregistrementJson.typeTravail.nom,
                            enregistrementJson.sujet.nom,
                            enregistrementJson.sujet.famille.couleur
                        )
                    );
                }
            }
        });
        return this;
    }
}