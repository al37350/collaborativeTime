export class Cell {
    value : number;
    toFormat : boolean;

    constructor(value: number, toFormat : boolean = false) {
        this.value = value;
        this.toFormat = toFormat;
    }

    toString(){
        return this.value;
    }
}