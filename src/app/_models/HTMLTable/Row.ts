import {Cell} from "./Cell";
export class Row {
    name : string;
    isImportant: boolean = false;
    cells : Array<Cell> = [];

    constructor(name: string) {
        this.name = name;
    }

    get length(){
        return this.cells.length
    }
    public addCell(cell : Cell) : Row{
        this.cells.push(cell);
        return this;
    }

    public toCSV() : string {
        return [this.name, ...this.cells].join(";");
    }

    public getTotal(): number{
        return this.cells.map(cell => cell.value).reduce((pv, cv) => pv+cv, 0);
    }
}