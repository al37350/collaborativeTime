import {Row} from "./Row";
import {Cell} from "./Cell";

export class Table {
    headers : Array<string> = [];
    rows : Array<Row> = [];

    public addHeader(header : string){
        this.headers.push(header);
    }

    public addRow(row : Row){
        this.rows.push(row);
    }

    public toCSV() : string {
        let result = [];
        result.push(["", ...this.headers].join(";"));
        for(let row of this.rows){
            result.push(row.toCSV());
        }

        return result.join("\n");
    }

    public getTotalHorizontal(){
        let result : Cell[] = [];
        for(let row of this.rows){
            result.push(new Cell(row.getTotal(), true));
        }
        return result;
    }

    public getTotalVertical(){
        if(this.rows.length){
            let result : Cell[] = [];
            let rowLength = this.rows[0].length;
            let i = 0;
            while (i<rowLength){
                let sum = 0;
                for(let row of this.rows){
                    sum += row.cells[i].value;
                }
                result.push(new Cell(sum, true));
                i++;
            }
            return result;
        }
    }

    public getTotal(){
        return new Cell(this.getTotalHorizontal().map(cell => cell.value).reduce((pv, cv) => pv+cv, 0), true);
    }
}