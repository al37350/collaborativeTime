import {TypeTravailSelectionDefined} from "./TypeTravailSelectionDefined";

export class TypeTravailCustom{
    nom: string;
    typeTravailSelectionDefined : TypeTravailSelectionDefined;

    constructor(nom: string) {
        this.nom = nom;
    }
}