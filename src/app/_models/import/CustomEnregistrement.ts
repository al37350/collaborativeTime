import {TagCustom} from "./TagCustom";
export class CustomEnregistrement {
    customSujet: string;
    customTypeTravail: string;
    tagCustomCollection : Array<TagCustom>;
    duree: number;
    day: number;
    month: number;
    year: number;

    constructor(customSujet: string, customTypeTravail: string, tagCustomCollection :Array<TagCustom>,  duree: number, day: number, month: number, year: number) {
        this.customSujet = customSujet;
        this.customTypeTravail = customTypeTravail;
        this.tagCustomCollection = tagCustomCollection;
        this.duree = duree;
        this.day = day;
        this.month = month;
        this.year = year;
    }
}