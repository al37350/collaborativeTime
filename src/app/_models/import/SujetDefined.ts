import {TypeTravailSelectionDefined} from "./TypeTravailSelectionDefined";
export class SujetDefined {
    id: number;
    nom: string;
    typeTravailSelectionDefinedCollection : Array<TypeTravailSelectionDefined> = [];

    constructor(id: number, nom: string, typeTravailSelectionDefinedCollection : Array<TypeTravailSelectionDefined>) {
        this.id = id;
        this.nom = nom;
        this.typeTravailSelectionDefinedCollection = typeTravailSelectionDefinedCollection;
    }
}