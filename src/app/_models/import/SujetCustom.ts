import {SujetDefined} from "./SujetDefined";
import {TypeTravailCustom} from "./TypeTravailCustom";
import {TagCustom} from "./TagCustom";

export class SujetCustom {
    nom: string;
    typeTravailCustomCollection : Array<TypeTravailCustom> = [];
    tagCustomCollection : Array<TagCustom> = [];

    sujetDefined : SujetDefined;

    constructor(nom: string) {
        this.nom = nom;
        this.sujetDefined = new SujetDefined(-1,"yolo", []);
    }

    addTypeTravailCustom(typeTravailCustom : TypeTravailCustom){
        if(!this.typeTravailCustomCollection.find(ttc => ttc.nom == typeTravailCustom.nom)){
            this.typeTravailCustomCollection.push(typeTravailCustom);
        }
    }

    addTagCustom(tagCustom : TagCustom){
        this.tagCustomCollection.push(tagCustom);
    }
}