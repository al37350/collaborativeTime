/**
 * Created by al on 16/05/17.
 */
export class TagDefined{
    id:number;
    nom: string;

    constructor(id: number, nom: string) {
        this.id = id;
        this.nom = nom;
    }
}