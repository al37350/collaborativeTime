import {TagDefined} from "./TagDefined";

export class TagCustom{
    nom: string;
    tagDefined : TagDefined;

    constructor(nom: string) {
        this.nom = nom;
    }
}