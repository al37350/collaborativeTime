import {SujetCustom} from "./SujetCustom";
export class SujetCustomCollection {
    collection : Array<SujetCustom> = [];

    addSujetCustom(sujetCustom : SujetCustom){
        let existingSujetCustom = this.collection.find(sujetCust => sujetCust.nom == sujetCustom.nom);
        if(!existingSujetCustom){
            this.collection.push(sujetCustom);
        }
        else{
            existingSujetCustom.typeTravailCustomCollection.push(sujetCustom.typeTravailCustomCollection[0])
        }
    }

    getSujetCustom(sujetCustomName :string) : SujetCustom{
        let find = this.collection.find(sujetCustom => sujetCustom.nom == sujetCustomName);
        if(find){
            return find;
        }
        let sujetCustom = new SujetCustom(sujetCustomName);
        this.addSujetCustom(sujetCustom);
        return sujetCustom;
    }
}