export class Horloge {

    private _temps;
    get temps(): number { return this._temps; }

    constructor(){
        this._temps = Date.now();
        setInterval(() => this._temps=Date.now() , 10000);
    }
}
