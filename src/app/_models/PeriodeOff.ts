import {PeriodeTemps} from "./PeriodeTemps";

export class PeriodeOff extends PeriodeTemps{

    motif: string;

    constructor(id: number, jour: string, heureDebut: string, heureFin: string, duree: string, motif: string) {
        super(id, jour, heureDebut, heureFin, duree);
        this.motif = motif;
    }
}