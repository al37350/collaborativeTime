/**
 * Created by al on 19/04/17.
 */

export class EnregistrementQualification {
    sujetId: number;
    typeTravailSelection: any;
    tagsPersoId : number[];
    description: string;

    constructor(sujetId: number, typeTravailSelection: any, tagsPersoId: number[], description : string = "") {
        this.sujetId = sujetId;
        this.typeTravailSelection = typeTravailSelection;
        this.tagsPersoId = tagsPersoId;
        this.description = description;
    }
}