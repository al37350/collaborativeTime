import * as moment from 'moment';

export class PeriodeTemps {
    id: number;
    jour: string;
    heureDebut;
    heureFin;
    duree: string;
    couleur: string;

    constructor(id: number, jour: string, heureDebut: string, heureFin: string, duree: string, couleur: string = "#000") {
        this.id = id;
        this.jour = jour;
        this.heureDebut = moment(heureDebut, 'X');
        this.heureFin = moment(heureFin, 'X');
        this.duree = duree;
        this.couleur = couleur;
    }

    get heureDebutFormatted(): string {
        return this.heureDebut.format('LTS');
    }

    get heureFinFormatted(): string {
        return this.heureFin.format('LTS');
    }
}