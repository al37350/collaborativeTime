export class Link {
    name:string;
    img:string;
    path:string;
    poid: number;

    constructor(name, img, path, poid) {
        this.name = name;
        this.img = img;
        this.path = path;
        this.poid = poid;
    }
};