import { Injectable } from '@angular/core';
import {CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import {CurrentUserService} from "../_services/currentUser.service";

@Injectable()
export class CanActivateAuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private currentUserService: CurrentUserService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if(this.currentUserService.tokenIsValid()){
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}