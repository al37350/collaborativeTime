import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SujetRepartitionChartComponent } from './sujet-repartition-chart.component';

describe('SujetRepartitionChartComponent', () => {
  let component: SujetRepartitionChartComponent;
  let fixture: ComponentFixture<SujetRepartitionChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SujetRepartitionChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SujetRepartitionChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
