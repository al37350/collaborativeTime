import {Component, OnInit, Input, EventEmitter, Output, OnChanges} from '@angular/core';
import {RepartitionService} from "../../_services/stat/repartition.service";
import {TimeConverter} from "../../_models/TimeConverter";
import {FamilleService} from "../../_services/famille.service";
import {Table} from "../../_models/HTMLTable/Table";
import {Row} from "../../_models/HTMLTable/Row";
import {Cell} from "../../_models/HTMLTable/Cell";

@Component({
    selector: 'sujet-repartition-chart',
    templateUrl: './sujet-repartition-chart.component.html',
    styleUrls: ['./sujet-repartition-chart.component.scss']
})
export class SujetRepartitionChartComponent implements OnInit, OnChanges {
    @Input() debut;
    @Input() fin;

    @Output() onFamilleChange: EventEmitter<any> = new EventEmitter();

    private chart;

    private familles;
    private selectedFamille;

    private options : Object;
    private show : boolean = false;

    private table : Table = new Table();

    constructor(
        private repartitionService : RepartitionService,
        private familleService : FamilleService
    ) { }

    ngOnInit() {
        this.options = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                height : 400
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                },
                series: {
                    turboThreshold:3000,
                    cursor: 'pointer'
                }
            },
            tooltip: {
                formatter: function () {
                    return "<b>"+this.key+" :</b> " + TimeConverter.fromSecondsToHms(this.y);
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: []
            }]

        };

        this.familleService
            .getFamilles()
            .subscribe(
                (data) => {
                    this.familles = data;
                    if(this.familles.length){
                        this.selectedFamille = this.familles[0];
                        this.onChangeFamille();
                    }
                },
                (err) => console.log(err)
            );
    }

    ngOnChanges(changes: any){
        if(changes.debut){
            if(!changes.debut.firstChange){
                this.loadRepartition();
            }
        }
        if(changes.fin){
            if(!changes.fin.firstChange){
                this.loadRepartition();
            }
        }
    }

    saveChart(chart){
        this.chart=chart;
    }

    onChangeFamille(){
        this.loadRepartition();
        this.loadRepartitionTable();
        this.onFamilleChange.emit(this.selectedFamille);
    }

    loadRepartition(){
        this.repartitionService.getFamilleSujetRepartition(
            this.selectedFamille.id,
            this.debut.format('YYYY-MM-DD'),
            this.fin.format('YYYY-MM-DD')
        ).subscribe(
            (rep) => {
                if(rep.length){
                    this.chart.setTitle({text: 'Famille : ' + this.selectedFamille.nom});
                    this.chart.series[0].setData(this.formatData(rep));
                    this.show = true;
                }
                else{
                    this.show = false;
                }
            }
        );
    }

    loadRepartitionTable(){
        this.repartitionService.getCaracteristiqueFamilleStats(
            this.selectedFamille.id,
            this.debut.format('YYYY-MM-DD'),
            this.fin.format('YYYY-MM-DD')
        ).subscribe(
            (res) => {
                this.table = new Table();
                this.table.headers = res.names;
                for(let value of res.values){
                    let row = new Row(value.name);
                    for(let cell of value.datas){
                        row.addCell(new Cell(parseInt(cell), true))
                    }
                    this.table.addRow(row);
                }
            }
        )
    }

    formatData(data){
        return data.map(jsonFamilleSujet => {
            return {
                name: jsonFamilleSujet[0].nom,
                y: parseInt(jsonFamilleSujet[1]),
                sujetId: parseInt(jsonFamilleSujet[0].id),
            }
        })
    }
}
