import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeTravailSujetRepartitionChartComponent } from './type-travail-sujet-repartition-chart.component';

describe('TypeTravailSujetRepartitionChartComponent', () => {
  let component: TypeTravailSujetRepartitionChartComponent;
  let fixture: ComponentFixture<TypeTravailSujetRepartitionChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeTravailSujetRepartitionChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeTravailSujetRepartitionChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
