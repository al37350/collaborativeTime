import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {RepartitionService} from "../../_services/stat/repartition.service";
import {TimeConverter} from "../../_models/TimeConverter";
import {Table} from "../../_models/HTMLTable/Table";
import {Row} from "../../_models/HTMLTable/Row";
import {Cell} from "../../_models/HTMLTable/Cell";

@Component({
    selector: 'type-travail-sujet-repartition-chart',
    templateUrl: './type-travail-sujet-repartition-chart.component.html',
    styleUrls: ['./type-travail-sujet-repartition-chart.component.scss']
})
export class TypeTravailSujetRepartitionChartComponent implements OnInit, OnChanges {
    @Input() debut;
    @Input() fin;

    @Input() famille;

    private chart;

    private selectedSujet;

    private options : Object;
    private show : boolean = false;

    private table : Table = new Table();

    private opt = {};
    constructor(
        private repartitionService : RepartitionService
    ) { }


    ngOnInit() {
        this.options = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                height : 400
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                },
                series: {
                    turboThreshold:3000,
                    cursor: 'pointer'
                }
            },
            tooltip: {
                formatter: function () {
                    return "<b>"+this.key+" :</b> " + TimeConverter.fromSecondsToHms(this.y);
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: []
            }]

        }
    }

    ngOnChanges(changes: any) {
        if(this.famille){
            if(this.famille.sujets.length){
                this.selectedSujet = this.famille.sujets[0];
                this.onChangeSujet();
                this.show = true;
            }
            else{
                this.show = false;
            }
        }
    }

    onChangeSujet(){
        this.repartitionService.getTypeTravailSujetRepartition(
            this.selectedSujet.id,
            this.debut.format('YYYY-MM-DD'),
            this.fin.format('YYYY-MM-DD')
        ).subscribe(
            (rep) => {
                this.chart.setTitle({text: 'Sujet : ' + this.selectedSujet.nom});
                this.chart.series[0].setData(this.formatData(rep));
            }
        );

        this.repartitionService.getCaracteristiqueSujetStats(
            this.selectedSujet.id,
            this.debut.format('YYYY-MM-DD'),
            this.fin.format('YYYY-MM-DD')
        ).subscribe(
            (res) => {
                this.table = new Table();
                this.table.headers = res.names;
                for(let value of res.values){
                    let row = new Row(value.name);
                    for(let cell of value.datas){
                        row.addCell(new Cell(parseInt(cell), true))
                    }
                    this.table.addRow(row);
                }
            }
        );

        this.repartitionService.getHistoSujet(
            this.selectedSujet.id,
            this.debut.format('YYYY-MM-DD'),
            this.fin.format('YYYY-MM-DD')
        ).subscribe(
            (rep) => {
                this.opt= {
                    chart: {
                        zoomType: 'x',
                        height : 400
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: 'Evolution des temps de travail pour le sujet ' + this.selectedSujet.nom
                    },
                    xAxis: {
                        type: 'datetime',
                        tickInterval: 3600 * 24 *1000,
                        title: {
                            text: null
                        }
                    },
                    series: rep

                };
            }
        );

    }

    saveChart(chart){
        this.chart=chart;
    }

    formatData(data){
        return data.map(jsonFamilleSujet => {
            return {
                name: jsonFamilleSujet[0].nom,
                y: parseInt(jsonFamilleSujet[1]),
                sujetId: parseInt(jsonFamilleSujet[0].id),
            }
        })
    }
}
