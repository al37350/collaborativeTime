import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultChoiceChartComponent } from './default-choice-chart.component';

describe('DefaultChoiceChartComponent', () => {
  let component: DefaultChoiceChartComponent;
  let fixture: ComponentFixture<DefaultChoiceChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultChoiceChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultChoiceChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
