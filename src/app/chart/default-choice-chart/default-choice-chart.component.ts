import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'choice-chart',
    templateUrl: './default-choice-chart.component.html',
    styleUrls: ['./default-choice-chart.component.scss']
})
export class DefaultChoiceChartComponent implements OnInit {

    @Input() opt;
    private chart;

    constructor() { }

    ngOnInit() { }

    saveChart(chart){
        this.chart=chart;
    }

    changeType(type){
        let i =0;
        while(i<this.chart.series.length){
            this.chart.series[i].update(
                {
                    type : type
                }
            );
            i++;
        }

    }
    
}
