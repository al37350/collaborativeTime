import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {RepartitionService} from "../../_services/stat/repartition.service";
import {TimeConverter} from "../../_models/TimeConverter";
import {Table} from "../../_models/HTMLTable/Table";
import {Row} from "../../_models/HTMLTable/Row";
import {Cell} from "../../_models/HTMLTable/Cell";

@Component({
    selector: 'famille-repartition-chart',
    templateUrl: './famille-repartition-chart.component.html',
    styleUrls: ['./famille-repartition-chart.component.scss']
})
export class FamilleRepartitionChartComponent implements OnInit, OnChanges {
    @Input() debut;
    @Input() fin;

    private options : Object;
    private chart;

    private table : Table = new Table();

    constructor(
        private repartitionService : RepartitionService
    ) { }

    ngOnInit() {
        this.options = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                height : 400
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                },
                series: {
                    turboThreshold:3000,
                    cursor: 'pointer'
                }
            },
            tooltip: {
                formatter: function () {
                    return "<b>"+this.key+" :</b> " + TimeConverter.fromSecondsToHms(this.y);
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: []
            }]

        };
    }

    ngOnChanges(changes: any){
        this.repartitionService.getFamilleRepartition(this.debut.format('YYYY-MM-DD'), this.fin.format('YYYY-MM-DD')).subscribe(
            (rep) => {
                this.chart.series[0].setData(rep.map(jsonFamille => {
                    return {
                        name: jsonFamille[0].nom,
                        y: parseInt(jsonFamille[1]),
                        color: jsonFamille[0].couleur,
                        familleId: parseInt(jsonFamille[0].id),
                    }
                }));
                this.chart.setTitle({text: 'Répartition selon les familles du ' + this.debut.format('DD-MM-YYYY') + ' au ' + this.fin.format('DD-MM-YYYY')});
            }
        );

        this.repartitionService.getTypeTravailStats(
            this.debut.format('YYYY-MM-DD'),
            this.fin.format('YYYY-MM-DD')
        ).subscribe(
            (res) => {
                this.table = new Table();
                this.table.headers = res.names;
                for(let value of res.values){
                    let row = new Row(value.name);
                    for(let cell of value.datas){
                        row.addCell(new Cell(parseInt(cell), true))
                    }
                    this.table.addRow(row);
                }
            }
        )
    }

    saveChart(chart){
        this.chart=chart;
    }
}
