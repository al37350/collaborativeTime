import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilleRepartitionChartComponent } from './famille-repartition-chart.component';

describe('FamilleRepartitionChartComponent', () => {
  let component: FamilleRepartitionChartComponent;
  let fixture: ComponentFixture<FamilleRepartitionChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilleRepartitionChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilleRepartitionChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
