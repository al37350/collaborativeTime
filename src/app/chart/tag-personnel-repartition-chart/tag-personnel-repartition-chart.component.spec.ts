import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagPersonnelRepartitionChartComponent } from './tag-personnel-repartition-chart.component';

describe('TagPersonnelRepartitionChartComponent', () => {
  let component: TagPersonnelRepartitionChartComponent;
  let fixture: ComponentFixture<TagPersonnelRepartitionChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagPersonnelRepartitionChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagPersonnelRepartitionChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
