import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {RepartitionService} from "../../_services/stat/repartition.service";
import {TimeConverter} from "../../_models/TimeConverter";
import {CurrentUserService} from "../../_services/currentUser.service";

@Component({
  selector: 'tag-personnel-repartition-chart',
  templateUrl: './tag-personnel-repartition-chart.component.html',
  styleUrls: ['./tag-personnel-repartition-chart.component.scss']
})
export class TagPersonnelRepartitionChartComponent implements OnInit, OnChanges {
  @Input() debut;
  @Input() fin;

  private options : Object;
  private chart;

  constructor(
      private repartitionService : RepartitionService,
      private currentUserService : CurrentUserService
  ) { }

  ngOnInit() {
    this.options = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        height : 400
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        },
        series: {
          turboThreshold:3000,
          cursor: 'pointer'
        }
      },
      tooltip: {
        formatter: function () {
          return "<b>"+this.key+" :</b> " + TimeConverter.fromSecondsToHms(this.y);
        }
      },
      series: [{
        name: 'Brands',
        colorByPoint: true,
        data: []
      }]

    };
  }

  ngOnChanges(changes: any){
    this.repartitionService.getTagPersonnelRepartition(this.currentUserService.getUser().id, this.debut.format('YYYY-MM-DD'), this.fin.format('YYYY-MM-DD')).subscribe(
        (rep) => {
          this.chart.series[0].setData(rep.map(jsonTagPersonnel => {
            return {
              name: jsonTagPersonnel[0].nom,
              y: parseInt(jsonTagPersonnel[1]),
              familleId: parseInt(jsonTagPersonnel[0].id),
            }
          }));
          this.chart.setTitle({text: 'Répartition des tags personnels du ' + this.debut.format('DD-MM-YYYY') + ' au ' + this.fin.format('DD-MM-YYYY')});
        }
    );
  }

  saveChart(chart){
    this.chart=chart;
  }

}
