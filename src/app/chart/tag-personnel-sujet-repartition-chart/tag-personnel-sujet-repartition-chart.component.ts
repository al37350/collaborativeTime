import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {RepartitionService} from "../../_services/stat/repartition.service";
import {CurrentUserService} from "../../_services/currentUser.service";
import {TimeConverter} from "../../_models/TimeConverter";
import {EnregistrementFroidStoreService} from "../../_services/EnregistrementFroidStore.service";

@Component({
    selector: 'tag-personnel-sujet-repartition-chart',
    templateUrl: './tag-personnel-sujet-repartition-chart.component.html',
    styleUrls: ['./tag-personnel-sujet-repartition-chart.component.scss']
})
export class TagPersonnelSujetRepartitionChartComponent implements OnInit, OnChanges {
    @Input() debut;
    @Input() fin;

    private options : Object;
    private chart;

    private sujets;
    private sujetSelectedId;

    constructor(
        private repartitionService : RepartitionService,
        private currentUserService : CurrentUserService,
        private enregistrementFroidStoreService : EnregistrementFroidStoreService
    ) { }

    ngOnInit() {
        this.enregistrementFroidStoreService.sujetEmitter.subscribe(
            tabSujet => {
                if(tabSujet){
                    this.sujets = this.groupBy(this.enregistrementFroidStoreService.sujets, item => [item.famille.nom]);
                    if(tabSujet.length){
                        this.sujetSelectedId = tabSujet[0].id;
                        this.loadData();
                    }
                }
            }
        );

        this.options = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                height : 400
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                },
                series: {
                    turboThreshold:3000,
                    cursor: 'pointer'
                }
            },
            tooltip: {
                formatter: function () {
                    return "<b>"+this.key+" :</b> " + TimeConverter.fromSecondsToHms(this.y);
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: []
            }]

        };
    }

    ngOnChanges(changes: any){
        if(this.sujetSelectedId){
            this.loadData()
        }
    }

    loadData(){
        this.repartitionService.getTagPersonnelSujetRepartition(this.currentUserService.getUser().id, this.sujetSelectedId, this.debut.format('YYYY-MM-DD'), this.fin.format('YYYY-MM-DD')).subscribe(
            (rep) => {
                this.chart.series[0].setData(rep.map(jsonTagPersonnel => {
                    return {
                        name: jsonTagPersonnel[0].nom,
                        y: parseInt(jsonTagPersonnel[1]),
                        familleId: parseInt(jsonTagPersonnel[0].id),
                    }
                }));
                this.chart.setTitle({text: 'Répartitions des tags personnels du ' + this.debut.format('DD-MM-YYYY') + ' au ' + this.fin.format('DD-MM-YYYY')});
            }
        );
    }

    groupBy( array , f )
    {
        var groups = {};
        array.forEach( function( o )
        {
            var group = JSON.stringify( f(o) );
            groups[group] = groups[group] || [];
            groups[group].push( o );
        });
        return Object.keys(groups).map( group => {
            return {
                "nom" : JSON.parse(group)[0],
                "values" : groups[group]
            }
        });
    }

    saveChart(chart){
        this.chart=chart;
    }

    onChangeSujet(sujetId){
        this.sujetSelectedId = sujetId;
        this.loadData();
    }
}
