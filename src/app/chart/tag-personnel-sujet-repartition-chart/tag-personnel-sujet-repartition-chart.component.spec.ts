import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagPersonnelSujetRepartitionChartComponent } from './tag-personnel-sujet-repartition-chart.component';

describe('TagPersonnelSujetRepartitionChartComponent', () => {
  let component: TagPersonnelSujetRepartitionChartComponent;
  let fixture: ComponentFixture<TagPersonnelSujetRepartitionChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagPersonnelSujetRepartitionChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagPersonnelSujetRepartitionChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
