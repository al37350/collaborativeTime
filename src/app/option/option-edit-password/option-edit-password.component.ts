import { Component, OnInit } from '@angular/core';
import {UtilisateurService} from "../../_services/utilisateur.service";
import {AlertService} from "../../_services/alert.service";
import {CurrentUserService} from "../../_services/currentUser.service";

@Component({
  selector: 'app-option-edit-password',
  templateUrl: './option-edit-password.component.html',
  styleUrls: ['./option-edit-password.component.scss']
})
export class OptionEditPasswordComponent implements OnInit {
  private model;

  constructor(
      public utilisateurService : UtilisateurService,
      public alertService : AlertService,
      public currentUserService : CurrentUserService
  ) {
    this.model = {};
    this.model.password = "";
    this.model.passwordsecure = "";
  }

  ngOnInit() {
  }

  correspond(){
    return this.model.password == this.model.passwordsecure;
  }

  onSubmit() {
    this.utilisateurService.patchUtilisateur(
        this.currentUserService.getUser().id,
        {
          "password" : this.model.password
        }
    ).subscribe(
        (reponse) => {
          this.alertService.success("Modification effectuée");
        },
        (err) => {
          console.log(err);
          let message = JSON.parse(err._body);
          this.alertService.error(message.message);
        }
    );
  }

}
