import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-option',
    templateUrl: './option.component.html',
    styleUrls: ['./option.component.scss']
})
export class OptionComponent implements OnInit {
    private links;

    constructor() {
        this.links = [
            {
                path : '/options/editconfiguration',
                name : 'Gérer ma configuration'
            },
            {
                path : '/options/tagperso',
                name : 'Gérer mes tags personnels'
            },
            {
                path : '/options/editutilisateurinfo',
                name : 'Informations personnelles'
            },
            {
                path : '/options/editpassword',
                name : 'Modification du mot de passe'
            },
        ];
    }

    ngOnInit() {
    }
}