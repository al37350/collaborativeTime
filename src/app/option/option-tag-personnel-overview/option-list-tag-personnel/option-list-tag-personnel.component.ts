import { Component, OnInit } from '@angular/core';
import {AlertService} from "../../../_services/alert.service";
import {TagPersonnelService} from "../../../_services/tagPersonnel.service";
import {AuthenticationService} from "../../../_services/authentification.service";
import {UtilisateurService} from "../../../_services/utilisateur.service";
import {CurrentUserService} from "../../../_services/currentUser.service";

@Component({
    selector: 'app-option-list-tag-personnel',
    templateUrl: 'option-list-tag-personnel.component.html',
    styleUrls: ['option-list-tag-personnel.component.scss']
})
export class OptionListTagPersonnelComponent implements OnInit {

    private userId;

    private tags;
    private nom = "";

    constructor(
        private tagPersonnelService : TagPersonnelService,
        private alertService : AlertService,
        private authenticationService : AuthenticationService,
        private utilisateurService : UtilisateurService,
        private currentUserService : CurrentUserService
    ) { }

    ngOnInit() {
        this.userId = this.currentUserService.getUser().id;
        this.utilisateurService.getUtilisateurTagPersonnel(this.userId).subscribe(
            (data) => {
                this.tags = data;
            },
            (err) => console.log(err)

        );
    }

    deleteTag(id : number){
        this.tagPersonnelService.deleteTagPersonnel(id).subscribe(
            (data) => this.tags =this.tags.filter(item => item.id != id),
            (err) => console.log(err)

        );
    }

    onSubmit(){

        this.tagPersonnelService.postTagPersonnel(
            {
                "nom" : this.nom,
                "utilisateur" : this.userId
            }
        ).subscribe(
            (data) => {
                this.alertService.success("Modification effectuée");
                this.nom ="";
                this.tags.push(data);
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.error(message.message);
            }
        );
    }

}
