import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {TagPersonnelService} from "../../../_services/tagPersonnel.service";
import {AlertService} from "../../../_services/alert.service";

@Component({
  selector: 'app-option-edit-tag-personnel',
  templateUrl: './option-edit-tag-personnel.component.html',
  styleUrls: ['./option-edit-tag-personnel.component.scss']
})
export class OptionEditTagPersonnelComponent implements OnInit {

  private model;
  private tagPersonnelId;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private tagPersonnelService: TagPersonnelService,
      private alertService: AlertService
  ) { }

  ngOnInit(){
    this.tagPersonnelId = this.route.snapshot.params['id'];

    this.getTagPersonnel();
  }

  getTagPersonnel(){
    this.tagPersonnelService.getTagPersonnel(this.tagPersonnelId).subscribe(
        (data) => {
          this.model = data;
        },
        (err) => console.log(err)
    );
  }

  onSubmit(){
    this.tagPersonnelService.patchTagPersonnel(
        this.tagPersonnelId,
        {
          "nom" : this.model.nom,
        }
    ).subscribe(
        (reponse) => {
          this.alertService.success("Modification effectuée");
          this.router.navigate(["options/tagperso/list"]);
        },
        (err) => {
          console.log(err);
          let message = JSON.parse(err._body);
          this.alertService.error(message.message);
        }
    );
  }

}
