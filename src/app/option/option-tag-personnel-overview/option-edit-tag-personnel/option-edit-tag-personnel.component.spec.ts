import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionEditTagPersonnelComponent } from './option-edit-tag-personnel.component';

describe('OptionEditTagPersonnelComponent', () => {
  let component: OptionEditTagPersonnelComponent;
  let fixture: ComponentFixture<OptionEditTagPersonnelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionEditTagPersonnelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionEditTagPersonnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
