import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionTagPersonnelOverviewComponent } from './option-tag-personnel-overview.component';

describe('OptionTagPersonnelOverviewComponent', () => {
  let component: OptionTagPersonnelOverviewComponent;
  let fixture: ComponentFixture<OptionTagPersonnelOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionTagPersonnelOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionTagPersonnelOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
