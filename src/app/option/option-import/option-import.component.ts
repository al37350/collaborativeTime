import { Component, OnInit } from '@angular/core';
import * as Baby from 'babyparse';
import {SujetService} from "../../_services/sujet.service";
import {EnregistrementFroidStoreService} from "../../_services/EnregistrementFroidStore.service";
import {SujetDefined} from "../../_models/import/SujetDefined";
import {CustomEnregistrement} from "../../_models/import/CustomEnregistrement";
import {SujetCustom} from "../../_models/import/SujetCustom";
import {SujetCustomCollection} from "../../_models/import/SujetCustomCollection";
import {TypeTravailCustom} from "../../_models/import/TypeTravailCustom";
import {CurrentUserService} from "../../_services/currentUser.service";
import {EnregistrementService} from "../../_services/enregistrement.service";
import {AlertService} from "../../_services/alert.service";
import * as moment from 'moment';
import {Router} from "@angular/router";
import {TypeTravailSelectionDefined} from "../../_models/import/TypeTravailSelectionDefined";
import {TagCustom} from "../../_models/import/TagCustom";
import {TagPersonnelService} from "../../_services/tagPersonnel.service";
import {TagDefined} from "../../_models/import/TagDefined";
import {JourInstanceService} from "../../_services/jourInstance.service";

@Component({
    selector: 'app-option-import',
    templateUrl: './option-import.component.html',
    styleUrls: ['./option-import.component.scss']
})
export class OptionImportComponent implements OnInit {

    private sujetDefinedCollection : Array<SujetDefined>;
    private tagDefinedCollection : Array<TagDefined>;
    private customEnregistrementCollection : Array<CustomEnregistrement>;
    private sujetCustomCollection : SujetCustomCollection;
    private tagCustomCollection;

    private userId : number;

    private show : boolean = false;
    private heureFinAuto : boolean = false;

    constructor(
        private router : Router,
        private sujetService : SujetService,
        private enregistrementFroidStoreService : EnregistrementFroidStoreService,
        private currentUserService : CurrentUserService,
        private enregistrementService : EnregistrementService,
        private alertService : AlertService,
        private tagPersonnelService : TagPersonnelService,
        private jourInstanceService: JourInstanceService
    ) {
        this.sujetDefinedCollection = [];
        this.tagDefinedCollection = [];
        this.customEnregistrementCollection = [];
        this.sujetCustomCollection = new SujetCustomCollection;
        this.tagCustomCollection = [];
    }

    ngOnInit() {
        this.userId = this.currentUserService.getUser().id;

        this.tagPersonnelService.getTagsPersonnel()
            .subscribe(
                (data) => {
                    data.map(tagPerso => {
                        this.tagDefinedCollection.push(new TagDefined(tagPerso.id, tagPerso.nom))
                    });
                },
                (err) => console.log(err)
            );
    }

    onChange(inputValue) {

        let file: File = inputValue.files[0];
        let myReader: FileReader = new FileReader();
        myReader.onloadend = (e) => {
            let parsed = Baby.parse(myReader.result, {
                header: true,
                complete : (results, file) => {
                    this.show = true;
                    this.customEnregistrementCollection = results.data.map(enregistrement =>
                    {

                        let sujetCustom = this.sujetCustomCollection.getSujetCustom(enregistrement.SujetCustom);

                        sujetCustom.addTypeTravailCustom(new TypeTravailCustom(enregistrement.TypeTravailCustom));
                        /*Verifier si le sujet custom existe pas déjà*/
                        let tagCustomEnregistrementCollection = [];
                        enregistrement.Tags.split("|").map( tagCustomNom => {
                            if(tagCustomNom != ""){
                                let tagCustom = new TagCustom(tagCustomNom);
                                tagCustomEnregistrementCollection.push(tagCustom);
                                if(!this.tagCustomCollection.filter(tagCustomInArray => tagCustomInArray.nom == tagCustomNom).length){
                                    this.tagCustomCollection.push({
                                        "nom" : tagCustomNom,
                                        "objet" : tagCustom
                                    });
                                }
                            }
                        });
                        return new CustomEnregistrement(
                            enregistrement.SujetCustom,
                            enregistrement.TypeTravailCustom,
                            tagCustomEnregistrementCollection,
                            enregistrement.DureeSeconde,
                            enregistrement.DD,
                            enregistrement.MM,
                            enregistrement.YYYY

                        )
                    });
                }
            });

        };
        myReader.readAsText(file);

        this.sujetDefinedCollection = [];
        for(let sujet of this.enregistrementFroidStoreService.sujets){
            this.sujetService
                .getTypeTravails(sujet.id)
                .subscribe(
                    (data) => {
                        let arrayOfTypeTravailSelectionDefined = data.map(typeTravailSelection => new TypeTravailSelectionDefined(typeTravailSelection.id, typeTravailSelection.typeTravail.nom));

                        let sujetDefined = new SujetDefined(sujet.id,sujet.nom, arrayOfTypeTravailSelectionDefined);
                        this.sujetDefinedCollection.push(sujetDefined);
                    },
                    (err) => console.log(err)
                );
        }
    }

    onSelectSujet(event, sujetCustom) {
        sujetCustom.typeTravailCustomCollection.map( typeTravailCustom => {
            if(typeTravailCustom.typeTravailSelectionDefined){
                typeTravailCustom.typeTravailSelectionDefined = undefined;
            }
        });
    }

    onSelectTypeTravail(event, sujetCustom) {
    }

    validate(){
        let dates = [];

        let data = this.customEnregistrementCollection.map( customEnregistrement => {
            let customSujetName = customEnregistrement.customSujet;
            let customTypeTravailName = customEnregistrement.customTypeTravail;

            let sujetCorrespondanceEnregistrement = this.sujetCustomCollection.collection.find(sujetCustom => sujetCustom.nom == customSujetName);

            let typeTravailCorrespondanceEnregistrement = sujetCorrespondanceEnregistrement.typeTravailCustomCollection.find(typeTravailCustom => typeTravailCustom.nom == customTypeTravailName).typeTravailSelectionDefined;

            console.log(typeTravailCorrespondanceEnregistrement);
            let typeTravailSelectionId = typeTravailCorrespondanceEnregistrement.id;

            if(!dates.filter(dateArray => dateArray.day == customEnregistrement.day
                && dateArray.month == customEnregistrement.month
                && dateArray.year==customEnregistrement.year).length){
                dates.push({
                    day: customEnregistrement.day,
                    month: customEnregistrement.month,
                    year: customEnregistrement.year
                })
            }

            return {
                "enregistrement" : {
                    "jour" : customEnregistrement.day,
                    "mois" : customEnregistrement.month,
                    "annee" : customEnregistrement.year,
                    "dayName" : moment(new Date(customEnregistrement.year, customEnregistrement.month, customEnregistrement.day)).format('dddd'),
                    "duree" : customEnregistrement.duree,
                    "typeTravailSelection" : typeTravailSelectionId,
                    "utilisateur" : this.userId
                },
                "tags" : customEnregistrement.tagCustomCollection.map( tagCustom => {
                    return this.tagCustomCollection.find(tag => tag.nom === tagCustom.nom).tagDefined.id;
                })
            }
        });

        this.enregistrementService.postEnregistrementsListe(data).subscribe(
            (rep) => {
                if(this.heureFinAuto){
                    let datesFormatted = dates.map(dateData => moment().year(dateData.year).month(dateData.month - 1).date(dateData.day).format('YYYY-MM-DD'));
                    this.jourInstanceService.calculJoursFinAuto(this.userId, datesFormatted).subscribe(
                        (rep) => {
                            this.alertService.success("Import réussi");
                            this.router.navigate(["/options/editconfiguration"]);
                        }
                    );
                }
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }
        );
    }
}
