import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionImportComponent } from './option-import.component';

describe('OptionImportComponent', () => {
  let component: OptionImportComponent;
  let fixture: ComponentFixture<OptionImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
