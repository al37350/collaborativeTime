import { Component, OnInit } from '@angular/core';
import {AlertService} from "../../_services/alert.service";
import {UtilisateurService} from "../../_services/utilisateur.service";
import {CurrentUserService} from "../../_services/currentUser.service";

@Component({
    selector: 'app-option-edit-configuration',
    templateUrl: './option-edit-configuration.component.html',
    styleUrls: ['./option-edit-configuration.component.scss']
})
export class OptionEditConfigurationComponent implements OnInit {
    private userId: number;
    private configuration;
    private configurationJour;
    private configurationJourIndex;

    private links = [];
    private jours = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

    constructor(
        private alertService: AlertService,
        private utilisateurService: UtilisateurService,
        private currentUserService : CurrentUserService
    ) {
        this.jours.map((jour, index) =>{
           this.links.push({
               name : jour,
               index : index
           });
        });
    }

    ngOnInit() {
        this.userId = this.currentUserService.getUser().id;
        this.utilisateurService.getUtilisateurConfiguration(this.userId).subscribe(
            (data) => {
                this.configuration = data;
                this.configurationJour = data.lundi;
                this.configurationJourIndex = 0;
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.error(message.message);
            }
        );
    }

    onSubmit() {
        let jourName = this.jours[this.configurationJourIndex].toLowerCase();
        this.utilisateurService.postConfigurationJour(
            this.userId, jourName,
            {
                "debutAM" : this.configurationJour.debutAM,
                "finAM" : this.configurationJour.finAM,
                "debutPM" : this.configurationJour.debutPM,
                "finPM" : this.configurationJour.finPM
            }
        ).subscribe(
            (reponse) => {
                this.alertService.success("Modification effectuée");
                this.configuration[jourName] = reponse;
                this.configurationJour = reponse;
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }
        );
    }

    dayChange(linkEvent){
        this.configurationJourIndex = linkEvent.index;
        this.configurationJour = this.configuration[linkEvent.name.toLowerCase()];
    }

    deleteJourConf(){
        this.configurationJour.debutAM = 'toDelete';
        this.configurationJour.finAM = 'toDelete';
        this.configurationJour.debutPM = 'toDelete';
        this.configurationJour.finPM = 'toDelete';
        this.onSubmit();
    }

    addJourConf(){
        this.configurationJour = {};
    }

}
