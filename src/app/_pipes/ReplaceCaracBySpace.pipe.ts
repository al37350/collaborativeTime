import { Pipe, PipeTransform } from '@angular/core';
import 'rxjs/add/operator/map';

@Pipe({name: 'replaceCaracBySpace'})
export class ReplaceCaracBySpacePipe implements PipeTransform {
    transform(value : string, args:string[]) : any {
        return value.replace(/(_|-|,)+/g, ' ');
    }
}