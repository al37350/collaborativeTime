import { Pipe, PipeTransform } from '@angular/core';
import 'rxjs/add/operator/map';

@Pipe({name: 'secondsFormat'})
export class SecondsFormatPipe implements PipeTransform {
    transform(value, args:string[]) : any {
        let sec_num : number = Math.abs(Number.parseInt(value));
        let hours   = Math.floor(sec_num / 3600);
        let minutes = Math.floor((sec_num - (hours * 3600)) / 60);

        let res : string = "";

        hours  < 10 ? res+="0"+hours : res+= hours;
        res += "h";
        minutes  < 10 ? res+="0"+minutes : res+= minutes;

        return res;
    }
}