import { Injectable } from '@angular/core';

@Injectable()
export class CurrentUserService{

    private currentUser;

    constructor(){
        this.currentUser = localStorage.getItem("currentUser");
        if(this.currentUser != null){
            this.currentUser = JSON.parse(this.currentUser);
        }
    }

    getAll(){
        return this.currentUser;
    }

    removeAll(){
        localStorage.removeItem("currentUser");
    }

    exist(){
        return !(this.currentUser === null);
    }

    getId() : number{
        return this.currentUser.id;
    }

    getValue() : string{
        return this.currentUser.value;
    }

    getCreatedAt() : string{
        return this.currentUser.createdAt;
    }

    getExpiredAt(){
        if(this.exist()){
            return this.currentUser.expiredAt;
        }
        return false;
    }

    getUser(){
        return this.currentUser.user;
    }

    setUser(data: any) {
        let dataJson = JSON.stringify(data);
        this.currentUser = JSON.parse(dataJson);
        localStorage.setItem('currentUser', dataJson);
    }

    tokenIsValid(){
        if(!this.getExpiredAt()){
            return false;
        }
        return new Date() < new Date(this.getExpiredAt());
    }
}