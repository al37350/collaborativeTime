import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";
import * as moment from 'moment';
import {JourInstanceService} from "./jourInstance.service";
import {AlertService} from "./alert.service";
import {SharedPeriodeTempsService} from "./shared.periodeTemps.service";

@Injectable()
export class EnregistrementService{
    baseUrl: string;

    constructor(
        private http: HttpClient,
        private jourInstanceService : JourInstanceService,
        private sharedPeriodeTempsService : SharedPeriodeTempsService,
        private alertService : AlertService
    ){
        this.baseUrl = ApiURL+'/API/enregistrement/';
    }

    getEnregistrement(id: number) {
        return this.http.get(this.baseUrl + id).map(res => res.json());
    }

    postEnregistrement(data) {
        return this.http.post(this.baseUrl, data).map(res => res.json());
    }

    postEnregistrementDuree(data){
        return this.http.post(this.baseUrl +"duree", data).map(res => res.json());
    }

    /**
     *
     * @param data
     * {
                "jour" : 12,
                "mois" : 4,
                "annee" : 2017,
                "duree" : 17500,
                "typeTravail" : 24,
                "sujet" : 3,
                "utilisateur" : 43
            }
     */
    postEnregistrementsListe(data){
        return this.http.post(this.baseUrl +"liste", data).map(res => res.json());
    }

    patchEnregistrement(id: number, data) {
        let minEnregistrement = 60 * 1;

        if(data.duree < minEnregistrement){
            data.heureFin = data.heureFin+(minEnregistrement-data.duree)
            data.duree = minEnregistrement;
        }

        return this.http.patch(this.baseUrl + id, data).map(res => res.json());
    }

    deleteEnregistrement(id: number) {
        return this.http.delete(this.baseUrl + id).map(res => res.json());
    }

    postEnregistrementTagPerso(idEnregistrement: number, idTagPerso: number) {
        return this.http.post(this.baseUrl+idEnregistrement+ "/tagpersonnel/" +idTagPerso, {}).map(res => res.json());
    }

    deleteEnregistrementTagPerso(idEnregistrement: number, idTagPerso: number) {
        return this.http.delete(this.baseUrl+idEnregistrement+ "/tagpersonnel/" +idTagPerso).map(res => res.json());
    }

    getEnregistrementsJourInstance(jourInstanceId : number){
        return this.http.get(this.baseUrl + '?jourInstance='+jourInstanceId).map(res => res.json());
    }

    /**
     *
     * @param idEnregistrement
     * @param data
     *
     * data = {
     *      "heureFin" : "12:30,
     *      "duree" : 125,
     *      "typeTravail" : 12,
     *      "sujet" : 50
     */
    saveFinEnregistrement(idEnregistrement, data, tagsPersoId = null){
        this.patchEnregistrement(idEnregistrement, data)
            .subscribe(
                (rep) => {
                    this.alertService.success("Enregistrement pout le sujet '" + rep.sujet.nom +"' créé avec pour type de travail '" + rep.typeTravail.nom + "'");
                    if(tagsPersoId){
                        this.addTagPersoEnregistrement(idEnregistrement, tagsPersoId);
                    }

                    /* Si la date affichée sur le calendar est celle du jour de l'enregistrement alors on ajoute l'enregistrement */
                    if(moment().format('L') === this.jourInstanceService.getMoment().format('L')){
                        this.sharedPeriodeTempsService.replacePeriodeTemps(rep);
                    }
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    addTagPersoEnregistrement(idEnregistrement, tagsPersoId){
        tagsPersoId.map(idTagPerso => {
           this.postEnregistrementTagPerso(idEnregistrement, idTagPerso)
                .subscribe(
                    (rep) => {},
                    (err) => {
                        console.log(err);
                        let message = JSON.parse(err._body);
                        this.alertService.error(message.message);
                    }
                );
        });
    }
}