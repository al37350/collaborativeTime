import {Injectable} from '@angular/core';
import "rxjs/add/operator/map";
import {Subject} from "rxjs";

@Injectable()
export class DateSelectedPickerService{

    private date;
    public dateChange : Subject<string> = new Subject<string>();

    constructor(){}

    setDate(date){
        this.date = date;
        this.dateChange.next(this.date);
    }

    getDate(){
        return this.date;
    }
}
