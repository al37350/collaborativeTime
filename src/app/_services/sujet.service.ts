import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class SujetService{

    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/sujet/';
    }

    getSujet(id : number){
        return this.http.get(this.baseUrl + id).map(res => res.json())
    }

    getTypeTravails(id: number) {
        return this.http.get(this.baseUrl + id + "/typetravail").map(res => res.json())
    }

    getutilisateur(id: number) {
        return this.http.get(this.baseUrl + id + "/utilisateur").map(res => res.json())
    }

    postSujet(data) {
        return this.http.post(this.baseUrl, data).map(res => res.json())
    }

    postSujetListe(userId, data){
        return this.http.post(this.baseUrl +"utilisateur/"+ userId +"/liste", data).map(res => res.json());
    }

    deleteSujet(id: number) {
        return this.http.delete(this.baseUrl + id).map(res => res.json());
    }

    patchSujet(sujetId: number, data) {
        return this.http.patch(this.baseUrl + sujetId, data).map(res => res.json())
    }

    addUtilisateur(idSujet: number, idUtilisateur : number) {
        return this.http.post(this.baseUrl +idSujet + "/utilisateur/" + idUtilisateur, {}).map(res => res.json())
    }

    deleteUser(idSujet: number, idUtilisateur : number) {
        return this.http.delete(this.baseUrl +idSujet + "/utilisateur/" + idUtilisateur).map(res => res.json())
    }

    getSujetEnregistrements(idSujet: number) {
        return this.http.get(this.baseUrl + idSujet + "/enregistrement").map(res => res.json())
    }
}
