import {Injectable} from '@angular/core';
import "rxjs/add/operator/map";
import {CurrentUserService} from "./currentUser.service";
import {NotificationService} from "./notification.service";

@Injectable()
export class SharedNotificationService{

    private userId;
    notifications = [];

    constructor(
        private currentUserService : CurrentUserService,
        private notificationService : NotificationService
    ){
        this.loadNotification();
    }

    loadNotification(){

        this.userId = this.currentUserService.getUser().id;
        this.notificationService.getNotifications(this.userId).subscribe(
            (data) => {
                this.notifications =  data;
            },
            (err) => console.log(err)
        );
    }
}
