import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class TypeTravailSelectionService{

    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/typetravailselection/';
    }

    getTypeTravailSelections() {
        return this.http.get(this.baseUrl).map(res => res.json());
    }

    deleteTypeTravailSelection(id : number){
        return this.http.delete(this.baseUrl + id).map(res => res.json());
    }

    getTypeTravailSelectionFromSujet(id : number){
        return this.http.get(this.baseUrl +'sujet/'+ id).map(res => res.json());
    }

    postTypeTravailSelection(data) {
        return this.http.post(this.baseUrl, data).map(res => res.json());
    }

    getCaracteristiques(typeTravailSelectionId: number){
        return this.http.get(this.baseUrl + typeTravailSelectionId + "/caracteristique").map(res => res.json());
    }

    setCaracteristique(typeTravailSelectionId: number, idCaracteristique: number) {
        return this.http.post(this.baseUrl + typeTravailSelectionId + "/caracteristique/" +idCaracteristique, {}).map(res => res.json());
    }

    deleteCaracteristique(typeTravailSelectionId: number, idCaracteristique: number) {
        return this.http.delete(this.baseUrl + typeTravailSelectionId + "/caracteristique/" +idCaracteristique).map(res => res.json());
    }

}
