import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class GroupeService{
    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/groupe/';
    }

    getGroupes(){
        return this.http.get(this.baseUrl).map(res => res.json())
    }

    getGroupe(id: number) {
        return this.http.get(this.baseUrl + id).map(res => res.json())
    }

    setDroit(groupeId: number, droitId: number) {
        return this.http.post(this.baseUrl + groupeId + "/droit/" +droitId, {}).map(res => res.json());
    }

    deleteDroit(groupeId: number, droitId: number) {
        return this.http.delete(this.baseUrl + groupeId + "/droit/" +droitId).map(res => res.json());
    }
}