import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class PeriodeOffService{
    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/periodeoff/';
    }

    postPeriodeOff(data) {
        return this.http.post(this.baseUrl, data).map(res => res.json());
    }

    patchPeriodeOff(id: number, data) {
        return this.http.patch(this.baseUrl + id, data).map(res => res.json());
    }

}