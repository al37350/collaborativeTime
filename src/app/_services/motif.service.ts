import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class MotifService{

    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/motif/';
    }

    getMotifs() {
        return this.http.get(this.baseUrl).map(res => res.json());
    }

    getMotif(id : number){
        return this.http.get(this.baseUrl + id).map(res => res.json());
    }

    patchMotif(id : number, data){
        return this.http.patch(this.baseUrl + id, data).map(res => res.json());
    }

    deleteMotif(id : number){
        return this.http.delete(this.baseUrl + id).map(res => res.json());
    }

    postMotif(data) {
        return this.http.post(this.baseUrl, data ).map(res => res.json());
    }
}
