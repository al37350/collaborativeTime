import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class UtilisateurService{

    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/utilisateur/';
    }

    create(user){
        return this.http.post(ApiURL+'/API/utilisateur/' , user).map(res => res.json())
    }

    getUtilisateurs(){
        return this.http.get(this.baseUrl).map(res => res.json())
    }

    getUtilisateur(id : number){
        return this.http.get(this.baseUrl + id).map(res => res.json())
    }

    patchUtilisateur(id: number, data) {

        return this.http.patch(
            this.baseUrl + id,
            data
        ).map(res => res.json())
    }

    deleteUtilisateur(id: number) {
        return this.http.delete(
            this.baseUrl + id
        ).map(res => res.json())
    }

    getUtilisateurSujetSouscris(id : number, filtre : string ="") {
        return this.http.get(this.baseUrl + id + "/sujet?"+ filtre).map(res => res.json());
    }

    getUtilisateurProprietaireSujet(id: number, filtre : string) {
        return this.http.get(this.baseUrl + id + "/proprietairesujet?"+ filtre).map(res => res.json());
    }

    getUtilisateurGroupe(id: number) {
        return this.http.get(this.baseUrl + id + "/groupe").map(res => res.json());
    }

    setGroupe(id: number, idGroupe :number) {
        return this.http.post(this.baseUrl + id + "/groupe/" +idGroupe, {}).map(res => res.json());
    }

    deleteGroupe(id: number, idGroupe :number) {
        return this.http.delete(this.baseUrl + id + "/groupe/" +idGroupe).map(res => res.json());
    }

    getUtilisateurConfiguration(id: number) {
        return this.http.get(this.baseUrl + id + "/configuration").map(res => res.json());
    }

    getUtilisateurEnregistrementOuvert(id: number) {
        return this.http.get(this.baseUrl + id + "/enregistrement?ouvert=true").map(res => res.json());
    }

    getUtilisateurTagPersonnel(id: number) {
        return this.http.get(this.baseUrl + id + "/tagpersonnel").map(res => res.json());
    }

    postConfigurationJour(userId: number, dayOfWeek: string, data) {
        return this.http.post(this.baseUrl + userId + "/configuration/" +dayOfWeek, data).map(res => res.json())
    }

    getUtilisateurEnregistrementJourInstance(id, jourInstanceId){
        return this.http.get(this.baseUrl + id + '/enregistrement?jourInstance='+jourInstanceId).map(res => res.json());
    }

}
