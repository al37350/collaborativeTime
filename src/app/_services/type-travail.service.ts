import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class TypeTravailService{

    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/typetravail/';
    }

    getTypeTravails() {
        return this.http.get(this.baseUrl).map(res => res.json());
    }

    deleteTypeTravail(id : number){
        return this.http.delete(this.baseUrl + id).map(res => res.json());
    }

    postTypeTravail(data) {
        return this.http.post(this.baseUrl, data).map(res => res.json());
    }

    getTypeTravail(id: number) {
        return this.http.get(this.baseUrl + id).map(res => res.json());
    }

    setCaracteristique(typeTravailId: number, idCaracteristique: number) {
        return this.http.post(this.baseUrl + typeTravailId + "/caracteristique/" +idCaracteristique, {}).map(res => res.json());
    }

    deleteCaracteristique(typeTravailId: number, idCaracteristique: number) {
        return this.http.delete(this.baseUrl + typeTravailId + "/caracteristique/" +idCaracteristique).map(res => res.json());
    }

    patchTypeTravail(typeTravailId : number, data) {
        return this.http.patch(this.baseUrl + typeTravailId, data).map(res => res.json());
    }
}
