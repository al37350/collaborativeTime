import {EventEmitter, Injectable} from '@angular/core';
import "rxjs/add/operator/map";
import {JourInstanceService} from "./jourInstance.service";
import {PeriodeTempsService} from "./periodeTemps.service";
import {AlertService} from "./alert.service";

@Injectable()
export class SharedPeriodeTempsService{

    private jourInstance;
    periodesTemps = [];

    periodesTempsEmitter = new EventEmitter<any>();

    constructor(
        private alertService : AlertService,
        private periodeTempsService : PeriodeTempsService,
        private jourInstanceService : JourInstanceService,
    ){
        this.jourInstanceService.jourInstance$.subscribe(
            (data) => {
                if(Object.keys(data).length) {
                    this.jourInstance = data;
                    this.loadPeriodeTemps();
                }
            },
            (err) => console.log(err)
        );
    }

    loadPeriodeTemps(){
        this.periodeTempsService
            .getPeriodeTempsJourInstance(this.jourInstance.id)
            .subscribe(
                (data) => {
                    this.periodesTemps = data;
                    this.periodesTempsEmitter.emit(data);
                },
                        (err) => {
                            console.log(err);
                            let message = JSON.parse(err._body);
                            this.alertService.JSONError(message);
                        }
            );
    }

    replacePeriodeTemps(newPeriodeTemps : any){
        this.periodesTemps[this.periodesTemps.findIndex(el => el.id === newPeriodeTemps.id)] = newPeriodeTemps;
        this.periodesTempsEmitter.emit(this.periodesTemps);
    }

    deletePeriodeTemps(id : number){
        this.periodesTemps = this.periodesTemps.filter(el => el.id != id);
        this.periodesTempsEmitter.emit(this.periodesTemps);
    }

    addPeriodeTemps(newPeriodeTemps : any){
        this.periodesTemps.push(newPeriodeTemps);
        this.periodesTempsEmitter.emit(this.periodesTemps);
    }
}
