import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {CurrentUserService} from "./currentUser.service";

@Injectable()
export class HttpClient {

    constructor(
        private http: Http,
        private currentUserService : CurrentUserService
    ) {}

    createAuthorizationHeader(headers: Headers) {
        headers.append('X-Auth-Token', this.currentUserService.getValue());
    }

    get(url) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.get(url, {
            headers: headers
        });
    }

    post(url, data) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.post(url, data, {
            headers: headers
        });
    }

    put(url, data) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.put(url, data, {
            headers: headers
        });
    }

    patch(url, data) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.patch(url, data, {
            headers: headers
        });
    }

    delete(url: string) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.delete(url, {
            headers: headers
        });
    }
}