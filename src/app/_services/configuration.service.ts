import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class ConfigurationService{

    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/configuration/';
    }

    patchConfiguration(configurationId: number, data) {
        return this.http.patch(this.baseUrl + configurationId, data).map(res => res.json())
    }
}
