import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class TagPersonnelService{

    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/tagpersonnel/';
    }

    getTagsPersonnel() {
        return this.http.get(this.baseUrl).map(res => res.json());
    }
    getTagPersonnel(id: number) {
        return this.http.get(this.baseUrl + id).map(res => res.json());
    }

    deleteTagPersonnel(id : number){
        return this.http.delete(this.baseUrl + id).map(res => res.json());
    }

    postTagPersonnel(data) {
        return this.http.post(this.baseUrl, data ).map(res => res.json());
    }

    patchTagPersonnel(id: number, data) {
        return this.http.patch(this.baseUrl + id, data ).map(res => res.json());
    }
}
