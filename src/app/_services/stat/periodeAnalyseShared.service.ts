import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import * as moment from 'moment';
import {BehaviorSubject} from "rxjs";

@Injectable()
export class PeriodeAnalyseShared{

    private debutMoment;
    public debutEmitter;

    private finMoment;
    public finEmitter;

    constructor(){
        this.debutEmitter = new BehaviorSubject(moment().subtract(30,'days'));
        this.finEmitter = new BehaviorSubject(moment());
    }

    public set debut(moment){
        this.debutMoment = moment;
        this.debutEmitter.next(this.debutMoment);
    }

    public get debut(){
        return this.debutMoment
    };

    public set fin(moment){
        this.finMoment = moment;
        this.finEmitter.next(this.finMoment);
    }

    public get fin(){
        return this.finMoment;
    }
}