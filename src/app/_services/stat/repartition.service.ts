/**
 * Created by al on 18/05/17.
 */
import { Injectable } from '@angular/core';
import { ApiURL } from '../../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "../HttpClient";

@Injectable()
export class RepartitionService{

    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/stat/repartition/';
    }

    getFamilleRepartition(debut : string, fin:string) {
        return this.http.get(this.baseUrl + 'famille?debut='+debut+'&fin='+fin).map(res => res.json())
    }

    getTagPersonnelRepartition(userId: number,debut : string, fin:string) {
        return this.http.get(this.baseUrl + 'utilisateur/'+userId+'/tagpersonnel?debut='+debut+'&fin='+fin).map(res => res.json())
    }

    getTagPersonnelSujetRepartition(userId: number, sujetId: number, debut : string, fin:string) {
        return this.http.get(this.baseUrl + 'utilisateur/'+userId+'/sujet/'+sujetId+'/tagpersonnel?debut='+debut+'&fin='+fin).map(res => res.json())
    }

    getFamilleSujetRepartition(id : number, debut : string, fin:string){
        return this.http.get(this.baseUrl + 'famille/'+id+'?debut='+debut+'&fin='+fin).map(res => res.json())
    }

    getTypeTravailSujetRepartition(id : number, debut : string, fin:string){
        return this.http.get(this.baseUrl + 'sujet/'+id+'?debut='+debut+'&fin='+fin).map(res => res.json())
    }

    getTypeTravailStats(debut : string, fin:string){
        return this.http.get(ApiURL+'/API/stat/typetravail?debut='+debut+'&fin='+fin).map(res => res.json())
    }

    getCaracteristiqueFamilleStats(familleId, debut : string, fin:string){
        return this.http.get(ApiURL+'/API/stat/typetravail/famille/'+familleId+'?debut='+debut+'&fin='+fin).map(res => res.json())
    }

    getCaracteristiqueSujetStats(sujetId, debut : string, fin:string){
        return this.http.get(ApiURL+'/API/stat/typetravail/sujet/'+sujetId+'?debut='+debut+'&fin='+fin).map(res => res.json())
    }

    getRapportUtilisateur(utilisateurId, debut : string, fin:string){
        return this.http.get(ApiURL+'/API/stat/report/utilisateur/'+utilisateurId+'?debut='+debut+'&fin='+fin).map(res => res.json())
    }

    getCaracteristiqueStats(caracteristiqueId, debut : string, fin:string){
        return this.http.get(ApiURL+'/API/stat/caracteristique/'+caracteristiqueId+'?debut='+debut+'&fin='+fin).map(res => res.json());
    }

    getHistoSujet(sujetId, debut:string, fin:string){
        return this.http.get(ApiURL+'/API/stat/histo/sujet/'+sujetId+'?debut='+debut+'&fin='+fin).map(res => res.json());
    }


}