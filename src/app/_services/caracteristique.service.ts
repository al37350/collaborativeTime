import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class CaracteristiqueService{

    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/caracteristique/';
    }

    getCaracteristiques() {
        return this.http.get(this.baseUrl).map(res => res.json());
    }

    getCaracteristique(id: number) {
        return this.http.get(this.baseUrl + id).map(res => res.json());
    }

    patchCaracteristique(id: number, data){
        return this.http.patch(this.baseUrl + id, data).map(res => res.json());

    }

    deleteCaracteristique(id : number){
        return this.http.delete(this.baseUrl + id).map(res => res.json());
    }

    postCaracteristique(data) {
        return this.http.post(this.baseUrl, data ).map(res => res.json());
    }
}
