import {Injectable, EventEmitter} from '@angular/core';
import "rxjs/add/operator/map";
import {BehaviorSubject, ReplaySubject} from "rxjs";

@Injectable()
export class EnregistrementSharedService{
    enregistrementUpdated: ReplaySubject<number> = new ReplaySubject<number>(1);

    private enregistrementId;

    setEnregistrement(id){
        this.enregistrementId = id;
        this.enregistrementUpdated.next(id);
    }

    getEnregistrement(){
        return this.enregistrementId;
    }
}