import {Injectable} from '@angular/core';
import "rxjs/add/operator/map";
import {UtilisateurService} from "./utilisateur.service";
import {CurrentUserService} from "./currentUser.service";
import {AlertService} from "./alert.service";
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { BehaviorSubject, Observable } from "rxjs";
import {Subject} from "rxjs/Subject";

@Injectable()
export class EnregistrementFroidStoreService{

    private _sujets: any[] = [];
    public sujetEmitter = new BehaviorSubject([]);

    private _typesTravail: any[] = [];

    private _tagsPersonnels: IMultiSelectOption[] = [];

    private _enregistrementExistant;
    public enregistrementEmitter = new Subject();

    constructor(
        private utilisateurService : UtilisateurService,
        private currentUserService : CurrentUserService,
        private alertService : AlertService
    ){
        this.loadSujets();
        this.loadTagsPersonnels();
        this.loadEnregistrementExistant();
    }

    addSujet(sujet: any){
        this._sujets.push(sujet);
        this.sujetEmitter.next(this._sujets);
    }

    removeSujet(idSujet: number) {
        this._sujets =this._sujets.filter(item => item.id != idSujet);
        this.sujetEmitter.next(this._sujets);
    }

    public get sujets(){
        return this._sujets;
    }

    public set sujets(sujets){
        this._sujets = sujets;
    }

    get enregistrementExistant(){
        return this._enregistrementExistant;
    }

    get tagsPersonnels(){
        return this._tagsPersonnels;
    }

    private loadSujets(){
        this.utilisateurService
            .getUtilisateurSujetSouscris(this.currentUserService.getUser().id)
            .subscribe(
                (data) => {
                    this._sujets = data;
                    this.sujetEmitter.next(this._sujets);
                    this.loadTypesTravail();
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    private loadTagsPersonnels(){
        this.utilisateurService
            .getUtilisateurTagPersonnel(this.currentUserService.getUser().id)
            .subscribe(
                (rep) => {
                    rep.map( tag => {
                        this._tagsPersonnels.push({
                            id :  tag.id,
                            name : tag.nom
                        });
                    });
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    private loadEnregistrementExistant(){
        this.utilisateurService
            .getUtilisateurEnregistrementOuvert(this.currentUserService.getUser().id)
            .subscribe(
                rep => {
                    this._enregistrementExistant = rep;
                    this.enregistrementEmitter.next(this._enregistrementExistant);
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    private loadTypesTravail(){

    }
}
