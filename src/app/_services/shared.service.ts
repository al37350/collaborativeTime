import {Injectable} from '@angular/core';
import "rxjs/add/operator/map";
import {Link} from "../_models/link";

@Injectable()
export class SharedService{
    enregistrementFroidPossible : boolean = true;

    groupes: any [];
    links: Link []= [];

    constructor(){}

    setGroupes(groupes: any[]){
        this.groupes = groupes;
        this.generateLinks();
    }

    putGroupe(groupes : any){
        let currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.groupes = groupes;
        currentUser.user.groupes = this.groupes;
        localStorage.setItem("currentUser", JSON.stringify(currentUser));

        this.generateLinks();
    }

    putGroupeDroits(groupe : any){
        if(this.groupes.filter(item => item.id == groupe.id)){
            let tmp = this.groupes.filter(item => item.id != groupe.id);
            tmp.push(groupe);
            this.groupes = tmp;
            let currentUser = JSON.parse(localStorage.getItem("currentUser"));
            currentUser.user.groupes = this.groupes;
            localStorage.setItem("currentUser", JSON.stringify(currentUser));

            this.generateLinks();
        }
    }


    generateLinks(){
        this.links.length = 0;
        let menu = new Set();
        this.groupes.map(groupe => {
            groupe.droits.map( droit => {
                menu.add(droit.nom);
            });
        });

        menu.forEach((item) => {
            switch (item){
                case "Dashboard" :
                    this.links.push(new Link('Tableau de bord',require('../_ressources/dashboard.svg'),"/dashboard", 6));
                    break;
                case "Sujets" :
                    this.links.push(new Link('Sujets', require('../_ressources/project.svg'), "/projects", 5));
                    break;
                case "Admin" :
                    this.links.push(new Link('Admin panel', require('../_ressources/admin.svg'), "/admin", 4));
                    break;
                case "Report" :
                    this.links.push(new Link('Rapports', require('../_ressources/report.svg'), "/reports", 3));
                    break;
                case "Options" :
                    this.links.push(new Link('Options',  require('../_ressources/options.svg'), "/options", 2));
                    break;
            }
        });

        this.links.push(new Link('Déconnexion', require('../_ressources/logout.svg'), "/login", 1));

        this.links.sort((linkA,linkB) => {
            return linkB.poid - linkA.poid
        });
    }
}
