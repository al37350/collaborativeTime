import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { ApiURL } from '../globals';
import 'rxjs/add/operator/map'
import {CurrentUserService} from "./currentUser.service";

@Injectable()
export class AuthenticationService {

    baseUrl : string;

    constructor(
        private http: Http,
        private currentUserService: CurrentUserService
    ) {
        this.baseUrl = ApiURL +'/API/auth-tokens/';
    }

    login(username: string, password: string) {
        return this.http.post(this.baseUrl, { login: username, password: password })
            .map((response: Response) => {
                this.currentUserService.setUser(response.json());
            });
    }
}
