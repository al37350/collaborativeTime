import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class NotificationService{

    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/notification/';
    }

    getNotifications(utilisateurId : number) {
        return this.http.get(this.baseUrl + "utilisateur/" + utilisateurId).map(res => res.json());
    }

    patchNotification(notifId : number, data : any) {
        return this.http.patch(this.baseUrl +  notifId, data).map(res => res.json());
    }
}
