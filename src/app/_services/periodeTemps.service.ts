import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class PeriodeTempsService{
    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/periodestemps/';
    }

    getPeriodeTempsJourInstance(jourInstanceId : number){
        return this.http.get(this.baseUrl + '?jourInstance='+jourInstanceId).map(res => res.json());
    }

    recollerPeriodeTempsJourInstance(jourInstanceId : number){
        return this.http.post(this.baseUrl + 'jourinstance/'+jourInstanceId+'/recaler', {}).map(res => res.json());
    }

    deletePeriodeTemps(id: number) {
        return this.http.delete(this.baseUrl + id).map(res => res.json());
    }

    patchPeriodeTemps(id: number, data) {
        return this.http.patch(this.baseUrl + id, data).map(res => res.json());
    }
}