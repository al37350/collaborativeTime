import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";
import * as moment from 'moment';
import {CurrentUserService} from "./currentUser.service";
import { Subject }    from 'rxjs/Subject';
import {ReplaySubject} from "rxjs";
import {AlertService} from "./alert.service";

@Injectable()
export class JourInstanceService{
    private baseUrl: string;
    private userId : number;
    private moment;
    private jourInstance = new ReplaySubject(1);

    jourInstance$ = this.jourInstance.asObservable();

    constructor(
        private http: HttpClient,
        private currentUserService : CurrentUserService,
        private alertService : AlertService
    ){
        this.baseUrl = ApiURL+'/API/jourinstance/';
        moment.locale('fr');
        this.setMoment(moment());
    }

    public setMoment(moment){
        this.moment = moment;
        this.getJourInstanceAndNotify();
    }

    public getMoment(){
        return this.moment;
    }

    public getJourInstanceFromMoment(moment){
        this.userId = this.currentUserService.getUser().id;
        return this.http.post(this.baseUrl, {
            "jour" : moment.date(),
            "mois" : moment.month()+1,
            "annee" : moment.year(),
            "dayName" :  moment.format('dddd'),
            "utilisateur" : this.userId
        }).map(res => res.json());
    }

    public getJourInstanceAndNotify() {
        this.userId = this.currentUserService.getUser().id;
        return this.http.post(this.baseUrl, {
            "jour" : this.moment.date(),
            "mois" : this.moment.month()+1,
            "annee" : this.moment.year(),
            "dayName" :  this.moment.format('dddd'),
            "utilisateur" : this.userId
        }).map(res => res.json()).subscribe(
            (data) => {
                this.jourInstance.next(data);
            }
        );
    }

    public validePeriodeMoisInstance(moment){
        this.userId = this.currentUserService.getUser().id;
        return this.http.post(this.baseUrl + "periodevalide", {
            "mois" : moment.month()+1,
            "annee" : moment.year(),
            "utilisateur" : this.userId
        }).map(res => res.json())
    }

    public patchJourInstance(jourInstanceId: number, data) {
        return this.http.patch(this.baseUrl + jourInstanceId, data).map(res => res.json()).subscribe(
            (data) => {
                this.alertService.success("Nouveau horaires du jour enregistrés.");
                this.jourInstance.next(data);
            }
        );
    }

    /**
     *
     * @param userId
     * @param dateFormattedArray array 2017-09-24
     */
    public calculJoursFinAuto(userId: number, dateFormattedArray){
        return this.http.patch(this.baseUrl+'calculfin', { user : userId, dateFormattedArray: dateFormattedArray}).map(res => res.json());
    }

}
