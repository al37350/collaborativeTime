import { Injectable } from '@angular/core';
import { ApiURL } from '../globals';
import "rxjs/add/operator/map";
import {HttpClient} from "./HttpClient";

@Injectable()
export class FamilleService{

    baseUrl: string;

    constructor(public http: HttpClient){
        this.baseUrl = ApiURL+'/API/famille/';
    }

    getFamilles() {
        return this.http.get(this.baseUrl).map(res => res.json());
    }

    getFamille(id : number) {
        return this.http.get(this.baseUrl + id).map(res => res.json());
    }

    getFamilleSujets(id : number) {
        return this.http.get(this.baseUrl +id+"/sujet").map(res => res.json());
    }


    deleteFamille(id : number){
        return this.http.delete(this.baseUrl + id).map(res => res.json());
    }

    postFamille(data) {
        return this.http.post(this.baseUrl, data ).map(res => res.json());
    }

    patchFamille(id : number, data) {
        return this.http.patch(this.baseUrl + id, data ).map(res => res.json());
    }
}
