import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AlertService {
    private subject = new Subject<any>();
    private keepAfterNavigationChange = false;

    constructor(private router: Router) {
        // clear alert message on route change
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    this.keepAfterNavigationChange = false;
                } else {
                    // clear alert
                    this.subject.next();
                }
            }
        });
    }

    success(message: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
    }

    error(message: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message });
    }

    JSONError(message){
        message.errors = Object.keys(message.errors).map(key => message.errors[key]);

        console.log(message);

        /*let errors = message.errors.children;

        let keys= Object.keys(errors);
        keys.map(key => {
            console.log(key);
            // let keyValues =
            errors[key].errors.map(value => {
               console.log(value);
            });
        });*/

        this.subject.next({ type: 'error', text: JSON.parse(JSON.stringify(message)) , json: true });
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}