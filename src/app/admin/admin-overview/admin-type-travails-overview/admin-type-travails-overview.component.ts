import { Component, OnInit } from '@angular/core';
import {SujetService} from "../../../_services/sujet.service";
import {AlertService} from "../../../_services/alert.service";
import {TypeTravailService} from "../../../_services/type-travail.service";

@Component({
    selector: 'app-admin-type-travails-overview',
    templateUrl: './admin-type-travails-overview.component.html',
    styleUrls: ['./admin-type-travails-overview.component.scss']
})
export class AdminTypeTravailsOverviewComponent implements OnInit {

    constructor() { }

    ngOnInit() {}
}
