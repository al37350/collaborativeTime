import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTypeTravailsOverviewComponent } from './admin-type-travails-overview.component';

describe('AdminTypeTravailsOverviewComponent', () => {
  let component: AdminTypeTravailsOverviewComponent;
  let fixture: ComponentFixture<AdminTypeTravailsOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTypeTravailsOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTypeTravailsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
