import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTypeTravailsListOverviewComponentComponent } from './admin-type-travails-list-overview-component.component';

describe('AdminTypeTravailsListOverviewComponentComponent', () => {
  let component: AdminTypeTravailsListOverviewComponentComponent;
  let fixture: ComponentFixture<AdminTypeTravailsListOverviewComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTypeTravailsListOverviewComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTypeTravailsListOverviewComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
