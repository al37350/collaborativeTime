import { Component, OnInit } from '@angular/core';
import {AlertService} from "../../../../_services/alert.service";
import {TypeTravailService} from "../../../../_services/type-travail.service";

@Component({
    selector: 'app-admin-type-travails-list-overview-component',
    templateUrl: './admin-type-travails-list-overview-component.component.html',
    styleUrls: ['./admin-type-travails-list-overview-component.component.scss']
})
export class AdminTypeTravailsListOverviewComponentComponent implements OnInit {

    private typeTravails : any;
    private typetravail = "";

    constructor(
        private alertService : AlertService,
        private typeTravailService : TypeTravailService
    ) { }

    ngOnInit() {
        this.typeTravailService
            .getTypeTravails()
            .subscribe(
                (data) => {
                    this.typeTravails = data;
                },
                (err) => console.log(err)
            );
    }

    deleteTypeTravail(id : number){

        this.typeTravailService
            .deleteTypeTravail(id)
            .subscribe(
                (data) => {
                    this.typeTravails =this.typeTravails.filter(item => item.id != id)
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    addTypeTravail(){
        this.typeTravailService.postTypeTravail(
            {
                "nom" : this.typetravail
            }
        ).subscribe(
            (reponse) => {
                this.typetravail ="";
                this.typeTravails.push(reponse);
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }
        );
    }
}
