import { Component, OnInit } from '@angular/core';
import {Groupe} from "../../../../_models/groupe";
import {GroupeService} from "../../../../_services/groupe.service";

@Component({
  selector: 'app-admin-groupes-list-overview',
  templateUrl: './admin-groupes-list-overview.component.html',
  styleUrls: ['./admin-groupes-list-overview.component.scss']
})
export class AdminGroupesListOverviewComponent implements OnInit {

  groupes: Groupe [];

  constructor(public groupeService: GroupeService) { }

  ngOnInit() {
    this.groupeService
        .getGroupes()
        .subscribe(
            (data) => this.groupes = data,
            (err) => console.log(err)
        );
  }

}
