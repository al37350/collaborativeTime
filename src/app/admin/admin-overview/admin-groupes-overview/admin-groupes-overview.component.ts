import { Component, OnInit } from '@angular/core';
import {Groupe} from "../../../_models/groupe";
import {GroupeService} from "../../../_services/groupe.service";

@Component({
  selector: 'admin-groupes',
  templateUrl: './admin-groupes-overview.component.html',
  styleUrls: ['./admin-groupes-overview.component.scss']
})
export class AdminGroupesOverviewComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }
}
