import { Component, OnInit } from '@angular/core';
import {CaracteristiqueService} from "../../../../_services/caracteristique.service";
import {AlertService} from "../../../../_services/alert.service";

@Component({
    selector: 'app-admin-caracteristiques-list-overview',
    templateUrl: './admin-caracteristiques-list-overview.component.html',
    styleUrls: ['./admin-caracteristiques-list-overview.component.scss']
})
export class AdminCaracteristiquesListOverviewComponent implements OnInit {

    private caracteristiques;
    private nom = "";

    constructor(
        private caracteristiqueService : CaracteristiqueService,
        private alertService : AlertService
    ) { }

    ngOnInit() {
        this.caracteristiqueService.getCaracteristiques().subscribe(
            (data) => {
                this.caracteristiques = data;
            },
            (err) => console.log(err)

        );
    }

    deleteCaracteristique(id : number){
        this.caracteristiqueService.deleteCaracteristique(id).subscribe(
            (data) => {
                this.caracteristiques =this.caracteristiques.filter(item => item.id != id);
            },
            (err) => console.log(err)

        );
    }

    onSubmit(){

        this.caracteristiqueService.postCaracteristique(
            {
                "nom" : this.nom
            }
        ).subscribe(
            (data) => {
                this.alertService.success("Modification effectuée");
                this.nom ="";
                this.caracteristiques.push(data);
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }
        );
    }
}
