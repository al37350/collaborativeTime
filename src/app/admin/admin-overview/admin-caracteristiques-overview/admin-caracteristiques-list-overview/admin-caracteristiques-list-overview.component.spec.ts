import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCaracteristiquesListOverviewComponent } from './admin-caracteristiques-list-overview.component';

describe('AdminCaracteristiquesListOverviewComponent', () => {
  let component: AdminCaracteristiquesListOverviewComponent;
  let fixture: ComponentFixture<AdminCaracteristiquesListOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCaracteristiquesListOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCaracteristiquesListOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
