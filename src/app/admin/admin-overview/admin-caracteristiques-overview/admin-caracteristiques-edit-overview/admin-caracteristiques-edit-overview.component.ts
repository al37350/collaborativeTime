import { Component, OnInit } from '@angular/core';
import {CaracteristiqueService} from "../../../../_services/caracteristique.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService} from "../../../../_services/alert.service";

@Component({
  selector: 'app-admin-caracteristiques-edit-overview',
  templateUrl: './admin-caracteristiques-edit-overview.component.html',
  styleUrls: ['./admin-caracteristiques-edit-overview.component.scss']
})
export class AdminCaracteristiquesEditOverviewComponent implements OnInit {

  private model;
  private caracteristiqueId;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private caracteristiqueservice: CaracteristiqueService,
      private alertService: AlertService
  ) { }

  ngOnInit(){
    this.caracteristiqueId = this.route.snapshot.params['id'];

    this.getCaracteristique();
  }

  getCaracteristique(){
    this.caracteristiqueservice.getCaracteristique(this.caracteristiqueId).subscribe(
        (data) => {
          this.model = data;
        },
        (err) => console.log(err)
    );
  }

  onSubmit(){
    this.caracteristiqueservice.patchCaracteristique(
        this.caracteristiqueId,
        {
          "nom" : this.model.nom,
        }
    ).subscribe(
        (reponse) => {
          this.alertService.success("Modification effectuée");
          this.router.navigate(["admin/overview/caracteristique"]);
        },
        (err) => {
          console.log(err);
          let message = JSON.parse(err._body);
          this.alertService.error(message.message);
        }
    );
  }
}
