import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCaracteristiquesEditOverviewComponent } from './admin-caracteristiques-edit-overview.component';

describe('AdminCaracteristiquesEditOverviewComponent', () => {
  let component: AdminCaracteristiquesEditOverviewComponent;
  let fixture: ComponentFixture<AdminCaracteristiquesEditOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCaracteristiquesEditOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCaracteristiquesEditOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
