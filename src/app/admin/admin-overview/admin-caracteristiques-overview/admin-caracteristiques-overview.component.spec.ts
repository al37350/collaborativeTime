/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AdminCaracteristiquesOverviewComponent } from './admin-caracteristiques-overview.component';

describe('AdminCaracteristiquesOverviewComponent', () => {
  let component: AdminCaracteristiquesOverviewComponent;
  let fixture: ComponentFixture<AdminCaracteristiquesOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCaracteristiquesOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCaracteristiquesOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
