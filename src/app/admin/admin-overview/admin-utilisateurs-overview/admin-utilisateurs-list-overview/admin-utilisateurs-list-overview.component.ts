import { Component, OnInit } from '@angular/core';
import {UtilisateurService} from "../../../../_services/utilisateur.service";

@Component({
  selector: 'app-admin-utilisateurs-list-overview',
  templateUrl: './admin-utilisateurs-list-overview.component.html',
  styleUrls: ['./admin-utilisateurs-list-overview.component.scss']
})
export class AdminUtilisateursListOverviewComponent implements OnInit {
  utilisateurs: any[];

  constructor(public utilisateurService: UtilisateurService) { }

  ngOnInit() {
    this.utilisateurService
        .getUtilisateurs()
        .subscribe(
            (data) => {
              this.utilisateurs = data;
            },
            (err) => console.log(err)
        );
  }

}
