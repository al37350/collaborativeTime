import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-admin-overview',
    templateUrl: './admin-overview.component.html',
    styleUrls: ['./admin-overview.component.scss']
})
export class AdminOverviewComponent implements OnInit {
    private links;

    constructor() {
        this.links = [
            {
                path : '/admin/overview/utilisateur',
                name : 'Gérer les utilisateurs'
            },
            {
                path : '/admin/overview/groupe',
                name : 'Gérer les groupes'
            },
            {
                path : '/admin/overview/typetravail',
                name : 'Gérer les types de travail'
            },
            {
                path : '/admin/overview/caracteristique',
                name : 'Gérer les caractéristiques'
            },
            {
                path : '/admin/overview/motif',
                name : 'Gérer les motifs'
            },
            {
                path : '/admin/overview/famille',
                name : 'Gérer les familles'
            },
        ];
    }
    ngOnInit() {
    }

}
