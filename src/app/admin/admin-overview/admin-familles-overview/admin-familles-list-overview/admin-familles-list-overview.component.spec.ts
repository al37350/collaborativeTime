import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFamillesListOverviewComponent } from './admin-familles-list-overview.component';

describe('AdminFamillesListOverviewComponent', () => {
  let component: AdminFamillesListOverviewComponent;
  let fixture: ComponentFixture<AdminFamillesListOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFamillesListOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFamillesListOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
