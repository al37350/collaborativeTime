import { Component, OnInit } from '@angular/core';
import {AlertService} from "../../../../_services/alert.service";
import {FamilleService} from "../../../../_services/famille.service";

@Component({
    selector: 'app-admin-familles-list-overview',
    templateUrl: './admin-familles-list-overview.component.html',
    styleUrls: ['./admin-familles-list-overview.component.scss']
})
export class AdminFamillesListOverviewComponent implements OnInit {
    private familles;
    private nom = "";

    constructor(
        private familleService : FamilleService,
        private alertService : AlertService
    ) { }

    ngOnInit() {
        this.familleService.getFamilles().subscribe(
            (data) => {
                this.familles = data;
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }

        );
    }

    deleteFamille(id : number){
        this.familleService.deleteFamille(id).subscribe(
            (data) => {
                console.log(this.familles);
                this.familles =this.familles.filter(item => item.id != id);
                this.familles = this.familles.map(famille => {
                    famille.children = famille.children.filter(item => item.id != id);
                    return famille;
                })
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }

        );
    }

    onSubmit(){
        this.familleService.postFamille(
            {
                "nom" : this.nom,
                "couleur" : "#000000"
            }
        ).subscribe(
            (data) => {
                this.alertService.success("Modification effectuée");
                this.nom ="";
                this.familles.push(data);
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }
        );
    }
}
