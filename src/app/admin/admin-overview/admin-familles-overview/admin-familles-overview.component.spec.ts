import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFamillesOverviewComponent } from './admin-familles-overview.component';

describe('AdminFamillesOverviewComponent', () => {
  let component: AdminFamillesOverviewComponent;
  let fixture: ComponentFixture<AdminFamillesOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFamillesOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFamillesOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
