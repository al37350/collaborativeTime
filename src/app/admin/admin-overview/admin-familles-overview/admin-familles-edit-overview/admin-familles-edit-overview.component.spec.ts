import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFamillesEditOverviewComponent } from './admin-familles-edit-overview.component';

describe('AdminFamillesEditOverviewComponent', () => {
  let component: AdminFamillesEditOverviewComponent;
  let fixture: ComponentFixture<AdminFamillesEditOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFamillesEditOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFamillesEditOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
