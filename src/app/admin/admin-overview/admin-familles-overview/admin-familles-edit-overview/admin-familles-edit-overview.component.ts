import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FamilleService} from "../../../../_services/famille.service";
import {AlertService} from "../../../../_services/alert.service";

@Component({
    selector: 'app-admin-familles-edit-overview',
    templateUrl: './admin-familles-edit-overview.component.html',
    styleUrls: ['./admin-familles-edit-overview.component.scss']
})
export class AdminFamillesEditOverviewComponent implements OnInit {

    private model;
    private familleId;

    private nom : string = "";
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private familleService: FamilleService,
        private alertService: AlertService
    ) { }

    ngOnInit(){
        this.familleId = this.route.snapshot.params['id'];

        this.getFamille();
    }

    getFamille(){
        this.familleService.getFamille(this.familleId).subscribe(
            (data) => {
                this.model = data;
                console.log(data);
            },
            (err) => console.log(err)
        );
    }

    onSubmit(){
        this.familleService.patchFamille(
            this.familleId,
            {
                "nom" : this.model.nom,
                "couleur" : this.model.couleur,
            }
        ).subscribe(
            (reponse) => {
                this.alertService.success("Modification effectuée");
                this.router.navigate(["admin/overview/famille"]);
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.error(message.message);
            }
        );
    }

    deleteFamille(id : number){
        this.familleService.deleteFamille(id).subscribe(
            (data) => {
                this.model.children =this.model.children.filter(item => item.id != id);
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }
        );
    }

    addFamille(){
        this.familleService.postFamille(
            {
                "nom" : this.nom,
                "parent": this.familleId,
                "couleur" : "#000000"
            }
        ).subscribe(
            (data) => {
                this.alertService.success("Modification effectuée");
                this.nom ="";
                this.model.children.push(data);
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }
        );
    }
}
