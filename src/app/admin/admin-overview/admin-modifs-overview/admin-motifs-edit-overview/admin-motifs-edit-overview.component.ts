import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {MotifService} from "../../../../_services/motif.service";
import {AlertService} from "../../../../_services/alert.service";

@Component({
  selector: 'app-admin-motifs-edit-overview',
  templateUrl: './admin-motifs-edit-overview.component.html',
  styleUrls: ['./admin-motifs-edit-overview.component.scss']
})
export class AdminMotifsEditOverviewComponent implements OnInit {
  private model;
  private motifId;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private motifService: MotifService,
      private alertService: AlertService
  ) { }

  ngOnInit(){
    this.motifId = this.route.snapshot.params['id'];

    this.getMotif();
  }

  getMotif(){
    this.motifService.getMotif(this.motifId).subscribe(
        (data) => {
          this.model = data;
        },
        (err) => console.log(err)
    );
  }

  onSubmit(){
    this.motifService.patchMotif(
        this.motifId,
        {
          "nom" : this.model.nom,
        }
    ).subscribe(
        (reponse) => {
          this.alertService.success("Modification effectuée");
          this.router.navigate(["admin/overview/motif"]);
        },
        (err) => {
          console.log(err);
          let message = JSON.parse(err._body);
          this.alertService.error(message.message);
        }
    );
  }


}
