import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMotifsEditOverviewComponent } from './admin-motifs-edit-overview.component';

describe('AdminMotifsEditOverviewComponent', () => {
  let component: AdminMotifsEditOverviewComponent;
  let fixture: ComponentFixture<AdminMotifsEditOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMotifsEditOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMotifsEditOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
