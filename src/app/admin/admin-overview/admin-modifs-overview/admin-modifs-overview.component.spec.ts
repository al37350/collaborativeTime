import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminModifsOverviewComponent } from './admin-modifs-overview.component';

describe('AdminModifsOverviewComponent', () => {
  let component: AdminModifsOverviewComponent;
  let fixture: ComponentFixture<AdminModifsOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminModifsOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminModifsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
