import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMotifsListOverviewComponent } from './admin-motifs-list-overview.component';

describe('AdminMotifsListOverviewComponent', () => {
  let component: AdminMotifsListOverviewComponent;
  let fixture: ComponentFixture<AdminMotifsListOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMotifsListOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMotifsListOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
