import { Component, OnInit } from '@angular/core';
import {AlertService} from "../../../../_services/alert.service";
import {MotifService} from "../../../../_services/motif.service";

@Component({
    selector: 'app-admin-motifs-list-overview',
    templateUrl: './admin-motifs-list-overview.component.html',
    styleUrls: ['./admin-motifs-list-overview.component.scss']
})
export class AdminMotifsListOverviewComponent implements OnInit {

    private motifs;
    private nom = "";

    constructor(
        private motifService : MotifService,
        private alertService : AlertService
    ) { }

    ngOnInit() {
        this.motifService.getMotifs().subscribe(
            (data) => {
                this.motifs = data;
            },
            (err) => console.log(err)

        );
    }

    deleteMotif(id : number){
        this.motifService.deleteMotif(id).subscribe(
            (data) => {
                this.motifs =this.motifs.filter(item => item.id != id);
            },
            (err) => console.log(err)

        );
    }

    onSubmit(){
        this.motifService.postMotif(
            {
                "nom" : this.nom
            }
        ).subscribe(
            (data) => {
                this.alertService.success("Modification effectuée");
                this.nom ="";
                this.motifs.push(data);
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }
        );
    }
}
