import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DroitService} from "../../../_services/droit.service";
import {GroupeService} from "../../../_services/groupe.service";
import {SharedService} from "../../../_services/shared.service";

@Component({
  selector: 'app-admin-edit-groupe',
  templateUrl: './admin-edit-groupe.component.html',
  styleUrls: ['./admin-edit-groupe.component.scss']
})
export class AdminEditGroupeComponent implements OnInit {
  private groupeModele;
  private droitsPossible;
  private groupeId;

  constructor(
      private route: ActivatedRoute,
      private droitService : DroitService,
      private groupeService :GroupeService,
      private sharedService : SharedService
  ) { }

  ngOnInit() {
    this.groupeId = this.route.snapshot.params['id'];

    this.droitService.getDroits().subscribe(
        (data) => {
          this.droitsPossible = data;
          this.getDroitOfGroupe();
        },
        (err) => console.log(err)

    );
  }

  getDroitOfGroupe(){
    this.groupeService.getGroupe(this.groupeId).subscribe(
        (data) => {
          this.groupeModele = data;
          data["droits"].map((data) => {
            let idDroit = data["id"];
            let indiceInCaracteristiquePossible = this.droitsPossible.map(item => item.id).indexOf(idDroit);
            this.droitsPossible[indiceInCaracteristiquePossible].selected = true;
          })
        },
    );
  }

  changeDroit(droit) {
      if (droit.selected) {
          this.groupeService.setDroit(this.groupeId, droit.id).subscribe(
              (data) => {
                  this.sharedService.putGroupeDroits(data);
              },
              (err) => console.log(err)
          );
      }
      else {
          this.groupeService.deleteDroit(this.groupeId, droit.id).subscribe(
              (data) => {
                  this.sharedService.putGroupeDroits(data);
              },
              (err) => console.log(err)
          );
      }
  }

}
