/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEditUtilisateurInfoComponent } from './admin-edit-utilisateur-info.component';

describe('AdminEditUtilisateurInfoComponent', () => {
  let component: AdminEditUtilisateurInfoComponent;
  let fixture: ComponentFixture<AdminEditUtilisateurInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditUtilisateurInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditUtilisateurInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
