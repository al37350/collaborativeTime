import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UtilisateurService} from "../../../../_services/utilisateur.service";
import {AlertService} from "../../../../_services/alert.service";
import {AuthenticationService} from "../../../../_services/authentification.service";
import {Utilisateur} from "../../../../_models/utilisateur";
import {CurrentUserService} from "../../../../_services/currentUser.service";

@Component({
    selector: 'app-admin-edit-utilisateur-info',
    templateUrl: 'admin-edit-utilisateur-info.component.html',
    styleUrls: ['admin-edit-utilisateur-info.component.scss']
})
export class AdminEditUtilisateurInfoComponent implements OnInit {
    private id: number;
    private details;
    private model;
    private status;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private utilisateurService: UtilisateurService,
        private alertService: AlertService,
        private currentUserService : CurrentUserService
    ) {
        this.status = "Envoi en cours..."
    }

    ngOnInit() {
        this.id = (typeof this.route.snapshot.params['id'] === 'undefined') ? this.currentUserService.getUser().id : this.route.snapshot.params['id'];
        this.utilisateurService.getUtilisateur(this.id).subscribe(
            (data) => this.model = new Utilisateur(data.id, data.nom, data.prenom, data.identifiant, data.email),
            (err) => this.details = err
        );
    }

    onSubmit() {
        this.utilisateurService.patchUtilisateur(
            this.id,
            {
                "nom" : this.model.nom,
                "prenom" : this.model.prenom,
                "email" : this.model.email,
                "identifiant" : this.model.identifiant
            }
        ).subscribe(
            (reponse) => {
                this.alertService.success("Modification effectuée");
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.error(message.message);
            }
        );
    }

    delete(){
        this.utilisateurService.deleteUtilisateur(
            this.id
        ).subscribe(
            (reponse) => this.router.navigate(["/admin/overview/utilisateur/list"]),
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.error(message.message);
            }
        );
    }

}