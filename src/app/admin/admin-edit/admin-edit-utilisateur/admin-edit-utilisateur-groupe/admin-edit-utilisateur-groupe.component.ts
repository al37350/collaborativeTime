import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {GroupeService} from "../../../../_services/groupe.service";
import {UtilisateurService} from "../../../../_services/utilisateur.service";
import {SharedService} from "../../../../_services/shared.service";
import {CurrentUserService} from "../../../../_services/currentUser.service";

@Component({
    selector: 'app-admin-edit-utilisateur-groupe',
    templateUrl: './admin-edit-utilisateur-groupe.component.html',
    styleUrls: ['./admin-edit-utilisateur-groupe.component.scss']
})
export class AdminEditUtilisateurGroupeComponent implements OnInit {
    private groupesPossible;
    private model;
    private userId;

    constructor(
        private route: ActivatedRoute,
        private groupeService : GroupeService,
        private utilisateurService : UtilisateurService,
        private currentUserService : CurrentUserService,
        private sharedService: SharedService
    ) { }

    ngOnInit() {
        this.userId = this.route.snapshot.params['id'];

        this.groupeService.getGroupes().subscribe(
            (data) => {
                this.groupesPossible = data;
                this.getTypeTravail();
            },
            (err) => console.log(err)
        );
    }

    getTypeTravail(){
        this.utilisateurService.getUtilisateurGroupe(this.userId).subscribe(
            (data) => {
                this.model = data;
                data.map((data) => {
                    let idGroupe = data["id"];
                    let indiceInCaracteristiquePossible = this.groupesPossible.map(item => item.id).indexOf(idGroupe);
                    this.groupesPossible[indiceInCaracteristiquePossible].selected = true;
                })

            },
            (err) => console.log(err)
        );
    }

    changeGroupe(groupe){
        if(groupe.selected){
            this.utilisateurService.setGroupe(this.userId, groupe.id).subscribe(
                (data) => {
                    if(this.currentUserService.getUser().id == this.userId){
                        this.sharedService.putGroupe(data);
                    }
                },
                (err) => console.log(err)

            );
        }
        else{
            this.utilisateurService.deleteGroupe(this.userId, groupe.id).subscribe(
                (data) => {
                    if(this.currentUserService.getUser().id == this.userId){
                        this.sharedService.putGroupe(data);
                    }
                },
                (err) => console.log(err)

            );
        }
    }

}
