/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AdminEditUtilisateurGroupeComponent } from './admin-edit-utilisateur-groupe.component';

describe('AdminEditUtilisateurGroupeComponent', () => {
  let component: AdminEditUtilisateurGroupeComponent;
  let fixture: ComponentFixture<AdminEditUtilisateurGroupeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditUtilisateurGroupeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditUtilisateurGroupeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
