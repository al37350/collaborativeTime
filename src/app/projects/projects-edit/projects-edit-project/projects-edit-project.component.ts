import { Component, OnInit } from '@angular/core';
import {SujetService} from "../../../_services/sujet.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TypeTravailService} from "../../../_services/type-travail.service";
import {UtilisateurService} from "../../../_services/utilisateur.service";
import {AlertService} from "../../../_services/alert.service";
import {CurrentUserService} from "../../../_services/currentUser.service";
import {EnregistrementFroidStoreService} from "../../../_services/EnregistrementFroidStore.service";
import {TypeTravailSelectionService} from "../../../_services/typeTravailSelection.service";

@Component({
    selector: 'app-projects-edit-project',
    templateUrl: './projects-edit-project.component.html',
    styleUrls: ['./projects-edit-project.component.scss']
})
export class ProjectsEditProjectComponent implements OnInit {
    private userId;
    private idSujet : number;
    private sujet: any;
    private utilisateurs : any;
    private utilisateursPossible : any;
    private typeTravailsPossible;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private sujetService: SujetService,
        private utilisateurService: UtilisateurService,
        private typeTravailService:TypeTravailService,
        private alertService : AlertService,
        private enregistrementFroidStoreService : EnregistrementFroidStoreService,
        private currentUserService : CurrentUserService,
        private typeTravailSelectionService : TypeTravailSelectionService
    ) { }

    ngOnInit() {
        this.idSujet = this.route.snapshot.params['id'];
        this.userId = this.currentUserService.getUser().id;

        this.sujetService
            .getSujet(this.idSujet)
            .subscribe(
                (data) => {
                    this.sujet = data;
                },
                (err) => console.log(err)
            );

        this.sujetService
            .getutilisateur(this.idSujet)
            .subscribe(
                (data) => {
                    this.utilisateurs = data;
                },
                (err) => console.log(err)
            );

        this.utilisateurService
            .getUtilisateurs()
            .subscribe(
                (data) => {
                    this.utilisateursPossible = data;
                },
                (err) => console.log(err)
            )

        this.typeTravailService.getTypeTravails().subscribe(
            (data) => {
                this.typeTravailsPossible = data;
                this.getTypeTravailSelection();
            },
            (err) => console.log(err)

        );
    }

    deleteProjet(id : number){
        this.sujetService
            .deleteSujet(id)
            .subscribe(
                (data) => {
                    this.router.navigate(["/projects/overview"]);
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    addUser(id : number){
        this.sujetService
            .addUtilisateur(this.idSujet, id)
            .subscribe(
                (data) => {
                    this.utilisateurs = data;
                    if(id == this.userId){
                        this.enregistrementFroidStoreService.addSujet(this.sujet);
                    }
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    deleteUser(id : number){
        this.sujetService
            .deleteUser(this.idSujet, id)
            .subscribe(
                (data) => {
                    if(id == this.userId){
                        this.enregistrementFroidStoreService.removeSujet(this.idSujet);
                    }
                    this.utilisateurs =this.utilisateurs.filter(item => item.id != id)
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    changeTypeTravail(event){
        console.log(event.value);
        if(event.checked){
            this.typeTravailSelectionService.postTypeTravailSelection({
                "sujet" : this.idSujet,
                "isActive" : true,
                "typeTravail" : event.value
            }).subscribe(
                (response) => {
                    let tt = this.typeTravailsPossible.filter(typeTravail => typeTravail.id == event.value)[0];
                    tt.typeTravailSelectionID = response.id;
                    tt.selected = true
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );
        }else{
            this.typeTravailSelectionService.deleteTypeTravailSelection(
                this.typeTravailsPossible.filter(typeTravail => typeTravail.id == event.value)[0].typeTravailSelectionID
            ).subscribe(
                (reponse) => {
                    this.typeTravailsPossible.filter(typeTravail => typeTravail.id == event.value)[0].selected = false
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );
        }
    }

    getTypeTravailSelection(){
        this.typeTravailSelectionService.getTypeTravailSelectionFromSujet(this.idSujet).subscribe(
            (data) => {
                data.map(typeTravailSelection => {
                    let idTypeTravail = typeTravailSelection["typeTravail"]["id"];
                    let indiceTypeTravail = this.typeTravailsPossible.map(item => item.id).indexOf(idTypeTravail);
                    this.typeTravailsPossible[indiceTypeTravail].selected = true;
                    this.typeTravailsPossible[indiceTypeTravail].typeTravailSelectionID = typeTravailSelection.id;
                });
            },
            (err) => console.log(err)
        );
    }
}
