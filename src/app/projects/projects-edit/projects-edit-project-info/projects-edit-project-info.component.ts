import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {AuthenticationService} from "../../../_services/authentification.service";
import {SujetService} from "../../../_services/sujet.service";
import {AlertService} from "../../../_services/alert.service";
import {Sujet} from "../../../_models/sujet";
import {FamilleService} from "../../../_services/famille.service";

@Component({
    selector: 'app-projects-edit-project-info',
    templateUrl: './projects-edit-project-info.component.html',
    styleUrls: ['./projects-edit-project-info.component.scss']
})
export class ProjectsEditProjectInfoComponent implements OnInit {
    private model;
    private sujetId : number;
    private familles;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private sujetService: SujetService,
        private alertService: AlertService,
        private familleService : FamilleService
    ) { }

    ngOnInit() {
        this.sujetId = this.route.snapshot.params['id'];
        this.model = new Sujet;
        this.sujetService.getSujet(this.sujetId)
            .subscribe(
                (reponse) => this.model = reponse,
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );

        this.familleService.getFamilles().subscribe(
            (data) => {
                this.familles = data;
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }

        );
    }

    onSubmit(){
        this.sujetService.patchSujet(
            this.sujetId,
            {
                "nom" : this.model.nom,
                "description" : this.model.description,
                "famille" : this.model.famille.id,
                "ouvert" : this.model.ouvert,
                "publique" : this.model.publique,
            }
        ).subscribe(
            (reponse) => {
                this.alertService.success("Modification effectuée");
                this.router.navigate(["projects/edit/project/", this.sujetId]);
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }
        );
    }

}
