import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsEditCaracComponent } from './projects-edit-carac.component';

describe('ProjectsEditCaracComponent', () => {
  let component: ProjectsEditCaracComponent;
  let fixture: ComponentFixture<ProjectsEditCaracComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsEditCaracComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsEditCaracComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
