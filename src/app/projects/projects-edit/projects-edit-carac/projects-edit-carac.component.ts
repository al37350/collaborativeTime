import { Component, OnInit } from '@angular/core';
import {CaracteristiqueService} from "../../../_services/caracteristique.service";
import {TypeTravailService} from "../../../_services/type-travail.service";
import {ActivatedRoute} from "@angular/router";
import {AlertService} from "../../../_services/alert.service";
import {TypeTravailSelectionService} from "../../../_services/typeTravailSelection.service";

@Component({
    selector: 'app-projects-edit-carac',
    templateUrl: './projects-edit-carac.component.html',
    styleUrls: ['./projects-edit-carac.component.scss']
})
export class ProjectsEditCaracComponent implements OnInit {

    private caracteristiquesPossible;
    private typeTravailSelectionId;

    constructor(
        private route: ActivatedRoute,
        private caracteristiqueService : CaracteristiqueService,
        private typeTravailSelectionService : TypeTravailSelectionService,
        private alertService : AlertService
    ) { }

    ngOnInit() {
        this.typeTravailSelectionId = this.route.snapshot.params['id'];
        this.caracteristiqueService.getCaracteristiques().subscribe(
            (data) => {
                this.caracteristiquesPossible = data;
                this.getCaracteristiquesSelected();
            },
            (err) => console.log(err));
    }

    getCaracteristiquesSelected() {
        this.typeTravailSelectionService.getCaracteristiques(this.typeTravailSelectionId).subscribe(
            (data) => {
                data.map(carac => {
                    let idCaracteritique = carac["id"];
                    let indiceInCaracteristiquePossible = this.caracteristiquesPossible.map(item => item.id).indexOf(idCaracteritique);
                    this.caracteristiquesPossible[indiceInCaracteristiquePossible].selected = true;
                });
            },
            (err) => console.log(err)
        );
    }

    changeCaracteristique(caracteristique){
        caracteristique.selected = !caracteristique.selected;
        if(caracteristique.selected){
            this.typeTravailSelectionService.setCaracteristique(this.typeTravailSelectionId, caracteristique.id).subscribe(
                (data) => {},
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
        }
        else{
            this.typeTravailSelectionService.deleteCaracteristique(this.typeTravailSelectionId, caracteristique.id).subscribe(
                (data) => {},
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
        }
    }
}
