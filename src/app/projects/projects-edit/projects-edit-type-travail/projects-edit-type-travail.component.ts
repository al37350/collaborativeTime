import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {TypeTravailService} from "../../../_services/type-travail.service";
import {AlertService} from "../../../_services/alert.service";

@Component({
    selector: 'app-projects-edit-type-travail',
    templateUrl: './projects-edit-type-travail.component.html',
    styleUrls: ['./projects-edit-type-travail.component.scss']
})
export class ProjectsEditTypeTravailComponent implements OnInit {
    private model;
    private typeTravailId;

    private setByDefault : boolean = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private typeTravailService: TypeTravailService,
        private alertService: AlertService
    ) { }

    ngOnInit(){
        this.typeTravailId = this.route.snapshot.params['id'];

        this.getTypeTravail();
    }

    getTypeTravail(){
        this.typeTravailService.getTypeTravail(this.typeTravailId).subscribe(
            (data) => {
                this.model = data;
                this.setByDefault = data.setByDefault;
            },
            (err) => console.log(err)
        );
    }

    onSubmit(){
        this.typeTravailService.patchTypeTravail(
            this.typeTravailId,
            {
                "nom" : this.model.nom,
                "setByDefault" : this.setByDefault,
            }
        ).subscribe(
            (reponse) => {
                this.alertService.success("Modification effectuée");
                this.router.navigate(["admin/overview/typetravail"]);
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.error(message.message);
            }
        );
    }
}
