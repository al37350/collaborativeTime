import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects-overview',
  templateUrl: './projects-overview.component.html',
  styleUrls: ['./projects-overview.component.scss']
})
export class ProjectsOverviewComponent implements OnInit {
  private links;

  constructor() {
    this.links = [
      {
        path : '/projects/overview/createur',
        name : 'Mes sujets créés'
      },
      {
        path : '/projects/overview/appartenance',
        name : 'Sujets auxquels j\'appartiens'
      },
    ];
  }

  ngOnInit() {
  }

}
