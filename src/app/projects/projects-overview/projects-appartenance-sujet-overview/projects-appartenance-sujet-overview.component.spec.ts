/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ProjectsAppartenanceSujetOverviewComponent } from './projects-appartenance-sujet-overview.component';

describe('ProjectsAppartenanceSujetOverviewComponent', () => {
  let component: ProjectsAppartenanceSujetOverviewComponent;
  let fixture: ComponentFixture<ProjectsAppartenanceSujetOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsAppartenanceSujetOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsAppartenanceSujetOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
