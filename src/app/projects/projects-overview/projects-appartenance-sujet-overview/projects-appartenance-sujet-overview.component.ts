import { Component, OnInit } from '@angular/core';
import {UtilisateurService} from "../../../_services/utilisateur.service";
import {CurrentUserService} from "../../../_services/currentUser.service";
import {FamilleService} from "../../../_services/famille.service";
import {AlertService} from "../../../_services/alert.service";

@Component({
  selector: 'app-projects-appartenance-sujet-overview',
  templateUrl: './projects-appartenance-sujet-overview.component.html',
  styleUrls: ['./projects-appartenance-sujet-overview.component.scss']
})
export class ProjectsAppartenanceSujetOverviewComponent implements OnInit {
    userId : number;
    nom: string = "";
    sujets: any[];

    showSettings : boolean = false;
    publique : boolean = null;
    familleSelected : boolean = null;
    familles;

    constructor(
        private utilisateurService: UtilisateurService,
        private currentUserService : CurrentUserService,
        private familleService : FamilleService,
        private alertService: AlertService
    ) { }

    ngOnInit() {
        this.userId = this.currentUserService.getUser().id;
        this.loadSujets();
        this.loadFamilles();
    }

    private loadSujets(){
        let filtre = "nom=" + this.nom;

        if(this.showSettings){
            if(null != this.publique){
                filtre += "&publique="+this.publique
            }

            if(null != this.familleSelected){
                filtre += "&famille="+this.familleSelected
            }
        }

        this.utilisateurService
            .getUtilisateurSujetSouscris(this.userId, filtre)
            .subscribe(
                (data) => {
                    this.sujets = data;
                },
                (err) => console.log(err)
            );
    }

    private loadFamilles(){
        this.familleService.getFamilles().subscribe(
            (data) => {
                this.familles = data;
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }

        );
    }
}
