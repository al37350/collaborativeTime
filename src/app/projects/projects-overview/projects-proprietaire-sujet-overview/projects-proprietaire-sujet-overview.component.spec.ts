/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ProjectsProprietaireSujetOverviewComponent } from './projects-proprietaire-sujet-overview.component';

describe('ProjectsProprietaireSujetOverviewComponent', () => {
  let component: ProjectsProprietaireSujetOverviewComponent;
  let fixture: ComponentFixture<ProjectsProprietaireSujetOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsProprietaireSujetOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsProprietaireSujetOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
