import { Component, OnInit } from '@angular/core';
import {UtilisateurService} from "../../../_services/utilisateur.service";
import {CurrentUserService} from "../../../_services/currentUser.service";
import * as Baby from 'babyparse';
import {SujetService} from "../../../_services/sujet.service";
import {AlertService} from "../../../_services/alert.service";
import {Router} from "@angular/router";
import {FamilleService} from "../../../_services/famille.service";

@Component({
    selector: 'app-projects-proprietaire-sujet-overview',
    templateUrl: './projects-proprietaire-sujet-overview.component.html',
    styleUrls: ['./projects-proprietaire-sujet-overview.component.scss']
})
export class ProjectsProprietaireSujetOverviewComponent implements OnInit {
    userId : number;
    nom: string = "";
    sujets: any[];

    showSettings : boolean = false;
    publique : boolean = null;
    familleSelected : boolean = null;
    familles;

    constructor(
        private router : Router,
        private utilisateurService: UtilisateurService,
        private currentUserService: CurrentUserService,
        private sujetService: SujetService,
        private alertService : AlertService,
        private familleService : FamilleService
    ){ }

    ngOnInit() {
        this.userId = this.currentUserService.getUser().id;
        this.loadFamilles();
        this.loadSujets();
    }


    private loadSujets(){
        let filtre = "nom=" + this.nom;

        if(this.showSettings){
            if(null != this.publique){
                filtre += "&publique="+this.publique
            }

            if(null != this.familleSelected){
                filtre += "&famille="+this.familleSelected
            }
        }

        this.utilisateurService
            .getUtilisateurProprietaireSujet(this.userId, filtre)
            .subscribe(
                (data) => {
                    this.sujets = data;
                },
                (err) => console.log(err)
            )
    }

    private loadFamilles(){
        this.familleService.getFamilles().subscribe(
            (data) => {
                this.familles = data;
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }

        );
    }

    onChange(inputValue){
        let file: File = inputValue.files[0];
        let myReader: FileReader = new FileReader();
        myReader.onloadend = (e) => {
            let parsed = Baby.parse(myReader.result, {
                header: true,
                complete : (results, file) => {
                    this.sujetService.postSujetListe(this.userId, results.data).subscribe(
                        (reponse) => {
                            this.alertService.success("Import réussi");
                            this.router.navigate(["projects/overview/createur"])
                        },
                        (err) => {
                            console.log(err);
                            let message = JSON.parse(err._body);
                            this.alertService.error(message.message);
                        }
                    )
                }
            });
        };
        myReader.readAsText(file);
    }
}
