import { Component, OnInit } from '@angular/core';
import {Sujet} from "../../../_models/sujet";
import {AlertService} from "../../../_services/alert.service";
import {SujetService} from "../../../_services/sujet.service";
import {AuthenticationService} from "../../../_services/authentification.service";
import {Router} from "@angular/router";
import {CurrentUserService} from "../../../_services/currentUser.service";
import {FamilleService} from "../../../_services/famille.service";

@Component({
    selector: 'app-projects-create-project-info',
    templateUrl: './projects-create-project-info.component.html',
    styleUrls: ['./projects-create-project-info.component.scss']
})
export class ProjectsCreateProjectInfoComponent implements OnInit {

    private model;
    private familles : any [];
    private familleSelected;

    constructor(
        private router: Router,
        private sujetService: SujetService,
        private alertService: AlertService,
        private currentUserService: CurrentUserService,
        private familleService : FamilleService
    ) { }

    ngOnInit() {
        this.model = new Sujet;

        this.familleService
            .getFamilles()
            .subscribe(
                (data) => {
                    this.familles = data;
                    if(this.familles.length){
                        this.familleSelected = this.familles[0].id;
                    }
                },
                (err) => console.log(err)
            );
    }

    onSubmit(){
        let userId = this.currentUserService.getUser().id;

        this.sujetService.postSujet(
            {
                "nom" : this.model.nom,
                "description" : this.model.description,
                "famille" : this.familleSelected,
                "ouvert" : "true",
                "publique" : "false",
                "proprietaire" : userId
            }
        ).subscribe(
            (reponse) => {
                this.alertService.success("Création effectuée");
                this.router.navigate(["projects/edit/project/", reponse.id ]);
            },
            (err) => {
                console.log(err);
                let message = JSON.parse(err._body);
                this.alertService.JSONError(message);
            }
        );
    }

    onChangeFamille(famille){
        this.familleSelected = famille;
    }
}
