import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsCreateEnregistrementQualifieComponent } from './projects-create-enregistrement-qualifie.component';

describe('ProjectsCreateEnregistrementQualifieComponent', () => {
  let component: ProjectsCreateEnregistrementQualifieComponent;
  let fixture: ComponentFixture<ProjectsCreateEnregistrementQualifieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsCreateEnregistrementQualifieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsCreateEnregistrementQualifieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
