import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Timer} from "../../../../_models/timer";
import {EnregistrementService} from "../../../../_services/enregistrement.service";
import {AlertService} from "../../../../_services/alert.service";
import {EnregistrementFroidStoreService} from "../../../../_services/EnregistrementFroidStore.service";
import {JourInstanceService} from "../../../../_services/jourInstance.service";
import {CurrentUserService} from "../../../../_services/currentUser.service";
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { EnregistrementQualification } from "../../../../_models/EnregistrementQualification";
import * as moment from 'moment';
import {SharedService} from "../../../../_services/shared.service";

@Component({
    selector: 'projects-create-enregistrement-qualifie',
    templateUrl: './projects-create-enregistrement-qualifie.component.html',
    styleUrls: ['./projects-create-enregistrement-qualifie.component.scss']
})
export class ProjectsCreateEnregistrementQualifieComponent implements OnInit {

    @Input() timer: Timer;
    @Input() disable: boolean = true;

    @Output() isRecording = new EventEmitter<boolean>();

    private enregistrementQualification : EnregistrementQualification = new EnregistrementQualification(666, 666, []);

    private userId;

    private dateDebut : number;
    private dateFin: number;
    private idEnregistrement : number;

    private jourInstanceId : number;

    private enregistrementExistant;

    constructor(
        private enregistrementService : EnregistrementService,
        private alertService : AlertService,
        private enregistrementFroidStoreService : EnregistrementFroidStoreService,
        private jourInstanceService : JourInstanceService,
        private currentUserService : CurrentUserService,
        private sharedService : SharedService
    ) {}

    ngOnInit() {
        this.enregistrementExistant = this.enregistrementFroidStoreService.enregistrementExistant;
        this.jourInstanceService.getJourInstanceFromMoment(moment()).subscribe(
            (data) => {
                this.jourInstanceId = data["id"];
            },
            (err) => console.log(err)
        );

        this.userId = this.currentUserService.getUser().id;
    }

    public startOrStopEnregistrement(isRecording : boolean){
        if(isRecording){
            this.activeAndSaveChrono();
        }
        else{
            this.stopAndSaveChrono();
        }
    }

    activeAndSaveChrono(){
        this.dateDebut = moment().unix();
        this.timer.start();
        this.isRecording.emit(true);
        this.sharedService.enregistrementFroidPossible = false;

        this.saveDebutEnregistremment();
    }

    stopAndSaveChrono(){
        this.dateFin = moment().unix();
        this.timer.stop();
        this.timer.reset();
        this.isRecording.emit(false);
        this.sharedService.enregistrementFroidPossible = true;

        this.saveFinEnregistremment();
    }

    private saveDebutEnregistremment(){

        let data = {
            "jourInstance" : this.jourInstanceId,
            "heureDebut" : this.dateDebut,
            "sujet" : this.enregistrementQualification.typeTravailSelection.sujet.id,
            "typeTravail" : this.enregistrementQualification.typeTravailSelection.typeTravail.id,
            "caracteristiques" : this.enregistrementQualification.typeTravailSelection.caracteristiques.map(cara => cara.id),
            "utilisateur" : this.userId
        };

        this.enregistrementService
            .postEnregistrement(data)
            .subscribe(
                (rep) => {
                    this.idEnregistrement = rep.id;
                    this.addTagPersoEnregistrement();
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    addTagPersoEnregistrement(){
        this.enregistrementQualification.tagsPersoId.map(idTagPerso => {
            this.enregistrementService
                .postEnregistrementTagPerso(this.idEnregistrement, idTagPerso)
                .subscribe(
                    (rep) => {},
                    (err) => {
                        console.log(err);
                        let message = JSON.parse(err._body);
                        this.alertService.error(message.message);
                    }
                );
        });
    }

    saveFinEnregistremment(){
        let data = {
            "heureFin" : this.dateFin,
            "duree" : this.dateFin - this.dateDebut,
        };

        this.enregistrementService.saveFinEnregistrement(this.idEnregistrement, data);
    }
}