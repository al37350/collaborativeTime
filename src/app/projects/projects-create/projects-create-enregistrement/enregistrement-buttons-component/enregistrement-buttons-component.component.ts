import { Component, OnInit, Output, Input,  EventEmitter } from '@angular/core';

@Component({
    selector: 'enregistrement-buttons',
    templateUrl: './enregistrement-buttons-component.component.html',
    styleUrls: ['./enregistrement-buttons-component.component.scss']
})
export class EnregistrementButtonsComponentComponent implements OnInit {

    @Input() allowPlay : boolean = true;
    @Input() allowStop : boolean = false;

    @Output() isRecording = new EventEmitter<boolean>();

    constructor() { }

    ngOnInit() {
    }

    public start(){
        this.allowPlay = false;
        this.allowStop = true;
        this.isRecording.emit(true);
    }

    public stop(){
        this.allowPlay = true;
        this.allowStop = false;
        this.isRecording.emit(false);
    }

}
