import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnregistrementButtonsComponentComponent } from './enregistrement-buttons-component.component';

describe('EnregistrementButtonsComponentComponent', () => {
  let component: EnregistrementButtonsComponentComponent;
  let fixture: ComponentFixture<EnregistrementButtonsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnregistrementButtonsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnregistrementButtonsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
