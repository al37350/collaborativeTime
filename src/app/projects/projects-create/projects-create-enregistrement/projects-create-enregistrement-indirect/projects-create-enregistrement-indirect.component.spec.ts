import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsCreateEnregistrementIndirectComponent } from './projects-create-enregistrement-indirect.component';

describe('ProjectsCreateEnregistrementIndirectComponent', () => {
  let component: ProjectsCreateEnregistrementIndirectComponent;
  let fixture: ComponentFixture<ProjectsCreateEnregistrementIndirectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsCreateEnregistrementIndirectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsCreateEnregistrementIndirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
