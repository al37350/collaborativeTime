import { Component, OnInit } from '@angular/core';
import {CurrentUserService} from "../../../../_services/currentUser.service";
import {JourInstanceService} from "../../../../_services/jourInstance.service";
import {EnregistrementQualification} from "../../../../_models/EnregistrementQualification";
import * as moment from 'moment';
import {NouiFormatter} from "ng2-nouislider";
import {TimeConverter} from "../../../../_models/TimeConverter";
import {EnregistrementService} from "../../../../_services/enregistrement.service";
import {AlertService} from "../../../../_services/alert.service";
import {SharedPeriodeTempsService} from "../../../../_services/shared.periodeTemps.service";

export class TimeFormatter implements NouiFormatter {
    to(value: number): string {
        let h = Math.floor(value / 3600);
        let m = Math.floor(value % 3600 / 60);
        let s = value - 60 * m - 3600 * h;
        let values = [h, m];
        let liaisons = ["h", ""];
        let timeString: string = '';
        values.forEach((_, i) => {
            if(values[i] < 10) {
                timeString += '0';
            }
            timeString += values[i].toFixed(0);
            if(i < 2) {
                timeString += liaisons[i];
            }
        });
        return timeString;
    };

    from(value: string): number {
        let v = value.split(':').map(parseInt);
        let time: number = 0;
        time += v[0] * 3600;
        time += v[1] * 60;
        time += v[2];
        return time;
    }
}

@Component({
    selector: 'projects-create-enregistrement-indirect',
    templateUrl: './projects-create-enregistrement-indirect.component.html',
    styleUrls: ['./projects-create-enregistrement-indirect.component.scss']
})
export class ProjectsCreateEnregistrementIndirectComponent implements OnInit {

    private enregistrementQualification : EnregistrementQualification = new EnregistrementQualification(666, 666, []);

    private userId;
    private jourInstanceId : number;

    public duree: number = 0;
    private heureDeb;
    private heureFin;
    private description: string = "";

    constructor(
        private jourInstanceService : JourInstanceService,
        private currentUserService : CurrentUserService,
        private enregistrementService : EnregistrementService,
        private sharedPeriodeTempsService : SharedPeriodeTempsService,
        private alertService : AlertService,
    ) {}

    ngOnInit() {
        this.jourInstanceService.jourInstance$.subscribe(
            (data) => {
                this.jourInstanceId = data["id"];
            },
            (err) => console.log(err)
        );

        this.userId = this.currentUserService.getUser().id;
    }

    public someTimeConfig: any = {
        start: 0,
        range: {
            min: 0,
            max: 28800
        },
        tooltips: new TimeFormatter(),
        step: 900
    };

    onChangeTextarea(description : string){
        this.description =description;
    }

    enregistrer(){
        let moment = this.jourInstanceService.getMoment();
        if(0 !== this.duree){
            let data = {
                "jourInstance" : this.jourInstanceId,
                "sujet" : this.enregistrementQualification.typeTravailSelection.sujet.id,
                "typeTravail" : this.enregistrementQualification.typeTravailSelection.typeTravail.id,
                "caracteristiques" : this.enregistrementQualification.typeTravailSelection.caracteristiques.map(cara => cara.id),
                "utilisateur" : this.userId,
                "description" : this.description,
                "duree" : this.duree,
            };
            this.postEnregistrement(data);

        }else{
            let debEnregistrement = TimeConverter.fromStringToTimestamp(moment, this.heureDeb);
            let finEnregistrement = TimeConverter.fromStringToTimestamp(moment, this.heureFin);
            let data = {
                "jourInstance" : this.jourInstanceId,
                "sujet" : this.enregistrementQualification.typeTravailSelection.sujet.id,
                "typeTravail" : this.enregistrementQualification.typeTravailSelection.typeTravail.id,
                "caracteristiques" : this.enregistrementQualification.typeTravailSelection.caracteristiques.map(cara => cara.id),
                "utilisateur" : this.userId,
                "description" : this.description,
                "heureDebut" : debEnregistrement,
                "heureFin" : finEnregistrement,
                "duree" : finEnregistrement-debEnregistrement,
            };
            this.postEnregistrement(data);
        }

    }

    postEnregistrement(data){
        if(0 !== this.duree){
            this.enregistrementService
                .postEnregistrementDuree(data)
                .subscribe(
                    (rep) => {
                        this.addTagPersoEnregistrement(rep.id);
                    },
                    (err) => {
                        console.log(err);
                        let message = JSON.parse(err._body);
                        this.alertService.JSONError(message);
                    }
                );
        }
        else{
            this.enregistrementService
                .postEnregistrement(data)
                .subscribe(
                    (rep) => {
                        this.addTagPersoEnregistrement(rep.id);
                        this.sharedPeriodeTempsService.addPeriodeTemps(rep);
                    },
                    (err) => {
                        console.log(err);
                        let message = JSON.parse(err._body);
                        this.alertService.JSONError(message);
                    }
                );
        }
    }

    addTagPersoEnregistrement(id:number){
        this.enregistrementQualification.tagsPersoId.map(idTagPerso => {
            this.enregistrementService
                .postEnregistrementTagPerso(id, idTagPerso)
                .subscribe(
                    (rep) => {},
                    (err) => {
                        console.log(err);
                        let message = JSON.parse(err._body);
                        this.alertService.error(message.message);
                    }
                );
        });
        this.heureDeb = "";
        this.heureFin = "";
        this.duree = 0;
        this.alertService.success("Enregistrement effectué");
    }

}
