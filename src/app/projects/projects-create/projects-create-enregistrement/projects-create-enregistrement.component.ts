import {Component, OnInit} from '@angular/core';
import {EnregistrementFroidStoreService} from "../../../_services/EnregistrementFroidStore.service";
import {EnregistrementService} from "../../../_services/enregistrement.service";
import {AlertService} from "../../../_services/alert.service";
import {Timer} from "../../../_models/timer";
import * as moment from 'moment';
import {JourInstanceService} from "../../../_services/jourInstance.service";
import {TimeConverter} from "../../../_models/TimeConverter";
import {EnregistrementQualification} from "../../../_models/EnregistrementQualification";

@Component({
    selector: 'app-projects-create-enregistrement',
    templateUrl: './projects-create-enregistrement.component.html',
    styleUrls: ['./projects-create-enregistrement.component.scss']
})
export class ProjectsCreateEnregistrementComponent implements OnInit {
    constructor(
        private enregistrementFroidStoreService : EnregistrementFroidStoreService,
        private enregistrementService : EnregistrementService,
        private alertService : AlertService,
        private jourInstanceService : JourInstanceService
    ) {}

    private sujets;
    private tagsPersonnels;
    private enregistrementExistant;

    private timer: Timer = new Timer();

    private disableQualifie : boolean = false;
    private disableQualifiable : boolean = false;

    private allowPlayQualifiable : boolean = true;
    private allowStopQualifiable : boolean = false;

    private enregistrementExistantOnSameDate : boolean;
    private heure : string;
    private heureDebutMoment;
    private enregistrementQualification : EnregistrementQualification = new EnregistrementQualification(666, 666, []);

    private enregistrementChaud : boolean = true;

    ngOnInit() {
        this.sujets = this.enregistrementFroidStoreService.sujets;
        this.tagsPersonnels = this.enregistrementFroidStoreService.tagsPersonnels;

        this.enregistrementFroidStoreService.enregistrementEmitter.subscribe(
            enregistrementExistant => {
                this.enregistrementExistant = enregistrementExistant;

                this.heureDebutMoment = moment.unix(enregistrementExistant["heureDebut"]);
                this.enregistrementExistantOnSameDate = moment().format('L') == this.heureDebutMoment.format('L');
                this.jourInstanceService.jourInstance$.subscribe(
                    (data) => {
                        this.heure = data["finPM"];
                    },
                    (err) => console.log(err)
                );
            }
        );
    }

    hideConfirmEnregistrement(){
        this.enregistrementService
            .deleteEnregistrement(this.enregistrementExistant.id)
            .subscribe(
                (rep) => {
                    this.enregistrementExistant = [];
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    continueEnregistrement(){
        this.disableQualifie = true;
        this.allowPlayQualifiable = false;
        this.allowStopQualifiable = true;

        this.timer.setTotalSecondes(Math.round(Date.now()/1000 - this.enregistrementExistant.heureDebut));
        this.timer.start();

        this.enregistrementExistant = [];
    }

    saveEnregistrementWithEndHour(){
        let heureFin = TimeConverter.fromStringToTimestamp(this.heureDebutMoment, this.heure);
        let data = {
            "heureFin" : heureFin,
            "duree" : heureFin - this.heureDebutMoment.unix(),
            "typeTravail" : this.enregistrementQualification.typeTravailSelection.typeTravail.id,
            "sujet" : this.enregistrementQualification.sujetId
        };

        this.enregistrementService.saveFinEnregistrement(this.enregistrementExistant["id"], data, this.enregistrementQualification.tagsPersoId);

        this.enregistrementExistant = [];
    }
}
