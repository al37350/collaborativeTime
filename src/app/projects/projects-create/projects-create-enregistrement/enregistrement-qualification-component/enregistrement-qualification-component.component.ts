import {Component, EventEmitter, Input, Output, OnInit, AfterViewInit} from '@angular/core';
import { EnregistrementQualification } from "../../../../_models/EnregistrementQualification";
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import {EnregistrementFroidStoreService} from "../../../../_services/EnregistrementFroidStore.service";
import {SujetService} from "../../../../_services/sujet.service";

@Component({
    selector: 'enregistrement-qualification',
    templateUrl: './enregistrement-qualification-component.component.html',
    styleUrls: ['./enregistrement-qualification-component.component.scss']
})
export class EnregistrementQualificationComponentComponent implements OnInit, AfterViewInit {

    @Input() enregistrementQualification: EnregistrementQualification;
    @Input() showDescription: boolean = false;
    @Output() onChangeQualification = new EventEmitter<EnregistrementQualification>();



    private sujets : any [];
    private typeTravails : any [];
    private  tagsPersonnels: IMultiSelectOption[] = [];

    tagsSelected: number[] = [];

    settings = {
        "enableSearch" : true
    };

    constructor(
        private enregistrementFroidStoreService : EnregistrementFroidStoreService,
        private sujetService : SujetService,
    ) { }

    ngOnInit() {
        this.tagsPersonnels = this.enregistrementFroidStoreService.tagsPersonnels;

        this.enregistrementFroidStoreService.sujetEmitter.subscribe(
            tabSujet => {
                if(tabSujet.length){
                    this.onChangeSujet(tabSujet[0].id);
                    this.sujets = this.groupBy(this.enregistrementFroidStoreService.sujets, item => [item.famille.nom]);
                }
            }
        );
    }

    onChangeSujet(idSujet){
        this.enregistrementQualification.sujetId = idSujet;
        this.onChangeQualification.emit(this.enregistrementQualification);
        this.sujetService
            .getTypeTravails(idSujet)
            .subscribe(
                (data) => {
                    this.typeTravails = data;
                    if(data.length){
                        this.onChangeTypeTravail(data[0].id);
                    }
                },
                (err) => console.log(err)
            );
    }

    onChangeTypeTravail(idTypeTravailSelection){
        let typeTravailSelect= this.typeTravails.find(typeTravail => typeTravail.id == idTypeTravailSelection);

        this.enregistrementQualification.typeTravailSelection = typeTravailSelect;
        this.onChangeQualification.emit(this.enregistrementQualification);
    }

    onChangeTextarea(description : string){
        this.enregistrementQualification.description = description;
        this.onChangeQualification.emit(this.enregistrementQualification);
    }

    public onChange() {
        this.enregistrementQualification.tagsPersoId = this.tagsSelected;
        this.onChangeQualification.emit(this.enregistrementQualification);
    }

    groupBy( array , f )
    {
        var groups = {};
        array.forEach( function( o )
        {
            var group = JSON.stringify( f(o) );
            groups[group] = groups[group] || [];
            groups[group].push( o );
        });
        return Object.keys(groups).map( group => {
            return {
                "nom" : JSON.parse(group)[0],
                "values" : groups[group]
            }
        });
    }
    ngAfterViewInit() {
        $('.selectpicker').selectpicker({
            style: 'btn-default',
            size: 4
        });
    }
}
