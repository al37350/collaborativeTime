import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnregistrementQualificationComponentComponent } from './enregistrement-qualification-component.component';

describe('EnregistrementQualificationComponentComponent', () => {
  let component: EnregistrementQualificationComponentComponent;
  let fixture: ComponentFixture<EnregistrementQualificationComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnregistrementQualificationComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnregistrementQualificationComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
