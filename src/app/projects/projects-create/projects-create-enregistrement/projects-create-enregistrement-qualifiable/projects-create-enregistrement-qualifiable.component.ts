import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Timer} from "../../../../_models/timer";
import * as moment from 'moment';
import {CurrentUserService} from "../../../../_services/currentUser.service";
import {EnregistrementService} from "../../../../_services/enregistrement.service";
import {JourInstanceService} from "../../../../_services/jourInstance.service";
import {AlertService} from "../../../../_services/alert.service";
import {EnregistrementQualification} from "../../../../_models/EnregistrementQualification";
import {EnregistrementFroidStoreService} from "../../../../_services/EnregistrementFroidStore.service";
import {SharedService} from "../../../../_services/shared.service";

@Component({
    selector: 'projects-create-enregistrement-qualifiable',
    templateUrl: './projects-create-enregistrement-qualifiable.component.html',
    styleUrls: ['./projects-create-enregistrement-qualifiable.component.scss']
})
export class ProjectsCreateEnregistrementQualifiableComponent implements OnInit {
    
    /*Pour récupération enregistrement en cours*/
    @Input() allowPlay : boolean = true;
    @Input() allowStop : boolean = false;

    @Input() timer: Timer;
    @Input() disable: boolean = true;

    @Output() isRecording = new EventEmitter<boolean>();

    private dateDebut : number;
    private dateFin: number;

    private jourInstanceId : number;
    private idEnregistrement: number;

    private showModal:boolean = false;
    private enregistrementQualification : EnregistrementQualification = new EnregistrementQualification(666, 666, []);

    constructor(
        private currentUserService : CurrentUserService,
        private jourInstanceService : JourInstanceService,
        private enregistrementService : EnregistrementService,
        private alertService : AlertService,
        private enregistrementFroidStoreService: EnregistrementFroidStoreService,
        private sharedService : SharedService
    ) { }

    ngOnInit() {
        this.jourInstanceService.getJourInstanceFromMoment(moment()).subscribe(
            (data) => {
                this.jourInstanceId = data["id"];
            },
            (err) => console.log(err)
        );

        this.enregistrementFroidStoreService.enregistrementEmitter.subscribe(
            enregistrementExistant => {
                this.dateDebut = enregistrementExistant["heureDebut"];
                this.idEnregistrement = enregistrementExistant["id"];
            }
        );
    }

    public startOrStopEnregistrement(isRecording : boolean){
        if(isRecording){
            this.activeAndSaveChrono();
        }
        else{
            this.showModal = true;
        }
    }

    activeAndSaveChrono(){
        this.dateDebut = moment().unix();
        this.timer.start();
        this.isRecording.emit(true);
        this.sharedService.enregistrementFroidPossible = false;

        this.saveDebutEnregistremment();
    }

    stopAndSaveChrono(){
        this.dateFin = moment().unix();
        this.timer.stop();
        this.timer.reset();
        this.isRecording.emit(true);
        this.sharedService.enregistrementFroidPossible = false;

        this.saveFinEnregistremment();
    }

    private saveDebutEnregistremment(){
        let data = {
            "jourInstance" : this.jourInstanceId,
            "heureDebut" : this.dateDebut,
            "utilisateur" : this.currentUserService.getUser().id
        };

        this.enregistrementService
            .postEnregistrement(data)
            .subscribe(
                (rep) => {
                    this.idEnregistrement = rep.id;
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );
    }

    saveFinEnregistremment(){
        let data = {
            "heureFin" : this.dateFin,
            "duree" : this.dateFin - this.dateDebut,
            "sujet" : this.enregistrementQualification.typeTravailSelection.sujet.id,
            "typeTravail" : this.enregistrementQualification.typeTravailSelection.typeTravail.id,
            "caracteristiques" : this.enregistrementQualification.typeTravailSelection.caracteristiques.map(cara => cara.id),
            "description" : this.enregistrementQualification.description
        };

        this.enregistrementService.saveFinEnregistrement(this.idEnregistrement, data, this.enregistrementQualification.tagsPersoId);
        this.showModal = false;
        this.sharedService.enregistrementFroidPossible = true;
    }
}
