import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsCreateEnregistrementQualifiableComponent } from './projects-create-enregistrement-qualifiable.component';

describe('ProjectsCreateEnregistrementQualifiableComponent', () => {
  let component: ProjectsCreateEnregistrementQualifiableComponent;
  let fixture: ComponentFixture<ProjectsCreateEnregistrementQualifiableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsCreateEnregistrementQualifiableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsCreateEnregistrementQualifiableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
