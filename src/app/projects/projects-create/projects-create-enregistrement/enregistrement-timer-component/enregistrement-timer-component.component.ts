import { Component, OnInit, Input } from '@angular/core';
import {Timer} from "../../../../_models/timer";

@Component({
  selector: 'enregistrement-timer',
  templateUrl: './enregistrement-timer-component.component.html',
  styleUrls: ['./enregistrement-timer-component.component.scss']
})
export class EnregistrementTimerComponentComponent implements OnInit {

  @Input() timer: Timer;

  constructor() { }

  ngOnInit() {
  }

}
