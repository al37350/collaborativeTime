import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnregistrementTimerComponentComponent } from './enregistrement-timer-component.component';

describe('EnregistrementTimerComponentComponent', () => {
  let component: EnregistrementTimerComponentComponent;
  let fixture: ComponentFixture<EnregistrementTimerComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnregistrementTimerComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnregistrementTimerComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
