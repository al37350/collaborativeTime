import { Component, OnInit } from '@angular/core';
import {JourInstanceService} from "../../_services/jourInstance.service";
import {MotifService} from "../../_services/motif.service";
import {PeriodeOffService} from "../../_services/periodeOff.service";
import {TimeConverter} from "../../_models/TimeConverter";
import * as moment from 'moment';
import {AlertService} from "../../_services/alert.service";

@Component({
    selector: 'app-dashbord-edit-jour-instance',
    templateUrl: './dashbord-edit-jour-instance.component.html',
    styleUrls: ['./dashbord-edit-jour-instance.component.scss']
})
export class DashbordEditJourInstanceComponent implements OnInit {

    private jourInstance;
    private motifs;
    private idMotif;

    constructor(
        private alertService : AlertService,
        private jourInstanceService : JourInstanceService,
        private motifService : MotifService,
        private periodeOffService : PeriodeOffService
    ) { }

    ngOnInit() {
        this.jourInstanceService.jourInstance$.subscribe(
            (data) => {
                this.jourInstance = data;
            },
            (err) => console.log(err)
        );

        this.motifs = this.motifService.getMotifs();
    }

    get date_formatted() : string {
        return this.jourInstanceService.getMoment().format('dddd DD MMM')
    }

    onSubmit() {
        let data = {
            "debutAM" : this.jourInstance.debutAM,
            "finAM" : this.jourInstance.finAM,
            "debutPM" : this.jourInstance.debutPM,
            "finPM" : this.jourInstance.finPM
        };

        this.jourInstanceService.patchJourInstance(this.jourInstance.id, data);

    }

    onChangeMotif(idMotif){
        this.idMotif = idMotif;
    }

    choisirMotif(){
        let moment = this.jourInstanceService.getMoment();

        let heureDebMatin = TimeConverter.fromStringToTimestamp(moment, this.jourInstance.debutAM);
        let heureFinMatin = TimeConverter.fromStringToTimestamp(moment, this.jourInstance.finAM);
        let heureDebAprem = TimeConverter.fromStringToTimestamp(moment, this.jourInstance.debutPM);
        let heureFinAprem = TimeConverter.fromStringToTimestamp(moment, this.jourInstance.finPM);

        this.periodeOffService
            .postPeriodeOff({
                "jourInstance" : this.jourInstance.id,
                "heureDebut" : heureDebMatin,
                "heureFin" : heureFinMatin,
                "duree" : heureFinMatin - heureDebMatin,
                "motif" : this.idMotif
            })
            .subscribe(
                (rep) => {
                    this.alertService.success("Matinée enregistrée")
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );

        this.periodeOffService
            .postPeriodeOff({
                "jourInstance" : this.jourInstance.id,
                "heureDebut" : heureDebAprem,
                "heureFin" : heureFinAprem,
                "duree" : heureFinAprem - heureDebAprem,
                "motif" : this.idMotif
            })
            .subscribe(
                (rep) => {
                    this.alertService.success("Après-midi enregistré")
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );
    }

}
