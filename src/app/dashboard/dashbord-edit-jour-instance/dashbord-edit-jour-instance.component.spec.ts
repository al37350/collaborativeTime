import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashbordEditJourInstanceComponent } from './dashbord-edit-jour-instance.component';

describe('DashbordEditJourInstanceComponent', () => {
  let component: DashbordEditJourInstanceComponent;
  let fixture: ComponentFixture<DashbordEditJourInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashbordEditJourInstanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashbordEditJourInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
