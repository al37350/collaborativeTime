import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Month} from "./models/month";
import * as moment from 'moment';
import {JourInstanceService} from "../../../_services/jourInstance.service";

@Component({
    selector: 'dashboard-datepicker-agenda',
    templateUrl: './dashboard-datepicker-agenda.component.html',
    styleUrls: ['./dashboard-datepicker-agenda.component.scss']
})
export class DashboardDatepickerAgendaComponent implements OnInit {
    @Input() private date;
    @Output()
    change: EventEmitter<number> = new EventEmitter<number>();

    @Output()
    hideDatePicker: EventEmitter<number> = new EventEmitter<number>();

    private month;
    private dayofMonth;

    get year() : string { return this.date.format('YYYY')}
    get date_formatted() : string {
        return this.date.format('dddd DD MMM')
    }

    get days() { return ['L', 'M', 'M', 'J', 'V', 'S', 'D'] }

    constructor(private jourInstanceService : JourInstanceService) {

    }

    ngOnInit() {
        this.month = new Month(this.date.month(), this.date.year());
        this.chargeMonthDays(moment({years:this.date.year(), months :this.date.month()}));
    }

    isSelected(day){
        return this.date.unix() === day.unix();
    }

    selectDate(day){
        this.date=day;
        this.change.emit(this.date);
    }

    nextMonth(){
        let month = this.month.month +1;
        let year = this.month.year;

        if(month > 11){
            year+= 1;
            month = 0;
        }
        this.month = new Month(month, year);
        let momentNewMois = moment({years:year, months :month});
        //this.jourInstanceService.setMoment(momentNewMois);
        this.chargeMonthDays(momentNewMois);
    }

    prevMonth(){
        let month = this.month.month - 1;
        let year = this.month.year;

        if(month < 0){
            year-= 1;
            month = 11;
        }
        this.month = new Month(month, year);
        let momentNewMois = moment({years:year, months :month});
        //this.jourInstanceService.setMoment(momentNewMois);
        this.chargeMonthDays(momentNewMois);
    }

    chargeMonthDays(moment){
        this.jourInstanceService.validePeriodeMoisInstance(moment).subscribe(
            (data) => {
                let rep = [];
                let allDaysMonth = this.month.getDays();
                allDaysMonth.map(dayMonth =>{
                    rep.push({
                        "jour" : dayMonth,
                        "valide" : this.ifDayMonthHasCoherenceResponse(dayMonth, data)
                    });
                });
                this.dayofMonth = rep;
            },
            (err) => console.log(err)
        );
    }

    private ifDayMonthHasCoherenceResponse(momentDayMonth , listOfCoherenceDay){
        for(let coherenceDay in listOfCoherenceDay){
            if(parseInt(coherenceDay) == momentDayMonth.format('D')){
                return listOfCoherenceDay[coherenceDay]
            }
        }
        return null
    }
}
