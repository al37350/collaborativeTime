import {Component, OnInit, OnDestroy} from '@angular/core';
import {AlertService} from "../../_services/alert.service";
import * as moment from 'moment';
import {TimeConverter} from "../../_models/TimeConverter";
import {SharedPeriodeTempsService} from "../../_services/shared.periodeTemps.service";

@Component({
    selector: 'app-dashboard-pie-enregistrement',
    templateUrl: './dashboard-pie-enregistrement.component.html',
    styleUrls: ['./dashboard-pie-enregistrement.component.scss']
})
export class DashboardPieEnregistrementComponent implements OnInit {

    private jourInstance;
    private periodeTempsStats =[];
    private data=[];

    options: Object;

    private show : boolean = false;

    constructor(
        private alertService : AlertService,
        private sharedEnregistremmentService : SharedPeriodeTempsService
    ) {}

    ngOnInit() {
        this.sharedEnregistremmentService.periodesTempsEmitter
            .subscribe(
                (data) => {
                    this.periodeTempsStats =[];
                    data.map(enregistrement => {
                        if(enregistrement.sujet != null){
                            let idPeriodeTemps = enregistrement.sujet === undefined ? enregistrement.motif.id: enregistrement.sujet.id;
                            let sujetStore = this.periodeTempsStats.filter(
                                sujet => sujet.id == idPeriodeTemps &&
                                ((enregistrement.sujet === undefined ? 'PeriodeOff': 'Enregistrement') == sujet.type)
                            );

                            if(sujetStore.length){
                                sujetStore[0].dureeTotale += enregistrement.duree;
                            }
                            else{
                                this.periodeTempsStats.push({
                                    type: enregistrement.sujet === undefined ? 'PeriodeOff': 'Enregistrement',
                                    id: idPeriodeTemps,
                                    dureeTotale:enregistrement.duree,
                                    color: enregistrement.sujet === undefined ? '#000': enregistrement.sujet.famille.couleur,
                                    name: enregistrement.sujet === undefined ?  enregistrement.motif.nom :  enregistrement.sujet.nom,
                                })
                            }
                        }
                    });
                    this.data.length = 0;

                    this.periodeTempsStats.sort((a,b) => a.dureeTotale - b.dureeTotale).map(periodeTemps => {
                        this.data.push(
                            {
                                name : periodeTemps.name,
                                data: [periodeTemps.dureeTotale],
                                color: periodeTemps.color
                            }
                        )
                    });
                    this.show = this.data.length != 0;

                    this.configureChart();
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );
    }

    private configureChart(){
        this.options = {
            chart: {
                type: 'bar',
                height: '200px',
                margin: [0,0,0,0]
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            tooltip:{
                formatter:
                    function () {
                        return "<b>" + this.series.name + " :</b> " + TimeConverter.fromSecondsToHms(this.y);
                    }
            },
            exporting: { enabled: false },
            yAxis: {
                min: 0,
                visible: false
            },
            xAxis: {
                minPadding:0,
                maxPadding:0,
                visible: false
            },
            legend: {
                enabled: true
            },

            plotOptions: {
                series: {
                    stacking: 'normal',
                    pointWidth: 40
                },
                /*bar: {
                    dataLabels: {
                        enabled: true,
                        distance : -50,
                        formatter: function() {
                            return this.series.name;
                        },
                        style: {
                            color: 'white',
                        },
                    },

                },*/
            },

            series: this.data

        };
    }
}
