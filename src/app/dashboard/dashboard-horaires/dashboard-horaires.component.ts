import { Component, OnInit } from '@angular/core';
import {JourInstanceService} from "../../_services/jourInstance.service";
import {PeriodeTempsService} from "../../_services/periodeTemps.service";
import {AlertService} from "../../_services/alert.service";
import * as moment from 'moment';
import {TimeConverter} from "../../_models/TimeConverter";
import {PeriodeTempsCollection} from "../../_models/PeriodeTempsCollection";
import {SharedPeriodeTempsService} from "../../_services/shared.periodeTemps.service";

@Component({
    selector: 'dashboard-horaires',
    templateUrl: './dashboard-horaires.component.html',
    styleUrls: ['./dashboard-horaires.component.scss']
})
export class DashboardHorairesComponent implements OnInit {

    private jourInstance;

    totalTimeEnregistrements = 0;
    totalTimeJourInstance = 0;

    constructor(
        private jourInstanceService : JourInstanceService,
        private sharedEnregistremmentService : SharedPeriodeTempsService,
        private alertService: AlertService,
    ) { }

    ngOnInit() {
        this.jourInstanceService.jourInstance$.subscribe(
            (data) => {
                if(Object.keys(data).length) {
                    this.jourInstance = data;

                    let date = moment(this.jourInstance.date);
                    this.totalTimeJourInstance =
                        (
                            TimeConverter.fromStringToTimestamp(date, this.jourInstance.finAM)
                            - TimeConverter.fromStringToTimestamp(date, this.jourInstance.debutAM)
                        )
                        +
                        (
                            TimeConverter.fromStringToTimestamp(date, this.jourInstance.finPM)
                            - TimeConverter.fromStringToTimestamp(date, this.jourInstance.debutPM)
                        );

                    this.loadEnregistrements();
                }
            },
            (err) => console.log(err)
        );
    }

    loadEnregistrements(){
        this.sharedEnregistremmentService.periodesTempsEmitter
            .subscribe(
                (data) => this.formatData(data),
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );
    }

    changeHoraireJour() {
        let data = {
            "debutAM" : this.jourInstance.debutAM,
            "finAM" : this.jourInstance.finAM,
            "debutPM" : this.jourInstance.debutPM,
            "finPM" : this.jourInstance.finPM
        };

        this.jourInstanceService.patchJourInstance(this.jourInstance.id, data);
    }

    private formatData(data){
        {
            this.totalTimeEnregistrements = 0;

            let periodeTempsCollection = (new PeriodeTempsCollection).fromJSON(data);
            periodeTempsCollection.enregistrements.map(
                enregistrement =>  {
                    this.totalTimeEnregistrements += moment.duration(enregistrement.heureFin.diff(enregistrement.heureDebut), 'seconds').asSeconds()/1000;
                }

            );

            periodeTempsCollection.periodesOff.map(
                periodeOff =>  {
                    this.totalTimeEnregistrements += moment.duration(periodeOff.heureFin.diff(periodeOff.heureDebut), 'seconds').asSeconds()/1000;
                }
            );
        }
    }

}
