import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardHorairesComponent } from './dashboard-horaires.component';

describe('DashboardHorairesComponent', () => {
  let component: DashboardHorairesComponent;
  let fixture: ComponentFixture<DashboardHorairesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardHorairesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardHorairesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
