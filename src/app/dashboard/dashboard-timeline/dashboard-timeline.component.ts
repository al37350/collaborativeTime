import {Component, OnInit, OnDestroy} from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import { Subject } from 'rxjs/Subject';
import {JourInstanceService} from "../../_services/jourInstance.service";
import {AlertService} from "../../_services/alert.service";
import {PeriodeTempsService} from "../../_services/periodeTemps.service";
import {PeriodeTempsCollection} from "../../_models/PeriodeTempsCollection";
import {ISubscription} from "rxjs/Subscription";
import * as moment from 'moment';
import {TimeConverter} from "../../_models/TimeConverter";
import {Router} from "@angular/router";
import {EnregistrementSharedService} from "../../_services/enregistrementShared";
import {SharedPeriodeTempsService} from "../../_services/shared.periodeTemps.service";

interface CustomCalendarEvent extends CalendarEvent {
    id: number;
}

export interface CustomCalendarEventTimesChangedEvent {
    event: CustomCalendarEvent;
    newStart: Date;
    newEnd?: Date;
}

@Component({
    selector: 'dashboard-timeline',
    templateUrl: './dashboard-timeline.component.html',
    styleUrls: ['./dashboard-timeline.component.scss']
})

export class DashboardTimelineComponent implements OnInit, OnDestroy {
    private subscription: ISubscription;

    private jourInstance;
    private dayStartHour : number = 8;
    private dayStartMinute : number = 0;
    private dayEndHour : number = 20;
    private dayEndMinute : number = 0;

    viewDate: Date;
    events: CustomCalendarEvent[] = [];

    totalTimeEnregistrements = 0;
    totalTimeJourInstance = 0;


    refresh: Subject<any> = new Subject();
    constructor(
        private jourInstanceService : JourInstanceService,
        private periodeTempsService : PeriodeTempsService,
        private sharedPeriodeTempsService : SharedPeriodeTempsService,
        private alertService: AlertService
    ) { }

    ngOnInit() {
        this.getJourInstance();
    }

    eventTimesChanged({event, newStart, newEnd}: CustomCalendarEventTimesChangedEvent): void {
        this.dragAndDropEvent(event, newStart, newEnd);
    }

    getJourInstance(){
        this.subscription = this.jourInstanceService.jourInstance$.subscribe(
            (data) => {
                if(Object.keys(data).length) {
                    this.viewDate = new Date(data["date"]);
                    this.jourInstance = data;

                    let date = moment(this.jourInstance.date);
                    this.totalTimeJourInstance =
                        (
                            TimeConverter.fromStringToTimestamp(date, this.jourInstance.finAM)
                            - TimeConverter.fromStringToTimestamp(date, this.jourInstance.debutAM)
                        )
                        +
                        (
                            TimeConverter.fromStringToTimestamp(date, this.jourInstance.finPM)
                            - TimeConverter.fromStringToTimestamp(date, this.jourInstance.debutPM)
                        );
                    [this.dayStartHour, this.dayStartMinute] = this.splitTimeString(data["debutAM"]);
                    [this.dayEndHour, this.dayEndMinute] = this.splitTimeString(data["finPM"]);

                    this.loadEnregistrements();
                }
                else{
                    this.events.length = 0;
                    this.refresh.next();
                }
            },
            (err) => console.log(err)
        );
    }

    loadEnregistrements(){
        this.sharedPeriodeTempsService.periodesTempsEmitter
            .subscribe(
                (data) => this.formatData(data),
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );
    }

    private deletePeriodeTemps(id : number){
        this.periodeTempsService
            .deletePeriodeTemps(id)
            .subscribe(
                (rep) => {
                    this.events = this.events.filter(iEvent => iEvent.id !== id);
                    this.sharedPeriodeTempsService.deletePeriodeTemps(id);
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    private dragAndDropEvent(event : CustomCalendarEvent, startEnregistrementDate, endEnregistrementDate){
        let data = {
            "heureDebut" : startEnregistrementDate.getTime()/1000,
            "heureFin" : endEnregistrementDate.getTime()/1000,
            "duree" : ( endEnregistrementDate.getTime() - startEnregistrementDate.getTime() )/1000
        };

        this.periodeTempsService
            .patchPeriodeTemps(event.id, data)
            .subscribe(
                (rep) => {
                    this.events = this.events.map(iEvent => {
                        if(iEvent.id === event.id){
                            event.start = startEnregistrementDate;
                            event.end = endEnregistrementDate;
                        }
                        return iEvent;
                    });
                    this.refresh.next();

                    this.sharedPeriodeTempsService.replacePeriodeTemps(rep);
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    private splitTimeString(time : string){
        return time.split(":").map( value => parseInt(value));
    }

    private recoller(){
        this.periodeTempsService.recollerPeriodeTempsJourInstance(this.jourInstance.id).subscribe(
            (data) => this.formatData(data)
        )
    }

    private formatData(data){
        {
            this.events.length = 0;
            this.totalTimeEnregistrements = 0;

            let periodeTempsCollection = (new PeriodeTempsCollection).fromJSON(data);
            periodeTempsCollection.enregistrements.map(
                enregistrement =>  {
                    this.totalTimeEnregistrements += moment.duration(enregistrement.heureFin.diff(enregistrement.heureDebut), 'seconds').asSeconds()/1000;
                    this.events.push({
                        id: enregistrement.id,
                        title: enregistrement.sujet + " - " + enregistrement.typeTravail,
                        color:
                        {
                            primary: "#777",
                            secondary: enregistrement.couleur
                        },
                        start: enregistrement.heureDebut.toDate(),
                        end: enregistrement.heureFin.toDate(),
                        draggable: true ,
                        resizable: {
                            beforeStart: true, // this allows you to configure the sides the event is resizable from
                            afterEnd: true
                        },
                        actions: [
                            {
                                label: '<i class=\'glyphicon glyphicon-remove\'></i>',
                                onClick: ({event}: {event: CustomCalendarEvent}): void => {
                                    this.deletePeriodeTemps(event.id);
                                }
                            }
                        ]
                    });
                }

            );

            periodeTempsCollection.periodesOff.map(
                periodeOff =>  {
                    this.events.push({
                        id: periodeOff.id,
                        title: periodeOff.motif,
                        color:
                        {
                            primary: "#777",
                            secondary: periodeOff.couleur
                        },
                        start: periodeOff.heureDebut.toDate(),
                        end: periodeOff.heureFin.toDate(),
                        draggable: true ,
                        resizable: {
                            beforeStart: true, // this allows you to configure the sides the event is resizable from
                            afterEnd: true
                        },
                        actions: [{
                            label: '<i class=\'glyphicon glyphicon-remove\'></i>',
                            onClick: ({event}: {event: CustomCalendarEvent}): void => {
                                this.deletePeriodeTemps(event.id);
                            }
                        }]
                    });
                }
            );
            this.refresh.next();
        }
    }

    ngOnDestroy(){
        this.subscription.unsubscribe();
    }
}
