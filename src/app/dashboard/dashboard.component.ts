import {Component, OnInit, OnDestroy} from '@angular/core';
import * as moment from 'moment';
import {DateSelectedPickerService} from "../_services/dateSelectedPickerService.service";
import {JourInstanceService} from "../_services/jourInstance.service";
import {CurrentUserService} from "../_services/currentUser.service";
import {ISubscription} from "rxjs/Subscription";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
    private subscription: ISubscription;

    private jour;
    private heure;
    private links;

    private jourInstance;
    private userGroupes = [];

    constructor(
        private dateSelectedPickerService : DateSelectedPickerService,
        private jourInstanceService : JourInstanceService,
        private currentUserService : CurrentUserService
    ) {
        this.links = [
            {
                path : 'froid',
                name : 'Mode précis',
                outlet: {outlets: { "enregistrement": ["precis"] }}
            },
            {
                path : 'froid',
                name : 'Saisie à la durée',
                outlet: {outlets: { "enregistrement": ["duree"] }}
            },
        ];
    }

    ngOnInit() {
        this.jour = moment().format('L');
        this.heure = moment().unix();

        this.subscription = this.jourInstanceService.jourInstance$.subscribe(
            (data) => {
                this.jourInstance = data;
            },
            (err) => console.log(err)
        );


        this.currentUserService.getUser().groupes.map(
          groupe => this.userGroupes.push(groupe.nom)
        );
    }

    dateChange(event){
        this.jourInstanceService.setMoment(event);
        this.dateSelectedPickerService.setDate(event);
    }

    ngOnDestroy(){
        this.subscription.unsubscribe();
    }

}
