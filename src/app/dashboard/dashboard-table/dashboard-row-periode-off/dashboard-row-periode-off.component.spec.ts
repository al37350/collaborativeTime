import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRowPeriodeOffComponent } from './dashboard-row-periode-off.component';

describe('DashboardRowPeriodeOffComponent', () => {
  let component: DashboardRowPeriodeOffComponent;
  let fixture: ComponentFixture<DashboardRowPeriodeOffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardRowPeriodeOffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardRowPeriodeOffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
