import { Component, OnInit, Input } from '@angular/core';
import {PeriodeTempsService} from "../../../_services/periodeTemps.service";
import {AlertService} from "../../../_services/alert.service";
import {JourInstanceService} from "../../../_services/jourInstance.service";
import {EnregistrementService} from "../../../_services/enregistrement.service";
import {TimeConverter} from "../../../_models/TimeConverter";
import * as moment from 'moment';
import {MotifService} from "../../../_services/motif.service";
import {PeriodeOffService} from "../../../_services/periodeOff.service";
import {SharedPeriodeTempsService} from "../../../_services/shared.periodeTemps.service";


@Component({
    selector: 'table-row-periode-off',
    templateUrl: './dashboard-row-periode-off.component.html',
    styleUrls: ['./dashboard-row-periode-off.component.scss']
})
export class DashboardRowPeriodeOffComponent implements OnInit {

    @Input() enregistrement;

    heureDebut;
    heureFin;

    showDelete : boolean = false;

    motifs;
    motifSelected : number;
    changerMotif : boolean = false;
    nombre: number = 0;

    dureeStyle = {
        display: 'inline-block',
        cursor: 'pointer',
        margin: 0,
        padding: 0,
        "font-size": "0.8em",
        "color": "grey"
    };

    constructor(
        private periodeTempsService : PeriodeTempsService,
        private motifService : MotifService,
        private jourInstanceService : JourInstanceService,
        private alertService : AlertService,
        private periodeOffService : PeriodeOffService,
        private sharedPeriodeTempsService : SharedPeriodeTempsService
    ) {}

    ngOnInit() {
        this.heureDebut = moment.unix(parseInt(this.enregistrement.data.heureDebut)).format('LT');
        this.heureFin = moment.unix(parseInt(this.enregistrement.data.heureFin)).format('LT');

        this.motifs = this.motifService.getMotifs();
        this.motifSelected = this.enregistrement.data.motif.id;
    }

    delete(){
        this.showDelete=false;
        this.periodeTempsService
            .deletePeriodeTemps(this.enregistrement.data.id)
            .subscribe(
                (rep) => {
                    this.sharedPeriodeTempsService.deletePeriodeTemps(this.enregistrement.data.id);
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    changeDureeEnregistrement(dureeInput){
        let duree = TimeConverter.fromStringToSeconds(dureeInput, "h");

        let data = {
            "duree" : duree,
            "heureFin" : moment((parseInt(this.enregistrement.data.heureDebut) + duree) * 1000).unix()
        };
        this.patchPeriodeOff(this.enregistrement.data.id, data)
    }

    changeDebutEnregistrement(debutInput){
        let moment = this.jourInstanceService.getMoment();

        let debEnregistrement = TimeConverter.fromStringToTimestamp(moment, debutInput);

        let data = {
            "heureDebut" : debEnregistrement,
            "duree": this.enregistrement.data.heureFin - debEnregistrement
        };

        this.patchPeriodeOff(this.enregistrement.data.id, data)
    }

    changeFinEnregistrement(finInput){
        let moment = this.jourInstanceService.getMoment();

        let finEnregistrement = TimeConverter.fromStringToTimestamp(moment, finInput);

        let data = {
            "heureFin" : finEnregistrement,
            "duree": finEnregistrement - this.enregistrement.data.heureDebut
        };

        this.patchPeriodeOff(this.enregistrement.data.id, data)
    }

    patchPeriodeOff(id : number, data){
        this.periodeTempsService
            .patchPeriodeTemps(id,data)
            .subscribe(
                (rep) => {
                    this.sharedPeriodeTempsService.replacePeriodeTemps(rep);

                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );
    }

    afficherChangementMotif(){
        this.changerMotif = true;
    }

    quitterChangementMotif(){
        this.nombre++;
        if(this.nombre>1){
            this.changerMotif = false;
            this.nombre=0;
        }
    }

    onChangeMotif(idMotif){
        this.motifSelected = idMotif;
    }

    enregistrer(){
        let data = {
            "motif" : this.motifSelected
        };

        this.periodeOffService
            .patchPeriodeOff(this.enregistrement.data.id,data)
            .subscribe(
                (rep) => {
                    this.sharedPeriodeTempsService.replacePeriodeTemps(rep);

                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );
    }
}
