import { Component, OnInit } from '@angular/core';
import {AlertService} from "../../_services/alert.service";
import {SharedPeriodeTempsService} from "../../_services/shared.periodeTemps.service";

@Component({
    selector: 'dashboard-table',
    templateUrl: './dashboard-table.component.html',
    styleUrls: ['./dashboard-table.component.scss']
})
export class DashboardTableComponent implements OnInit {

    private periodesTemps =[];

    private showCommentaires : boolean =false;

    constructor(
        private sharedEnregistremmentService : SharedPeriodeTempsService,
        private alertService : AlertService
    ) {}

    ngOnInit() {
        this.sharedEnregistremmentService.periodesTempsEmitter
            .subscribe(
                (data) => {
                    this.periodesTemps =[];
                    data.map(periodeTemps => {
                        if(periodeTemps.sujet !== undefined && periodeTemps.sujet != null) {
                            this.periodesTemps.push({
                                type: periodeTemps.sujet === undefined ? 'PeriodeOff' : 'Enregistrement',
                                data: periodeTemps,
                            })
                        }
                    });
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );
    }

}
