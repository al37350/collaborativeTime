import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRowEnregistrementComponent } from './dashboard-row-enregistrement.component';

describe('DashboardRowEnregistrementComponent', () => {
  let component: DashboardRowEnregistrementComponent;
  let fixture: ComponentFixture<DashboardRowEnregistrementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardRowEnregistrementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardRowEnregistrementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
