import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import {PeriodeTempsService} from "../../../_services/periodeTemps.service";
import {AlertService} from "../../../_services/alert.service";
import {JourInstanceService} from "../../../_services/jourInstance.service";
import {CurrentUserService} from "../../../_services/currentUser.service";
import {UtilisateurService} from "../../../_services/utilisateur.service";
import { ApiURL } from '../../../globals';
import {HttpClient} from "../../../_services/HttpClient";
import {Observable} from "rxjs/Observable";
import {EnregistrementFroidStoreService} from "../../../_services/EnregistrementFroidStore.service";
import {EnregistrementService} from "../../../_services/enregistrement.service";
import {SujetService} from "../../../_services/sujet.service";
import {SharedService} from "../../../_services/shared.service";
import {TimeConverter} from "../../../_models/TimeConverter";
import * as moment from 'moment';
import {SharedPeriodeTempsService} from "../../../_services/shared.periodeTemps.service";

@Component({
    selector: 'table-row-enregistrement',
    templateUrl: './dashboard-row-enregistrement.component.html',
    styleUrls: ['./dashboard-row-enregistrement.component.scss']
})
export class DashboardRowEnregistrementComponent implements OnInit, AfterViewInit {

    @Input() enregistrement;
    @Input() showCommentaire : boolean = false;

    heureDebut;
    heureFin;

    showDelete : boolean = false;

    changerTypeTravail : boolean = false;
    nombre: number = 0;
    private typeTravailSelected : number;
    private sujetSelected : number;
    private typeTravailsSelection : any [];

    commentaireStyle = {
        display: 'inline-block',
        cursor: 'pointer',
        margin: 0,
        padding: 0,
        "min-width":'100%',
        "min-height": '20px',
        "font-size": "0.8em",
        "color": "grey"
    };

    dureeStyle = {
        display: 'inline-block',
        cursor: 'pointer',
        margin: 0,
        padding: 0,
        "font-size": "0.8em",
        "color": "grey"
    };

    constructor(
        public http: HttpClient,
        private periodeTempsService : PeriodeTempsService,
        private enregistrementFroidStoreService : EnregistrementFroidStoreService,
        private utilisateurService: UtilisateurService,
        private enregistrementService : EnregistrementService,
        private jourInstanceService : JourInstanceService,
        private alertService : AlertService,
        private sujetService: SujetService,
        private sharedService : SharedService,
        private sharedPeriodeTempsService : SharedPeriodeTempsService
    ) {}

    ngOnInit() {
        this.heureDebut = moment.unix(parseInt(this.enregistrement.data.heureDebut)).format('LT');
        this.heureFin = moment.unix(parseInt(this.enregistrement.data.heureFin)).format('LT');

        //this.typeTravailSelected = this.enregistrement.data.typeTravailSelection.id;
        this.typeTravailSelected = 1;
        this.sujetSelected = this.enregistrement.data.sujet.id;
    }

    ngAfterViewInit(){
        /*('.selectpicker').selectpicker({
            size: 4
        });*/
    }

    delete(){
        this.showDelete=false;
        this.periodeTempsService
            .deletePeriodeTemps(this.enregistrement.data.id)
            .subscribe(
                (rep) => {
                    this.sharedPeriodeTempsService.deletePeriodeTemps(this.enregistrement.data.id);
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.error(message.message);
                }
            );
    }

    removeTag(tag){
        this.enregistrementService.deleteEnregistrementTagPerso(this.enregistrement.data.id, tag.id).subscribe();
    }

    addTag(tag){
        this.enregistrementService.postEnregistrementTagPerso(this.enregistrement.data.id, tag.id).subscribe();
    }

    changeDureeEnregistrement(dureeInput){
        let duree = TimeConverter.fromStringToSeconds(dureeInput, "h");

        let data = {
            "duree" : duree,
            "heureFin" : moment((parseInt(this.enregistrement.data.heureDebut) + duree) * 1000).unix()
        };
        this.patchEnregistrement(this.enregistrement.data.id, data)
    }

    changeDebutEnregistrement(debutInput){
        let moment = this.jourInstanceService.getMoment();

        let debEnregistrement = TimeConverter.fromStringToTimestamp(moment, debutInput);

        let data = {
            "heureDebut" : debEnregistrement,
            "duree": this.enregistrement.data.heureFin - debEnregistrement
        };

        this.patchEnregistrement(this.enregistrement.data.id, data)
    }

    changeFinEnregistrement(finInput){
        let moment = this.jourInstanceService.getMoment();

        let finEnregistrement = TimeConverter.fromStringToTimestamp(moment, finInput);

        let data = {
            "heureFin" : finEnregistrement,
            "duree": finEnregistrement - this.enregistrement.data.heureDebut
        };

        this.patchEnregistrement(this.enregistrement.data.id, data)
    }

    changeCommentaire(commentaireInput){
        let data = {
            "description" : commentaireInput
        };

        this.patchEnregistrement(this.enregistrement.data.id, data)
    }

    /* Partie consacrée au changement de type de travail */

    afficherChangementTypeTravail(){
        this.onChangeSujet(this.sujetSelected);
        this.changerTypeTravail = true;

        /*$('.selectpicker').selectpicker({
            size: 4
        });*/
    }

    quitterChangementTypeTravail(){
        this.nombre++;
        if(this.nombre>1){
            this.changerTypeTravail = false;
            this.nombre=0;
        }
    }


    enregistrer(){
        let ttSelection = this.typeTravailsSelection.find(tt => tt.id == this.typeTravailSelected );
        let data = {
            "caracteristiques" : ttSelection.caracteristiques.map(carac => carac.id),
            "typeTravail" : ttSelection.typeTravail.id,
        };

        console.log(data);
        this.patchEnregistrement(this.enregistrement.data.id, data);
    }

    patchEnregistrement(id : number, data){
        this.enregistrementService
            .patchEnregistrement(id,data)
            .subscribe(
                (rep) => {
                    this.sharedPeriodeTempsService.replacePeriodeTemps(rep);
                },
                (err) => {
                    console.log(err);
                    let message = JSON.parse(err._body);
                    this.alertService.JSONError(message);
                }
            );
    }

    onChangeSujet(idSujet){
        this.sujetSelected = idSujet;
        this.sujetService
            .getTypeTravails(idSujet)
            .subscribe(
                (data) => {
                    console.log(data);
                    this.typeTravailsSelection = data;
                    if(data.length){
                        this.onChangeTypeTravail(data[0].id);
                    }
                },
                (err) => console.log(err)
            );
    }

    onChangeTypeTravail(idTypeTravail){
        this.typeTravailSelected= idTypeTravail;
    }

    /* Fin Partie consacrée au changement de type de travail */
}
