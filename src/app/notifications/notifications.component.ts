import {Component, Input, OnInit} from '@angular/core';
import {NotificationService} from "../_services/notification.service";
import {CurrentUserService} from "../_services/currentUser.service";
import * as moment from 'moment';
import {SharedNotificationService} from "../_services/shared.notification.service";

@Component({
    selector: 'notification',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

    show: boolean = false;

    constructor(
        private notificationService : NotificationService,
        private sharedNotificationService : SharedNotificationService
    ) { }


    ngOnInit() {}

    markNotificationAsRead(notifId : number){
        let data = {
            isActive : false,
            viewAt : (moment()).toISOString()
        };

        this.notificationService.patchNotification(notifId, data).subscribe(
            (rep) => {
                this.sharedNotificationService.notifications[this.sharedNotificationService.notifications.findIndex(el => el.id === rep.id)] = rep;
            },
            (err) => console.log(err)
        );
    }

}
