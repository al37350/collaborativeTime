const Moment = require('moment');
const MomentRange = require('moment-range');

const moment = MomentRange.extendMoment(Moment);

export class Month {

    private start;
    private end;

    constructor(public month, public year){
        this.month = month;
        this.year = year;
        this.start =  moment([year, month]);
        this.end = this.start.clone().endOf('month');
    }

    getWeekStart(){
        return this.start.weekday();
    }

    getDays(){
        const range = moment.range(this.start, this.end);
        return Array.from(range.by('days'));
    }

    getFormatted(){
        return this.start.format('MMMM YYYY');
    }
}