import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Month} from "./models/month";
import * as moment from 'moment';

@Component({
  selector: 'datepicker-agenda',
  templateUrl: './datepicker-agenda.component.html',
  styleUrls: ['./datepicker-agenda.component.scss']
})
export class DatepickerAgendaComponent implements OnInit {
  @Input() private date;
  @Output()
  change: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  hideDatePicker: EventEmitter<number> = new EventEmitter<number>();

  private month;
  private dayofMonth;

  get year() : string { return this.date.format('YYYY')}
  get date_formatted() : string {
    return this.date.format('dddd DD MMM')
  }

  get days() { return ['L', 'M', 'M', 'J', 'V', 'S', 'D'] }

  constructor() {
  }

  ngOnInit() {
    this.month = new Month(this.date.month(), this.date.year());
    this.chargeMonthDays();
  }

  isSelected(day){
    return this.date.unix() === day.unix();
  }

  selectDate(day){
    this.date=day;
    this.change.emit(this.date);
  }

  nextMonth(){
    let month = this.month.month +1;
    let year = this.month.year;

    if(month > 11){
      year+= 1;
      month = 0;
    }
    this.month = new Month(month, year);
    this.chargeMonthDays();
  }

  prevMonth(){
    let month = this.month.month - 1;
    let year = this.month.year;

    if(month < 0){
      year-= 1;
      month = 11;
    }
    this.month = new Month(month, year);
    this.chargeMonthDays();
  }

  chargeMonthDays(){
    this.dayofMonth = this.month.getDays();
  }
}
