import {Component, OnInit, Input, style, animate, transition, trigger, Output, EventEmitter} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  animations: [
    trigger(
        'enterAnimation', [
          transition(':enter', [
            style({transform: 'translateX(100%)', opacity: 0}),
            animate('700ms', style({transform: 'translateX(0)', opacity: 1,}))
          ]),
          transition(':leave', [
            style({transform: 'translateX(0)', opacity: 1}),
            animate('700ms', style({transform: 'translateX(100%)', opacity: 0}))
          ])
        ]
    )
  ]
})

export class DatepickerComponent implements OnInit {
  private _value: string;
  private momentDate;

  @Input() private format : string = 'YYYY-MM-DD';
  @Input() private name : string ;

  @Input()
  set value(value: string) {
    this._value = value;
  }

  @Output()
  change: EventEmitter<number> = new EventEmitter<number>();

  get date() { return this.momentDate }

  get dateFormatted() : string { return this.date.format(this.format)}
  get dateRaw() : string { return this.date.format('YYYY-MM-DD')}

  constructor() {
    moment.locale('fr');
  }

  ngOnInit() {
    this.momentDate = moment(this._value, this.format);
    this.emitChange();
  }


  dateChange(event){
    this.momentDate=event;
    this.emitChange();
  }

  emitChange(){
    this.change.emit(this.date);
  }
}
