import { Component, OnInit } from '@angular/core';
import { AlertService } from '../_services/alert.service';

@Component({
  selector: 'alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  messages: any [] = [];

  constructor(private alertService: AlertService) { }

  ngOnInit() {
    this.alertService.getMessage().subscribe(
        message => {
          if(typeof message != 'undefined'){
            message.show = "yes";
            this.messages.push(message);
            this.lunchHideAuto(message);
          }
        }
    );
  }

  lunchHideAuto(message){
    setTimeout(() => {
      message.show = "notReally";
      setTimeout(() => message.show = "no", 5000);
    }, 10000);
  }

}
