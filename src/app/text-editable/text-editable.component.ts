import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'text-editable',
    templateUrl: './text-editable.component.html',
    styleUrls: ['./text-editable.component.scss']
})
export class TextEditableComponent implements OnInit {
    active : boolean = false;
    nombre: number = 0;

    @Input() value : string= "";
    @Input() placeholder : string= "";
    @Input() textStyle = {
        display: 'inline-block',
        cursor: 'pointer',
        margin: 0,
        padding: 0,
    };


    @Output() valueChange: EventEmitter<any> = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    quitterInput(){
        this.nombre++;
        if(this.nombre>1){
            this.active = false;
            this.nombre=0;
            this.valueChange.emit(this.value);
        }
    }
}
