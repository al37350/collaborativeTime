import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextEditableComponent } from './text-editable.component';

describe('TextEditableComponent', () => {
  let component: TextEditableComponent;
  let fixture: ComponentFixture<TextEditableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextEditableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextEditableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
