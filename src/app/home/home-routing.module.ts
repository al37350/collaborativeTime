import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from "../dashboard/dashboard.component";
import {ProjectsComponent} from "../projects/projects.component";
import {ReportComponent} from "../report/report.component";
import {OptionComponent} from "../option/option.component";
import {HomeComponent} from "./home.component";
import {CanActivateAuthGuard} from "../_guards/can-activate-auth.guard";
import {ProjectsOverviewComponent} from "../projects/projects-overview/projects-overview.component";
import {ProjectsEditComponent} from "../projects/projects-edit/projects-edit.component";
import {ProjectsEditProjectComponent} from "../projects/projects-edit/projects-edit-project/projects-edit-project.component";
import {ProjectsCreateComponent} from "../projects/projects-create/projects-create.component";
import {ProjectsCreateProjectInfoComponent} from "../projects/projects-create/projects-create-project-info/projects-create-project-info.component";
import {ProjectsEditProjectInfoComponent} from "../projects/projects-edit/projects-edit-project-info/projects-edit-project-info.component";
import {ProjectsEditTypeTravailComponent} from "../projects/projects-edit/projects-edit-type-travail/projects-edit-type-travail.component";
import {AdminComponent} from "../admin/admin.component";
import {AdminOverviewComponent} from "../admin/admin-overview/admin-overview.component";
import {AdminEditUtilisateurComponent} from "../admin/admin-edit/admin-edit-utilisateur/admin-edit-utilisateur.component";
import {AdminEditGroupeComponent} from "../admin/admin-edit/admin-edit-groupe/admin-edit-groupe.component";
import {AdminCreateComponent} from "../admin/admin-create/admin-create.component";
import {AdminCreateUtilisateurComponent} from "../admin/admin-create/admin-create-utilisateur/admin-create-utilisateur.component";
import {AdminUtilisateursOverviewComponent} from "../admin/admin-overview/admin-utilisateurs-overview/admin-utilisateurs-overview.component";
import {AdminGroupesOverviewComponent} from "../admin/admin-overview/admin-groupes-overview/admin-groupes-overview.component";
import {AdminEditUtilisateurInfoComponent} from "../admin/admin-edit/admin-edit-utilisateur/admin-edit-utilisateur-info/admin-edit-utilisateur-info.component";
import {OptionEditPasswordComponent} from "../option/option-edit-password/option-edit-password.component";
import {AdminUtilisateursListOverviewComponent} from "../admin/admin-overview/admin-utilisateurs-overview/admin-utilisateurs-list-overview/admin-utilisateurs-list-overview.component";
import {AdminGroupesListOverviewComponent} from "../admin/admin-overview/admin-groupes-overview/admin-groupes-list-overview/admin-groupes-list-overview.component";
import {ProjectsProprietaireSujetOverviewComponent} from "../projects/projects-overview/projects-proprietaire-sujet-overview/projects-proprietaire-sujet-overview.component";
import {ProjectsAppartenanceSujetOverviewComponent} from "../projects/projects-overview/projects-appartenance-sujet-overview/projects-appartenance-sujet-overview.component";
import {AdminCaracteristiquesOverviewComponent} from "../admin/admin-overview/admin-caracteristiques-overview/admin-caracteristiques-overview.component";
import {OptionEditConfigurationComponent} from "../option/option-edit-configuration/option-edit-configuration.component";
import {OptionEditTagPersonnelComponent} from "../option/option-tag-personnel-overview/option-edit-tag-personnel/option-edit-tag-personnel.component";
import {AdminModifsOverviewComponent} from "../admin/admin-overview/admin-modifs-overview/admin-modifs-overview.component";
import {DashbordEditJourInstanceComponent} from "../dashboard/dashbord-edit-jour-instance/dashbord-edit-jour-instance.component";
import {AdminFamillesOverviewComponent} from "../admin/admin-overview/admin-familles-overview/admin-familles-overview.component";
import {OptionImportComponent} from "../option/option-import/option-import.component";
import {AdminTypeTravailsOverviewComponent} from "../admin/admin-overview/admin-type-travails-overview/admin-type-travails-overview.component";
import {ReportPersonnalComponent} from "../report/report-personnal/report-personnal.component";
import {ReportSuperviseurComponent} from "../report/report-superviseur/report-superviseur.component";
import {AdminTypeTravailsListOverviewComponentComponent} from "../admin/admin-overview/admin-type-travails-overview/admin-type-travails-list-overview-component/admin-type-travails-list-overview-component.component";
import {ProjectsEditCaracComponent} from "../projects/projects-edit/projects-edit-carac/projects-edit-carac.component";
import {ReportSuperviseurCommunComponent} from "../report/report-superviseur/report-superviseur-commun/report-superviseur-commun.component";
import {ReportSuperviseurSpecifiqueComponent} from "../report/report-superviseur/report-superviseur-specifique/report-superviseur-specifique.component";
import {AdminCaracteristiquesListOverviewComponent} from "../admin/admin-overview/admin-caracteristiques-overview/admin-caracteristiques-list-overview/admin-caracteristiques-list-overview.component";
import {AdminCaracteristiquesEditOverviewComponent} from "../admin/admin-overview/admin-caracteristiques-overview/admin-caracteristiques-edit-overview/admin-caracteristiques-edit-overview.component";
import {AdminMotifsListOverviewComponent} from "../admin/admin-overview/admin-modifs-overview/admin-motifs-list-overview/admin-motifs-list-overview.component";
import {AdminMotifsEditOverviewComponent} from "../admin/admin-overview/admin-modifs-overview/admin-motifs-edit-overview/admin-motifs-edit-overview.component";
import {AdminFamillesListOverviewComponent} from "../admin/admin-overview/admin-familles-overview/admin-familles-list-overview/admin-familles-list-overview.component";
import {AdminFamillesEditOverviewComponent} from "../admin/admin-overview/admin-familles-overview/admin-familles-edit-overview/admin-familles-edit-overview.component";
import {OptionTagPersonnelOverviewComponent} from "../option/option-tag-personnel-overview/option-tag-personnel-overview.component";
import {OptionListTagPersonnelComponent} from "../option/option-tag-personnel-overview/option-list-tag-personnel/option-list-tag-personnel.component";
import {NotificationsComponent} from "../notifications/notifications.component";

const homeRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [CanActivateAuthGuard],
        //Outlet dans homeComponent
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', component: DashboardComponent},
            { path: 'notifications', component: NotificationsComponent},

            { path: 'projects', component: ProjectsComponent,
                //Outlet dans ProjectsComponent
                children: [
                    { path: '', redirectTo: 'overview', pathMatch: 'full' },
                    { path: 'overview', component: ProjectsOverviewComponent,
                        //outlet dans ProjectsOverviewComponent
                        children: [
                            { path: '', redirectTo: 'createur', pathMatch: 'full' },
                            { path: 'createur', component: ProjectsProprietaireSujetOverviewComponent },
                            { path: 'appartenance', component: ProjectsAppartenanceSujetOverviewComponent },
                        ]
                    },
                    { path: 'edit', component: ProjectsEditComponent,
                        //outlet dans ProjectEditComponent
                        children: [
                            { path: '', redirectTo: 'project', pathMatch: 'full' },
                            { path: 'project/:id', component: ProjectsEditProjectComponent },
                            { path: 'projectinfos/:id', component: ProjectsEditProjectInfoComponent },
                            { path: 'typetravailpossible/:id', component: ProjectsEditCaracComponent },
                            { path: 'jourinstance', component: DashbordEditJourInstanceComponent }
                        ]
                    },
                    { path: 'create', component: ProjectsCreateComponent,
                        //outlet dans ProjectsCreateComponent
                        children: [
                            { path: 'project', component: ProjectsCreateProjectInfoComponent }
                        ]
                    },
                ]
            },
            { path: 'admin', component: AdminComponent ,
                //Outlet dans AdminComponent
                children: [
                    { path: '', redirectTo: 'overview', pathMatch: 'full' },
                    { path: 'overview', component: AdminOverviewComponent,
                        //outlet dans AdminOverviewComponent
                        children: [
                            { path: '', redirectTo: 'utilisateur', pathMatch: 'full' },
                            { path: 'utilisateur', component: AdminUtilisateursOverviewComponent,
                                //outlet dans AdminUtilisateursOverviewComponent
                                children: [
                                    { path: '', redirectTo: 'list', pathMatch: 'full' },
                                    { path: 'list', component: AdminUtilisateursListOverviewComponent },
                                    { path: 'new', component: AdminCreateUtilisateurComponent },
                                    { path: 'edit/:id', component: AdminEditUtilisateurComponent },
                                ]
                            },
                            { path: 'groupe', component: AdminGroupesOverviewComponent,
                                //outlet dans AdminGroupesOverviewComponent
                                children: [
                                    { path: '', redirectTo: 'list', pathMatch: 'full' },
                                    { path: 'list', component: AdminGroupesListOverviewComponent },
                                    { path: 'edit/:id', component: AdminEditGroupeComponent },
                                ]
                            },
                            { path: 'caracteristique', component: AdminCaracteristiquesOverviewComponent,
                                //outlet dans AdminCaracteristiquesOverviewComponent
                                children: [
                                    { path: '', redirectTo: 'list', pathMatch: 'full' },
                                    { path: 'list', component: AdminCaracteristiquesListOverviewComponent },
                                    { path: 'edit/:id', component: AdminCaracteristiquesEditOverviewComponent },
                                ]
                            },
                            { path: 'motif', component: AdminModifsOverviewComponent,
                                //outlet dans AdminModifsOverviewComponent
                                children: [
                                    { path: '', redirectTo: 'list', pathMatch: 'full' },
                                    { path: 'list', component: AdminMotifsListOverviewComponent },
                                    { path: 'edit/:id', component: AdminMotifsEditOverviewComponent },
                                ]
                            },
                            { path: 'famille', component: AdminFamillesOverviewComponent,
                                //outlet dans AdminFamillesOverviewComponent
                                children: [
                                    { path: '', redirectTo: 'list', pathMatch: 'full' },
                                    { path: 'list', component: AdminFamillesListOverviewComponent },
                                    { path: 'edit/:id', component: AdminFamillesEditOverviewComponent },
                                ]
                            },
                            { path: 'typetravail', component: AdminTypeTravailsOverviewComponent,
                                //outlet dans AdminTypeTravailsOverviewComponent
                                children: [
                                    { path: '', redirectTo: 'list', pathMatch: 'full' },
                                    { path: 'list', component: AdminTypeTravailsListOverviewComponentComponent },
                                    { path: 'edit/:id', component: ProjectsEditTypeTravailComponent },
                                ]
                            },
                        ]
                    },
                    { path: 'create', component: AdminCreateComponent,
                        //outlet dans AdminCreate
                        children: [
                            { path: '', redirectTo: 'utilisateur', pathMatch: 'full' },
                            { path: 'utilisateur', component: AdminCreateUtilisateurComponent },
                        ]
                    }
                ]
            },
            { path: 'reports', component: ReportComponent,
                //outlet dans ReportComponent
                children: [
                    { path: '', redirectTo: 'superviseur', pathMatch: 'full' },
                    { path: 'superviseur', component: ReportSuperviseurComponent,
                        //outlet dans ReportSuperviseurComponent
                        children: [
                            { path: '', redirectTo: 'commun', pathMatch: 'full' },
                            { path: 'commun', component: ReportSuperviseurCommunComponent },
                            { path: 'specifique', component: ReportSuperviseurSpecifiqueComponent },
                        ]},
                    { path: 'personnel', component: ReportPersonnalComponent },
                ]
            },
            { path: 'options', component: OptionComponent,
                //outlet dans OptionComponent
                children: [
                    { path: '', redirectTo: 'editconfiguration', pathMatch: 'full' },
                    { path: 'editutilisateurinfo', component: AdminEditUtilisateurInfoComponent },
                    { path: 'editpassword', component: OptionEditPasswordComponent },
                    { path: 'editconfiguration', component: OptionEditConfigurationComponent },
                    { path: 'tagperso', component: OptionTagPersonnelOverviewComponent,
                        //outlet dans OptionTagPersonnelOverviewComponent
                        children: [
                            { path: '', redirectTo: 'list', pathMatch: 'full' },
                            { path: 'list', component: OptionListTagPersonnelComponent },
                            { path: 'edit/:id', component: OptionEditTagPersonnelComponent },
                        ]
                    },
                    { path: 'import', component: OptionImportComponent },
                ]
            }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(homeRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class HomeRoutingModule { }