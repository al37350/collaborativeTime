import {Component, OnInit, Input} from '@angular/core';
import {Table} from "../../_models/HTMLTable/Table";
import {Cell} from "../../_models/HTMLTable/Cell";
import * as moment from 'moment';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
    @Input()
    private table : Table;

    @Input()
    private showTotal : boolean = false;

    @Input()
    private format : string = 'HHhMM';

    @Input()
    private reverseDisplay : boolean = false;

    constructor() {}

    ngOnInit() {}

    downloadCSV(){
        let blob = new Blob([this.table.toCSV()], { type: 'text/csv' });
        let url= window.URL.createObjectURL(blob);
        window.open(url);
    }

    formatTime(cell : Cell, format: string){
        if(cell.toFormat){
            let hours   = Math.floor(cell.value / 3600);
            let minutes = Math.floor((cell.value - (hours * 3600)) / 60);
            let seconds = cell.value - (hours * 3600) - (minutes * 60);

            let res = "";
            res = format.replace("HH", hours.toString().length >1 ? hours.toString() : "0"+hours.toString());
            res = res.replace("MM", minutes.toString().length >1 ? minutes.toString() : "0"+minutes.toString());
            res = res.replace("SS", seconds.toString().length >1 ? seconds.toString() : "0"+seconds.toString());

            return res;
        }
        return cell.value;
    }

}
