import { Component, OnInit } from '@angular/core';
import {PeriodeAnalyseShared} from "../../_services/stat/periodeAnalyseShared.service";
import {CurrentUserService} from "../../_services/currentUser.service";

@Component({
    selector: 'report-personnal',
    templateUrl: './report-personnal.component.html',
    styleUrls: ['./report-personnal.component.scss']
})
export class ReportPersonnalComponent implements OnInit {

    private jourDebut;
    private jourFin;

    private utilisateur;
    constructor(
        private periodeAnalyseShared : PeriodeAnalyseShared,
        private currentUserService : CurrentUserService
    ){}

    ngOnInit() {
        this.periodeAnalyseShared.debutEmitter.subscribe((moment) => this.jourDebut = moment);
        this.periodeAnalyseShared.finEmitter.subscribe((moment) => this.jourFin = moment);
        this.utilisateur = this.currentUserService.getUser();
    }

    public changeDebut(event){
        this.jourDebut = event;
        this.periodeAnalyseShared.debut = event;
    }

    public changeFin(event){
        this.jourFin =event;
        this.periodeAnalyseShared.fin = event;
    }
}
