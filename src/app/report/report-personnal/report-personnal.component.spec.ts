import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportPersonnalComponent } from './report-personnal.component';

describe('ReportPersonnalComponent', () => {
  let component: ReportPersonnalComponent;
  let fixture: ComponentFixture<ReportPersonnalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportPersonnalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportPersonnalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
