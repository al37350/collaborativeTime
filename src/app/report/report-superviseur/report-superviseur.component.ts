import { Component, OnInit } from '@angular/core';
import {PeriodeAnalyseShared} from "../../_services/stat/periodeAnalyseShared.service";

@Component({
    selector: 'report-superviseur',
    templateUrl: './report-superviseur.component.html',
    styleUrls: ['./report-superviseur.component.scss']
})
export class ReportSuperviseurComponent implements OnInit {

    private jourDebut;
    private jourFin;

    private links;

    constructor(
        private periodeAnalyseShared : PeriodeAnalyseShared
    ) {
        this.links = [
            {
                path : '/reports/superviseur/commun',
                name : 'Commun'
            },
            {
                path : '/reports/superviseur/specifique',
                name : 'Spécifique'
            }
        ];
    }

    ngOnInit() {
        this.periodeAnalyseShared.debutEmitter.subscribe((moment) => this.jourDebut = moment);
        this.periodeAnalyseShared.finEmitter.subscribe((moment) => this.jourFin = moment);
    }

    public changeDebut(event){
        this.periodeAnalyseShared.debut = event;
    }

    public changeFin(event){
        this.periodeAnalyseShared.fin = event;
    }
}
