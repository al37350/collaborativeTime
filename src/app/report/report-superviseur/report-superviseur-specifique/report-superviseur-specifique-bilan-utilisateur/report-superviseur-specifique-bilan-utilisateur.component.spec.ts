import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSuperviseurSpecifiqueBilanUtilisateurComponent } from './report-superviseur-specifique-bilan-utilisateur.component';

describe('ReportSuperviseurSpecifiqueBilanUtilisateurComponent', () => {
  let component: ReportSuperviseurSpecifiqueBilanUtilisateurComponent;
  let fixture: ComponentFixture<ReportSuperviseurSpecifiqueBilanUtilisateurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSuperviseurSpecifiqueBilanUtilisateurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSuperviseurSpecifiqueBilanUtilisateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
