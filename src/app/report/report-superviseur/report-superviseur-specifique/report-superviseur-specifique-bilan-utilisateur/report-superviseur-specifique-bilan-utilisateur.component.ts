import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {Table} from "../../../../_models/HTMLTable/Table";
import {Row} from "../../../../_models/HTMLTable/Row";
import {Cell} from "../../../../_models/HTMLTable/Cell";
import {PeriodeAnalyseShared} from "../../../../_services/stat/periodeAnalyseShared.service";
import {RepartitionService} from "../../../../_services/stat/repartition.service";
import {CurrentUserService} from "../../../../_services/currentUser.service";

@Component({
    selector: 'report-superviseur-specifique-bilan-utilisateur',
    templateUrl: './report-superviseur-specifique-bilan-utilisateur.component.html',
    styleUrls: ['./report-superviseur-specifique-bilan-utilisateur.component.scss']
})
export class ReportSuperviseurSpecifiqueBilanUtilisateurComponent implements OnInit, OnChanges {

    private jourDebut;
    private jourFin;

    @Input()
    private utilisateur;

    private table : Table = new Table();

    constructor(
        private periodeAnalyseShared : PeriodeAnalyseShared,
        private repartitionService : RepartitionService
    ){}

    ngOnInit() {
        this.periodeAnalyseShared.debutEmitter.subscribe((moment) => {
            this.jourDebut = moment;
            this.periodeAnalyseShared.finEmitter.subscribe((moment) => {
                this.jourFin = moment;
                this.loadRapport();
            });
        });
    }

    ngOnChanges(changes: any): void {
        if(this.jourDebut && this.jourFin){
            this.loadRapport();
        }
    }

    loadRapport(){
        this.repartitionService.getRapportUtilisateur(
            this.utilisateur.id,
            this.jourDebut.format('YYYY-MM-DD'),
            this.jourFin.format('YYYY-MM-DD')
        ).subscribe(
            (data) => {
                this.table = new Table();
                this.table.addHeader("Temps");
                this.table.addHeader("%");

                let totalTime = data
                    .map(famille => famille.sujets)
                    .map(sujetsFamille => sujetsFamille
                        .map(sujet => parseInt(sujet.data))
                        .reduce((prev, elem) => prev +elem, 0)
                    )
                    .reduce((prev, elem) => prev +elem, 0);

                for(let famille of data){
                    let row = new Row(famille.name.toUpperCase());
                    row.isImportant = true;
                    let totalTimeFamille = famille.sujets.map(sujet => parseInt(sujet.data)).reduce((prev, elem) => prev +elem, 0);

                    row.addCell(new Cell(totalTimeFamille, true));
                    row.addCell(new Cell(totalTimeFamille ? Math.round(parseInt(totalTimeFamille)/totalTime*100) : 0));

                    this.table.addRow(row);
                    for(let sujet of famille.sujets){
                        if(parseInt(sujet.data)){
                            let row = new Row(sujet.name);
                            row.addCell(new Cell(parseInt(sujet.data), true));
                            row.addCell(new Cell(totalTimeFamille ? Math.round(parseInt(sujet.data)/totalTimeFamille*100) : 0));
                            this.table.addRow(row);
                        }
                    }
                }
                let row = new Row("Total de temps");
                row.addCell(new Cell(totalTime, true));
                this.table.addRow(row);
            }
        );

    }

}
