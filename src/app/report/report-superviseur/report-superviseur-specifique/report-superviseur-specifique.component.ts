import { Component, OnInit } from '@angular/core';
import {PeriodeAnalyseShared} from "../../../_services/stat/periodeAnalyseShared.service";
import {CurrentUserService} from "../../../_services/currentUser.service";
import {UtilisateurService} from "../../../_services/utilisateur.service";

@Component({
    selector: 'app-report-superviseur-specifique',
    templateUrl: './report-superviseur-specifique.component.html',
    styleUrls: ['./report-superviseur-specifique.component.scss']
})
export class ReportSuperviseurSpecifiqueComponent implements OnInit {

    private jourDebut;
    private jourFin;


    private familleSelected : Object;

    private utilisateurs;

    constructor(
        private periodeAnalyseShared : PeriodeAnalyseShared,
        private utilisateurService : UtilisateurService
    ) { }

    ngOnInit() {
        this.periodeAnalyseShared.debutEmitter.subscribe((moment) => this.jourDebut = moment);
        this.periodeAnalyseShared.finEmitter.subscribe((moment) => this.jourFin = moment);
        this.utilisateurService.getUtilisateurs().subscribe((rep) => this.utilisateurs = rep);
    }

    familleChange(famille){
        this.familleSelected = famille;
    }

}
