import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSuperviseurSpecifiqueComponent } from './report-superviseur-specifique.component';

describe('ReportSuperviseurSpecifiqueComponent', () => {
  let component: ReportSuperviseurSpecifiqueComponent;
  let fixture: ComponentFixture<ReportSuperviseurSpecifiqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSuperviseurSpecifiqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSuperviseurSpecifiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
