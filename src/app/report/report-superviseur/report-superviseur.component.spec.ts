import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSuperviseurComponent } from './report-superviseur.component';

describe('ReportSuperviseurComponent', () => {
  let component: ReportSuperviseurComponent;
  let fixture: ComponentFixture<ReportSuperviseurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSuperviseurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSuperviseurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
