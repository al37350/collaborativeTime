import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSuperviseurCommunComponent } from './report-superviseur-commun.component';

describe('ReportSuperviseurCommunComponent', () => {
  let component: ReportSuperviseurCommunComponent;
  let fixture: ComponentFixture<ReportSuperviseurCommunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSuperviseurCommunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSuperviseurCommunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
