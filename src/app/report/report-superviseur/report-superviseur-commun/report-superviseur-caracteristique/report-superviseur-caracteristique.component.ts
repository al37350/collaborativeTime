import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {Table} from "../../../../_models/HTMLTable/Table";
import {Row} from "../../../../_models/HTMLTable/Row";
import {Cell} from "../../../../_models/HTMLTable/Cell";
import {CaracteristiqueService} from "../../../../_services/caracteristique.service";
import {RepartitionService} from "../../../../_services/stat/repartition.service";

@Component({
    selector: 'report-superviseur-caracteristique',
    templateUrl: 'report-superviseur-caracteristique.component.html',
    styleUrls: ['report-superviseur-caracteristique.component.scss']
})
export class ReportSuperviseurCaracteristiqueComponent implements OnInit, OnChanges {
    @Input() debut;
    @Input() fin;

    private data;
    private table = new Table();

    private caracteristiques;
    private selectedCaracteristique;

    constructor(
        private caracteristiqueService : CaracteristiqueService,
        private repartionService : RepartitionService
    ) {}

    ngOnInit() {

        this.caracteristiqueService
            .getCaracteristiques()
            .subscribe(
                (data) => {
                    this.caracteristiques = data;
                    if(this.caracteristiques.length){
                        this.selectedCaracteristique = this.caracteristiques[0];
                        this.onChangeCaracteristique();
                    }
                },
                (err) => console.log(err)
            );
    }

    ngOnChanges(changes: any){
        if(changes.debut){
            if(!changes.debut.firstChange){
                this.onChangeCaracteristique();
            }
        }
        if(changes.fin){
            if(!changes.fin.firstChange){
                this.onChangeCaracteristique();
            }
        }
    }

    onChangeCaracteristique(){
        this.repartionService.getCaracteristiqueStats(
            this.selectedCaracteristique.id,
            this.debut.format('YYYY-MM-DD'),
            this.fin.format('YYYY-MM-DD')
        ).subscribe(
            (rep) => {
                this.table = new Table();
                this.table.headers = rep.names;
                for (let value of rep.values) {
                    let row = new Row(value.name);
                    for (let cell of value.datas) {
                        row.addCell(new Cell(parseInt(cell), true))
                    }
                    this.table.addRow(row);
                }
            }
        );
    }
}
