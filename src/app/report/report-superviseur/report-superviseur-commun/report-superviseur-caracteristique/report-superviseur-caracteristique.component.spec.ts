import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSuperviseurCaracteristiqueComponent } from './report-superviseur-caracteristique.component';

describe('ReportSuperviseurCaracteristiqueComponent', () => {
  let component: ReportSuperviseurCaracteristiqueComponent;
  let fixture: ComponentFixture<ReportSuperviseurCaracteristiqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSuperviseurCaracteristiqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSuperviseurCaracteristiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
