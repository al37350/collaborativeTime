import { Component, OnInit } from '@angular/core';
import {PeriodeAnalyseShared} from "../../../_services/stat/periodeAnalyseShared.service";

@Component({
    selector: 'app-report-superviseur-commun',
    templateUrl: './report-superviseur-commun.component.html',
    styleUrls: ['./report-superviseur-commun.component.scss']
})
export class ReportSuperviseurCommunComponent implements OnInit {

    private jourDebut;
    private jourFin;

    constructor(
        private periodeAnalyseShared : PeriodeAnalyseShared
    ) { }

    ngOnInit() {
        this.periodeAnalyseShared.debutEmitter.subscribe((moment) => this.jourDebut = moment);
        this.periodeAnalyseShared.finEmitter.subscribe((moment) => this.jourFin = moment);
    }

}
