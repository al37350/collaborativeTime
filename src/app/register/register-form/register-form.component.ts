import {Component, OnInit, Input} from '@angular/core';
import {Router} from "@angular/router";
import {AlertService} from "../../_services/alert.service";
import {UtilisateurService} from "../../_services/utilisateur.service";
import * as moment from 'moment';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {
  @Input() redirection = '/login';
  model: any = {};
  loading = false;
  private jour;

  constructor(
      private router: Router,
      private alertService: AlertService,
      private utilisateurService: UtilisateurService) { }

  ngOnInit() {
    this.jour = moment().format('L');
  }

  register() {
    this.loading = true;
    this.utilisateurService.create(this.model)
        .subscribe(
            data => {
              this.alertService.success('Registration successful', true);
              this.router.navigate([this.redirection]);
            },
            error => {
              console.log(error);
              let message = JSON.parse(error._body);
              this.alertService.JSONError(message);
              this.loading = false;
            });
  }

  dateChange(event){
    this.model.miseEnService = event.format('YYYY-MM-DD');
  }
}
