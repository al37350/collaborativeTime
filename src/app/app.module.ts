import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ContenuComponent } from './contenu/contenu.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AlertComponent } from './_directives/alert.component';
import { AlertService } from "./_services/alert.service";
import { AuthenticationService } from "./_services/authentification.service";
import { AppRoutingModule } from "./app-routing.module";
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProjectsComponent } from './projects/projects.component';
import { HomeRoutingModule } from "./home/home-routing.module";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ReportComponent } from './report/report.component';
import { OptionComponent } from './option/option.component';
import { GroupeService } from "./_services/groupe.service";
import { UtilisateurService } from "./_services/utilisateur.service";
import { LienComponent } from "./navigation/lien/lien.component";
import { HttpClient } from "./_services/HttpClient";
import { CanActivateAuthGuard } from "./_guards/can-activate-auth.guard";
import { ProjectsOverviewComponent } from './projects/projects-overview/projects-overview.component';
import { ProjectsProprietaireSujetOverviewComponent } from './projects/projects-overview/projects-proprietaire-sujet-overview/projects-proprietaire-sujet-overview.component';
import { ProjectsAppartenanceSujetOverviewComponent } from './projects/projects-overview/projects-appartenance-sujet-overview/projects-appartenance-sujet-overview.component';
import { ProjectsEditComponent } from './projects/projects-edit/projects-edit.component';
import { ProjectsEditProjectComponent } from './projects/projects-edit/projects-edit-project/projects-edit-project.component';
import { SujetService } from "./_services/sujet.service";
import { TypeTravailService } from "./_services/type-travail.service";
import { ProjectsCreateComponent } from './projects/projects-create/projects-create.component';
import { ProjectsCreateProjectInfoComponent } from "./projects/projects-create/projects-create-project-info/projects-create-project-info.component";
import { ProjectsEditProjectInfoComponent } from './projects/projects-edit/projects-edit-project-info/projects-edit-project-info.component';
import { ProjectsEditTypeTravailComponent } from './projects/projects-edit/projects-edit-type-travail/projects-edit-type-travail.component';
import { CaracteristiqueService } from "./_services/caracteristique.service";
import { DroitService } from "./_services/droit.service";
import { ProjectsCreateEnregistrementComponent } from './projects/projects-create/projects-create-enregistrement/projects-create-enregistrement.component';
import { EnregistrementService } from "./_services/enregistrement.service";
import { AdminComponent } from "./admin/admin.component";
import { AdminGroupesOverviewComponent } from "./admin/admin-overview/admin-groupes-overview/admin-groupes-overview.component";
import { AdminUtilisateursOverviewComponent } from "./admin/admin-overview/admin-utilisateurs-overview/admin-utilisateurs-overview.component";
import { AdminOverviewComponent } from "./admin/admin-overview/admin-overview.component";
import { AdminEditComponent } from "./admin/admin-edit/admin-edit.component";
import { AdminEditUtilisateurComponent } from "./admin/admin-edit/admin-edit-utilisateur/admin-edit-utilisateur.component";
import { AdminCreateComponent } from "./admin/admin-create/admin-create.component";
import { AdminCreateUtilisateurComponent } from "./admin/admin-create/admin-create-utilisateur/admin-create-utilisateur.component";
import { AdminEditGroupeComponent } from "./admin/admin-edit/admin-edit-groupe/admin-edit-groupe.component";
import { AdminEditUtilisateurInfoComponent } from "./admin/admin-edit/admin-edit-utilisateur/admin-edit-utilisateur-info/admin-edit-utilisateur-info.component";
import { AdminEditUtilisateurGroupeComponent } from './admin/admin-edit/admin-edit-utilisateur/admin-edit-utilisateur-groupe/admin-edit-utilisateur-groupe.component';
import { NavbarComponent } from './navigation/navbar/navbar.component';
import { OptionEditPasswordComponent } from "./option/option-edit-password/option-edit-password.component";
import { AdminUtilisateursListOverviewComponent } from './admin/admin-overview/admin-utilisateurs-overview/admin-utilisateurs-list-overview/admin-utilisateurs-list-overview.component';
import { AdminGroupesListOverviewComponent } from './admin/admin-overview/admin-groupes-overview/admin-groupes-list-overview/admin-groupes-list-overview.component';
import { RegisterFormComponent } from './register/register-form/register-form.component';
import { AdminCaracteristiquesOverviewComponent } from './admin/admin-overview/admin-caracteristiques-overview/admin-caracteristiques-overview.component';
import { OptionEditConfigurationComponent } from './option/option-edit-configuration/option-edit-configuration.component';
import { ConfigurationService } from "./_services/configuration.service";
import { TagPersonnelService } from "./_services/tagPersonnel.service";
import { SharedService } from "./_services/shared.service";
import { CurrentUserService } from "./_services/currentUser.service";
import { DashboardPieEnregistrementComponent } from './dashboard/dashboard-pie-enregistrement/dashboard-pie-enregistrement.component';
import { ClickOutsideDirective } from "angular2-click-outside/clickOutside.directive";
import { DateSelectedPickerService } from "./_services/dateSelectedPickerService.service";
import { TimepickerComponent } from './timepicker/timepicker.component';
import { TimepickerHourComponent } from './timepicker/timepicker-hour/timepicker-hour.component';
import { NavbarEmitterComponent } from './navigation/navbar-emitter/navbar-emitter.component';
import { JourInstanceService } from "./_services/jourInstance.service";
import { AdminModifsOverviewComponent } from './admin/admin-overview/admin-modifs-overview/admin-modifs-overview.component';
import { MotifService } from "./_services/motif.service";
import { KeysPipe } from "./_pipes/keys.pipe";
import { DatepickerComponent } from "./datepicker/datepicker.component";
import { DatepickerAgendaComponent } from "./datepicker/datepicker-agenda/datepicker-agenda.component";
import { DashboardDatepickerComponent } from "./dashboard/dashboard-datepicker/dashboard-datepicker.component";
import { DashboardDatepickerAgendaComponent } from "./dashboard/dashboard-datepicker/dashboard-datepicker-agenda/dashboard-datepicker-agenda.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CalendarModule } from 'angular-calendar';
import { DashboardTimelineComponent } from './dashboard/dashboard-timeline/dashboard-timeline.component';
import { SecondsFormatPipe } from "./_pipes/secondsFormat";
import { DashbordEditJourInstanceComponent } from './dashboard/dashbord-edit-jour-instance/dashbord-edit-jour-instance.component';
import { PeriodeOffService } from "./_services/periodeOff.service";
import { PeriodeTempsService } from "./_services/periodeTemps.service";
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import {EnregistrementFroidStoreService} from "./_services/EnregistrementFroidStore.service";
import { ProjectsCreateEnregistrementQualifieComponent } from './projects/projects-create/projects-create-enregistrement/projects-create-enregistrement-qualifie/projects-create-enregistrement-qualifie.component';
import { EnregistrementQualificationComponentComponent } from './projects/projects-create/projects-create-enregistrement/enregistrement-qualification-component/enregistrement-qualification-component.component';
import { EnregistrementTimerComponentComponent } from './projects/projects-create/projects-create-enregistrement/enregistrement-timer-component/enregistrement-timer-component.component';
import { EnregistrementButtonsComponentComponent } from './projects/projects-create/projects-create-enregistrement/enregistrement-buttons-component/enregistrement-buttons-component.component';
import { ProjectsCreateEnregistrementQualifiableComponent } from './projects/projects-create/projects-create-enregistrement/projects-create-enregistrement-qualifiable/projects-create-enregistrement-qualifiable.component';
import { AdminFamillesOverviewComponent } from './admin/admin-overview/admin-familles-overview/admin-familles-overview.component';
import {FamilleService} from "./_services/famille.service";
import {RepartitionService} from "./_services/stat/repartition.service";

import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import { OptionImportComponent } from './option/option-import/option-import.component';
import { AdminTypeTravailsOverviewComponent } from './admin/admin-overview/admin-type-travails-overview/admin-type-travails-overview.component';
import {TypeTravailSelectionService} from "./_services/typeTravailSelection.service";
import { ReportPersonnalComponent } from './report/report-personnal/report-personnal.component';
import { DefaultChoiceChartComponent } from './chart/default-choice-chart/default-choice-chart.component';
import { FamilleRepartitionChartComponent } from './chart/famille-repartition-chart/famille-repartition-chart.component';
import { SujetRepartitionChartComponent } from './chart/sujet-repartition-chart/sujet-repartition-chart.component';
import { TypeTravailSujetRepartitionChartComponent } from './chart/type-travail-sujet-repartition-chart/type-travail-sujet-repartition-chart.component';
import {PeriodeAnalyseShared} from "./_services/stat/periodeAnalyseShared.service";
import { TagPersonnelRepartitionChartComponent } from './chart/tag-personnel-repartition-chart/tag-personnel-repartition-chart.component';
import { TagPersonnelSujetRepartitionChartComponent } from './chart/tag-personnel-sujet-repartition-chart/tag-personnel-sujet-repartition-chart.component';
import { ReportSuperviseurComponent } from './report/report-superviseur/report-superviseur.component';
import { TableComponent } from './report/table/table.component';
import { AdminTypeTravailsListOverviewComponentComponent } from './admin/admin-overview/admin-type-travails-overview/admin-type-travails-list-overview-component/admin-type-travails-list-overview-component.component';
import { ProjectsEditCaracComponent } from './projects/projects-edit/projects-edit-carac/projects-edit-carac.component';
import { ReportSuperviseurCaracteristiqueComponent } from './report/report-superviseur/report-superviseur-commun/report-superviseur-caracteristique/report-superviseur-caracteristique.component';
import { ReportSuperviseurCommunComponent } from './report/report-superviseur/report-superviseur-commun/report-superviseur-commun.component';
import { ReportSuperviseurSpecifiqueComponent } from './report/report-superviseur/report-superviseur-specifique/report-superviseur-specifique.component';
import { ReportSuperviseurSpecifiqueBilanUtilisateurComponent } from './report/report-superviseur/report-superviseur-specifique/report-superviseur-specifique-bilan-utilisateur/report-superviseur-specifique-bilan-utilisateur.component';
import { AdminCaracteristiquesListOverviewComponent } from './admin/admin-overview/admin-caracteristiques-overview/admin-caracteristiques-list-overview/admin-caracteristiques-list-overview.component';
import { AdminCaracteristiquesEditOverviewComponent } from './admin/admin-overview/admin-caracteristiques-overview/admin-caracteristiques-edit-overview/admin-caracteristiques-edit-overview.component';
import { AdminMotifsListOverviewComponent } from './admin/admin-overview/admin-modifs-overview/admin-motifs-list-overview/admin-motifs-list-overview.component';
import { AdminMotifsEditOverviewComponent } from './admin/admin-overview/admin-modifs-overview/admin-motifs-edit-overview/admin-motifs-edit-overview.component';
import { AdminFamillesListOverviewComponent } from './admin/admin-overview/admin-familles-overview/admin-familles-list-overview/admin-familles-list-overview.component';
import { AdminFamillesEditOverviewComponent } from './admin/admin-overview/admin-familles-overview/admin-familles-edit-overview/admin-familles-edit-overview.component';
import { OptionTagPersonnelOverviewComponent } from './option/option-tag-personnel-overview/option-tag-personnel-overview.component';
import {OptionListTagPersonnelComponent} from "./option/option-tag-personnel-overview/option-list-tag-personnel/option-list-tag-personnel.component";
import {OptionEditTagPersonnelComponent} from "./option/option-tag-personnel-overview/option-edit-tag-personnel/option-edit-tag-personnel.component";
import {EnregistrementSharedService} from "./_services/enregistrementShared";
import {ReplaceCaracBySpacePipe} from "./_pipes/ReplaceCaracBySpace.pipe";
import {ColorPickerModule} from 'angular2-color-picker';
import { DashboardTableComponent } from './dashboard/dashboard-table/dashboard-table.component';
import { TextEditableComponent } from './text-editable/text-editable.component';
import { DashboardRowEnregistrementComponent } from './dashboard/dashboard-table/dashboard-row-enregistrement/dashboard-row-enregistrement.component';
import {TagInputModule} from "ngx-chips";
import { DashboardRowPeriodeOffComponent } from './dashboard/dashboard-table/dashboard-row-periode-off/dashboard-row-periode-off.component';
import { ProjectsCreateEnregistrementIndirectComponent } from './projects/projects-create/projects-create-enregistrement/projects-create-enregistrement-indirect/projects-create-enregistrement-indirect.component';
import {NouisliderModule} from "ng2-nouislider";
import { DashboardHorairesComponent } from './dashboard/dashboard-horaires/dashboard-horaires.component';
import { NotificationsComponent } from './notifications/notifications.component';
import {NotificationService} from "./_services/notification.service";
import {UiSwitchModule} from "ng2-ui-switch/dist";
import {TimeAgoPipe} from "./_services/time-ago-pipe";
import {SharedNotificationService} from "./_services/shared.notification.service";
import {SharedPeriodeTempsService} from "./_services/shared.periodeTemps.service";

export declare let require: any;
export function highchartsFactory() {
    const hc = require('highcharts/highstock');
    const dd = require('highcharts/modules/exporting');
    dd(hc);
    return hc;
}

@NgModule({
    declarations: [
        AppComponent,
        NavigationComponent,
        ContenuComponent,
        LienComponent,
        RegisterComponent,
        LoginComponent,
        HomeComponent,
        AlertComponent,
        DashboardComponent,
        ProjectsComponent,
        PageNotFoundComponent,
        AdminComponent,
        ReportComponent,
        OptionComponent,
        AdminGroupesOverviewComponent,
        AdminUtilisateursOverviewComponent,
        AdminOverviewComponent,
        AdminEditComponent,
        AdminEditUtilisateurInfoComponent,
        AdminEditUtilisateurComponent,
        AdminCreateComponent,
        AdminCreateUtilisateurComponent,
        ProjectsOverviewComponent,
        ProjectsProprietaireSujetOverviewComponent,
        ProjectsAppartenanceSujetOverviewComponent,
        ProjectsEditComponent,
        ProjectsEditProjectComponent,
        ProjectsCreateComponent,
        ProjectsCreateProjectInfoComponent,
        ProjectsEditProjectInfoComponent,
        ProjectsEditTypeTravailComponent,
        AdminEditGroupeComponent,
        ProjectsCreateEnregistrementComponent,
        AdminEditUtilisateurComponent,
        AdminEditUtilisateurGroupeComponent,
        NavbarComponent,
        OptionEditPasswordComponent,
        AdminUtilisateursListOverviewComponent,
        AdminGroupesListOverviewComponent,
        RegisterFormComponent,
        AdminCaracteristiquesOverviewComponent,
        OptionEditConfigurationComponent,
        OptionListTagPersonnelComponent,
        DashboardPieEnregistrementComponent,
        ClickOutsideDirective,
        TimepickerComponent,
        TimepickerHourComponent,
        NavbarEmitterComponent,
        AdminModifsOverviewComponent,
        KeysPipe,
        DatepickerComponent,
        DatepickerAgendaComponent,
        DashboardDatepickerComponent,
        DashboardDatepickerAgendaComponent,
        DashboardTimelineComponent,
        SecondsFormatPipe,
        DashbordEditJourInstanceComponent,
        ProjectsCreateEnregistrementQualifieComponent,
        EnregistrementQualificationComponentComponent,
        EnregistrementTimerComponentComponent,
        EnregistrementButtonsComponentComponent,
        ProjectsCreateEnregistrementQualifiableComponent,
        AdminFamillesOverviewComponent,
        OptionImportComponent,
        AdminTypeTravailsOverviewComponent,
        ReportPersonnalComponent,
        DefaultChoiceChartComponent,
        FamilleRepartitionChartComponent,
        SujetRepartitionChartComponent,
        TypeTravailSujetRepartitionChartComponent,
        TagPersonnelRepartitionChartComponent,
        TagPersonnelSujetRepartitionChartComponent,
        ReportSuperviseurComponent,
        TableComponent,
        AdminTypeTravailsListOverviewComponentComponent,
        ProjectsEditCaracComponent,
        ReportSuperviseurCaracteristiqueComponent,
        ReportSuperviseurCommunComponent,
        ReportSuperviseurSpecifiqueComponent,
        ReportSuperviseurSpecifiqueBilanUtilisateurComponent,
        AdminCaracteristiquesListOverviewComponent,
        AdminCaracteristiquesEditOverviewComponent,
        AdminMotifsListOverviewComponent,
        AdminMotifsEditOverviewComponent,
        AdminFamillesListOverviewComponent,
        AdminFamillesEditOverviewComponent,
        OptionTagPersonnelOverviewComponent,
        OptionEditTagPersonnelComponent,
        ReplaceCaracBySpacePipe,
        DashboardTableComponent,
        TextEditableComponent,
        DashboardRowEnregistrementComponent,
        DashboardRowPeriodeOffComponent,
        ProjectsCreateEnregistrementIndirectComponent,
        DashboardHorairesComponent,
        NotificationsComponent,
        TimeAgoPipe
    ],
    imports: [
        BrowserModule,
        CalendarModule.forRoot(),
        ChartModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        HomeRoutingModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MultiselectDropdownModule,
        ColorPickerModule,
        TagInputModule,
        NouisliderModule,
        UiSwitchModule
    ],
    providers: [
        CanActivateAuthGuard,
        HttpClient,
        AlertService,
        AuthenticationService,
        GroupeService,
        UtilisateurService,
        SujetService,
        TypeTravailService,
        CaracteristiqueService,
        DroitService,
        EnregistrementService,
        ConfigurationService,
        TagPersonnelService,
        SharedService,
        CurrentUserService,
        DateSelectedPickerService,
        JourInstanceService,
        MotifService,
        PeriodeOffService,
        PeriodeTempsService,
        EnregistrementFroidStoreService,
        FamilleService,
        TypeTravailSelectionService,
        {
            provide: HighchartsStatic,
            useFactory: highchartsFactory
        },
        RepartitionService,
        PeriodeAnalyseShared,
        EnregistrementSharedService,
        NotificationService,
        SharedNotificationService,
        SharedPeriodeTempsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
